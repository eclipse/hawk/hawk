#!/bin/bash

VERSION=$(grep Bundle-Version "$WORKSPACE"/core/plugins/org.eclipse.hawk.core/META-INF/MANIFEST.MF | \
              cut --delim=: -f2 | \
              sed -re 's/ *([0-9]+)[.]([0-9]+)[.].*/\1.\2.0/')
DIST_DIR="/home/data/httpd/download.eclipse.org/hawk/$VERSION"
GREYCAT_DIR="$DIST_DIR/greycat"
SSH_USER=genie.hawk@projects-storage.eclipse.org

echo "Creating downloads for Hawk Greycat backend ${VERSION} at ${DIST_DIR}"

# Use rsync with delete rather than a straight up 'rm', in case SSH fails intermittently
ssh "$SSH_USER" mkdir -p "$GREYCAT_DIR"
rsync -rvhz "$WORKSPACE/greycat/org.eclipse.hawk.greycat.updatesite/target/repository/" "$SSH_USER":"${GREYCAT_DIR}/" --delete
scp "$WORKSPACE"/greycat/org.eclipse.hawk.greycat.updatesite/target/org.eclipse.hawk.greycat.updatesite-*.zip "$SSH_USER":"${DIST_DIR}/"
