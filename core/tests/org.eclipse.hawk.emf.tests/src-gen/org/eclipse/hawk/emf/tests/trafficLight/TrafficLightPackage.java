/**
 */
package org.eclipse.hawk.emf.tests.trafficLight;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.hawk.emf.tests.trafficLight.TrafficLightFactory
 * @model kind="package"
 * @generated
 */
public interface TrafficLightPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "trafficLight";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://eclipse.org/hawk/test/trafficLight";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "tl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TrafficLightPackage eINSTANCE = org.eclipse.hawk.emf.tests.trafficLight.impl.TrafficLightPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.hawk.emf.tests.trafficLight.impl.TrafficLightImpl <em>Traffic Light</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.hawk.emf.tests.trafficLight.impl.TrafficLightImpl
	 * @see org.eclipse.hawk.emf.tests.trafficLight.impl.TrafficLightPackageImpl#getTrafficLight()
	 * @generated
	 */
	int TRAFFIC_LIGHT = 0;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRAFFIC_LIGHT__COLOR = 0;

	/**
	 * The number of structural features of the '<em>Traffic Light</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRAFFIC_LIGHT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Traffic Light</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRAFFIC_LIGHT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.hawk.emf.tests.trafficLight.LightColor <em>Light Color</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.hawk.emf.tests.trafficLight.LightColor
	 * @see org.eclipse.hawk.emf.tests.trafficLight.impl.TrafficLightPackageImpl#getLightColor()
	 * @generated
	 */
	int LIGHT_COLOR = 1;


	/**
	 * Returns the meta object for class '{@link org.eclipse.hawk.emf.tests.trafficLight.TrafficLight <em>Traffic Light</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Traffic Light</em>'.
	 * @see org.eclipse.hawk.emf.tests.trafficLight.TrafficLight
	 * @generated
	 */
	EClass getTrafficLight();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.hawk.emf.tests.trafficLight.TrafficLight#getColor <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Color</em>'.
	 * @see org.eclipse.hawk.emf.tests.trafficLight.TrafficLight#getColor()
	 * @see #getTrafficLight()
	 * @generated
	 */
	EAttribute getTrafficLight_Color();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.hawk.emf.tests.trafficLight.LightColor <em>Light Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Light Color</em>'.
	 * @see org.eclipse.hawk.emf.tests.trafficLight.LightColor
	 * @generated
	 */
	EEnum getLightColor();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TrafficLightFactory getTrafficLightFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.hawk.emf.tests.trafficLight.impl.TrafficLightImpl <em>Traffic Light</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.hawk.emf.tests.trafficLight.impl.TrafficLightImpl
		 * @see org.eclipse.hawk.emf.tests.trafficLight.impl.TrafficLightPackageImpl#getTrafficLight()
		 * @generated
		 */
		EClass TRAFFIC_LIGHT = eINSTANCE.getTrafficLight();

		/**
		 * The meta object literal for the '<em><b>Color</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRAFFIC_LIGHT__COLOR = eINSTANCE.getTrafficLight_Color();

		/**
		 * The meta object literal for the '{@link org.eclipse.hawk.emf.tests.trafficLight.LightColor <em>Light Color</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.hawk.emf.tests.trafficLight.LightColor
		 * @see org.eclipse.hawk.emf.tests.trafficLight.impl.TrafficLightPackageImpl#getLightColor()
		 * @generated
		 */
		EEnum LIGHT_COLOR = eINSTANCE.getLightColor();

	}

} //TrafficLightPackage
