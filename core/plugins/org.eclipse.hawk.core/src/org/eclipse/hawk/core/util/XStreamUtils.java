/*******************************************************************************
 * Copyright (c) 2023 The University of York, Aston University.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.core.util;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class XStreamUtils {

	private XStreamUtils() {}

	/**
	 * Initialises an XStream loader, which is allowed to deserialise one or more Java classes
	 * that may have their own XStream annotations.
	 */
	public static final XStream createXStreamLoader(Class<?>... allowedClasses) {
		XStream stream = new XStream(new DomDriver());

		stream.allowTypes(allowedClasses);
		stream.processAnnotations(allowedClasses);
		if (allowedClasses.length > 0) {
			stream.setClassLoader(allowedClasses[0].getClassLoader());
		}

		return stream;
	}

}
