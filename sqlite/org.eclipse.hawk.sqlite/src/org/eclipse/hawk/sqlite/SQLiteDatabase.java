/*******************************************************************************
 * Copyright (c) 2022-2023 The University of York.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.sqlite;

import java.sql.Connection;

import org.eclipse.hawk.core.graph.IGraphNodeIndex;
import org.eclipse.hawk.sqlite.queries.IQueries;
import org.eclipse.hawk.sqlite.queries.StandardQueries;
import org.eclipse.hawk.sqlite.schema.ISchema;
import org.eclipse.hawk.sqlite.schema.StandardSchema;

public class SQLiteDatabase extends AbstractSQLiteDatabase {

	@Override
	protected IGraphNodeIndex createNodeIndex(String name) {
		return new SQLiteNodeIndex(this, name);
	}

	@Override
	protected ISchema createSchema(Connection db) {
		return new StandardSchema(db);
	}

	@Override
	protected IQueries createQueries(Connection db) {
		return new StandardQueries(db);
	}

	@Override
	protected SQLiteNode createNode(int nodeId) {
		return new SQLiteNode(this, nodeId);
	}

	@Override
	protected SQLiteEdge createEdge(String type, final int startId, final int endId, int edgeId) {
		return new SQLiteEdge(this, edgeId, startId, endId, type);
	}

	@Override
	public String getHumanReadableName() {
		return "SQLite Database";
	}

}
