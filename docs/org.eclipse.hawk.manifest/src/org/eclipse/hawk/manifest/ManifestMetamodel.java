/*******************************************************************************
 * Copyright (c) 2011-2016 The University of York.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Konstantinos Barmpis - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.manifest;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.hawk.manifest.metamodel.ManifestMetaModelResource;
import org.eclipse.hawk.core.model.IHawkAttribute;
import org.eclipse.hawk.core.model.IHawkClass;
import org.eclipse.hawk.core.model.IHawkClassifier;
import org.eclipse.hawk.core.model.IHawkMetaModelResource;
import org.eclipse.hawk.core.model.IHawkPackage;
import org.eclipse.hawk.core.model.IHawkReference;
import org.eclipse.hawk.core.model.IHawkStructuralFeature;

public class ManifestMetamodel extends ManifestObject implements IHawkPackage {

	private final static String CLASSNAME = "ManifestMetamodel";
	
	private Set<IHawkClassifier> classes = new HashSet<>();
	ManifestMetaModelResource manifestMetaModelResource;

	public ManifestMetamodel(ManifestMetaModelResource manifestMetaModelResource) {
		this.manifestMetaModelResource = manifestMetaModelResource;
	}

	public void add(IHawkClass mc) {
		classes.add(mc);
	}

	@Override
	public String getUri() {
		return getNsURI();
	}

	@Override
	public String getUriFragment() {
		return "/";
	}

	@Override
	public boolean isFragmentUnique() {
		return false;
	}

	@Override
	public IHawkClassifier getType() {
		return null;
	}

	@Override
	public boolean isSet(IHawkStructuralFeature hsf) {
		return false;
	}

	@Override
	public Object get(IHawkAttribute attr) {
		return null;
	}

	@Override
	public Object get(IHawkReference ref, boolean b) {
		return null;
	}

	@Override
	public String getName() {
		return CLASSNAME;
	}

	@Override
	public IHawkClass getClassifier(String string) throws Exception {
		for (IHawkClassifier c : classes)
			if (c.getName().equals(string))
				return ((IHawkClass) c);
		return null;
	}

	@Override
	public String getNsURI() {
		return "org.eclipse.hawk.MANIFEST";
	}

	@Override
	public Set<IHawkClassifier> getClasses() {
		return classes;
	}

	@Override
	public IHawkMetaModelResource getResource() {
		return manifestMetaModelResource;
	}

}
