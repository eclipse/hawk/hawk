# Example for JavaScript

To run this example, ensure you have a Hawk server running on `localhost:8080`, and run:

```shell
./serve.sh
```

You will need Python 3 and `curl` installed in the system.
