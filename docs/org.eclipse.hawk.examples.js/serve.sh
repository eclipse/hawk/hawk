#!/bin/bash

THRIFT_JS=js/thrift.js
THRIFT_JS_URL="https://raw.githubusercontent.com/apache/thrift/v0.13.0/lib/js/src/thrift.js"

# Copy over all needed JavaScript files
mkdir -p js

if ! test -f "$THRIFT_JS"; then
  curl -o "$THRIFT_JS" "$THRIFT_JS_URL"
fi

cp ../../server/plugins/org.eclipse.hawk.service.api/src-gen-js/*.js js

# Serve this directory
echo "Starting server on http://localhost:9000/"
python -m http.server 9000
