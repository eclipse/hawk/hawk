/*******************************************************************************
 * Copyright (c) 2022-2023 The University of York.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.sqlite;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

import org.eclipse.hawk.core.graph.IGraphEdge;
import org.eclipse.hawk.core.graph.IGraphNode;
import org.eclipse.hawk.sqlite.AbstractSQLiteDatabase.SQLiteConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SQLiteEdge implements IGraphEdge {

	private static final Logger LOGGER = LoggerFactory.getLogger(SQLiteEdge.class);
	protected final AbstractSQLiteDatabase db;
	private final int id, from, to;
	private final String type;

	public SQLiteEdge(AbstractSQLiteDatabase db, int edgeId, int from, int to, String type) {
		this.db = db;
		this.id = edgeId;
		this.type = type;

		this.from = from;
		this.to = to;
	}

	@Override
	public Object getId() {
		return id;
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public Set<String> getPropertyKeys() {
		try {
			final SQLiteConnection conn = db.getConnection();
			final PreparedStatement stmt = conn.getQueries().getEdgePropKeysStatement(id);
			stmt.execute();
			return conn.getStrings(stmt);
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			return Collections.emptySet();
		}
	}

	@Override
	public Object getProperty(String name) {
		try {
			final SQLiteConnection conn = db.getConnection();
			PreparedStatement stmt = conn.getQueries().getEdgePropValueStatement(id, name);
			stmt.execute();
			return conn.getPropertyValue(stmt);
		} catch (SQLException | ClassNotFoundException | IOException e) {
			LOGGER.error(e.getMessage(), e);
		}
		
		return null;
	}

	@Override
	public void setProperty(String name, Object value) {
		try {
			final SQLiteConnection conn = db.getConnection();
			value = conn.preprocessPropertyValue(value);
			PreparedStatement stmt = conn.getQueries().getUpsertEdgePropStatement(id, name, value);
			int rowCount = stmt.executeUpdate();
			assert rowCount == 1 : "One row should have been inserted/updated when setting the property";
		} catch (SQLException | IOException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	@Override
	public IGraphNode getStartNode() {
		return db.createNode(from);
	}

	@Override
	public IGraphNode getEndNode() {
		return db.createNode(to);
	}

	@Override
	public void delete() {
		try {
			final SQLiteConnection conn = db.getConnection();
			conn.getQueries().getDeleteEdgePropsStatement(id).executeUpdate();
			conn.getQueries().getDeleteEdgeStatement(id).executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	@Override
	public void removeProperty(String name) {
		try {
			PreparedStatement stmt = db.getConnection().getQueries().getDeleteEdgePropStatement(id, name);
			stmt.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SQLiteEdge other = (SQLiteEdge) obj;
		return id == other.id;
	}

	@Override
	public String toString() {
		return String.format("SQLiteEdge [id=%s]", id);
	}
	
}
