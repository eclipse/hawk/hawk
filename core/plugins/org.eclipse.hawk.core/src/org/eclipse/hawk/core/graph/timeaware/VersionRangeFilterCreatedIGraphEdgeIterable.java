/*******************************************************************************
 * Copyright (c) 2023 University of York.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.core.graph.timeaware;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.function.Function;

import org.eclipse.hawk.core.graph.IGraphEdge;
import org.eclipse.hawk.core.graph.IGraphNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Iterable that loops over all the edges that were created within a certain
 * version range from a node.
 */
public class VersionRangeFilterCreatedIGraphEdgeIterable implements Iterable<IGraphEdge> {
	private static final Logger LOGGER = LoggerFactory.getLogger(VersionRangeFilterIGraphEdgeIterable.class);

	private final ITimeAwareGraphNode node;
	private final long fromInclusive, toInclusive;
	private final Function<IGraphNode, Iterable<IGraphEdge>> node2edges;
	private Function<IGraphEdge, Object> edge2id;

	public VersionRangeFilterCreatedIGraphEdgeIterable(ITimeAwareGraphNode node,
			long fromInclusive, long toInclusive,
			Function<IGraphNode, Iterable<IGraphEdge>> node2edges,
			Function<IGraphEdge, Object> edge2id) {
		this.node = node;
		this.fromInclusive = fromInclusive;
		this.toInclusive = toInclusive;
		this.node2edges = node2edges;
		this.edge2id = edge2id;
	}

	@Override
	public Iterator<IGraphEdge> iterator() {
		try {
			final Iterator<ITimeAwareGraphNode> itVersion = node
				.getVersionsBetween(fromInclusive, toInclusive).iterator();

			return new Iterator<IGraphEdge>() {
				Set<Object> prevIDs = null;
				Set<Object> currentIDs = new HashSet<>();

				Iterator<IGraphEdge> itEdge = null;
				IGraphEdge next = null;
				
				@Override
				public boolean hasNext() {
					if (prevIDs == null) {
						prevIDs = new HashSet<>();

						ITimeAwareGraphNode beforeFrom = node.travelInTime(fromInclusive - 1);
						if (beforeFrom != null) {
							for (IGraphEdge e : node2edges.apply(beforeFrom)) {
								prevIDs.add(edge2id.apply(e));
							}
						}
					}

					while (next == null && ((itEdge != null && itEdge.hasNext()) || itVersion.hasNext())) {
						if (itEdge == null) {
							final ITimeAwareGraphNode version = itVersion.next();
							itEdge = node2edges.apply(version).iterator();
						}
						if (itEdge.hasNext()) {
							IGraphEdge candidate = itEdge.next();
							Object endID = edge2id.apply(candidate);

							currentIDs.add(endID);
							if (!prevIDs.contains(endID)) {
								next = candidate;
							}
						} else {
							// End of the edges in this version: current is now prev
							prevIDs = currentIDs;
							currentIDs = new HashSet<>();
							itEdge = null;
						}
					}

					return next != null;
				}

				@Override
				public IGraphEdge next() {
					if (!hasNext()) throw new NoSuchElementException();
					IGraphEdge ret = next;
					next = null;
					return ret;
				}
			};
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return Collections.emptyIterator();
		}
	}
	
}
