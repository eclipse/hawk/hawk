/*******************************************************************************
 * Copyright (c) 2011-2017 The University of York, Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Konstantinos Barmpis - initial API and implementation
 *     Antonio Garcia-Dominguez - extract import to interface, cleanup,
 *       use revision in imports, divide into super + subclass
 ******************************************************************************/
package org.eclipse.hawk.core.runtime;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.hawk.core.IConsole;
import org.eclipse.hawk.core.ICredentialsStore;
import org.eclipse.hawk.core.IVcsManager;
import org.eclipse.hawk.core.VcsCommitItem;

/**
 * Indexes only the latest revision into the graph, and does not maintain
 * repository nodes.
 */
public class ModelIndexerImpl extends BaseModelIndexer {
	private static final String UNKNOWN_REMOTE_REVISION = "-4";
	private static final String UNKNOWN_LOCAL_REVISION = "-3";
	private final Map<String, String> currLocalTopRevisions = new HashMap<>();
	private final Map<String, String> currReposTopRevisions = new HashMap<>();

	public ModelIndexerImpl(String name, File parentfolder, ICredentialsStore credStore, IConsole c) {
		super(name, parentfolder, credStore, c);
	}

	@Override
	public void addVCSManager(IVcsManager vcs, boolean persist) {
		currLocalTopRevisions.put(vcs.getLocation(), UNKNOWN_LOCAL_REVISION);
		currReposTopRevisions.put(vcs.getLocation(), UNKNOWN_REMOTE_REVISION);

		super.addVCSManager(vcs, persist);
	}

	@Override
	public void removeVCSManager(IVcsManager vcs) throws Exception {
		currLocalTopRevisions.remove(vcs.getLocation());
		currReposTopRevisions.remove(vcs.getLocation());
		super.removeVCSManager(vcs);
	}

	@Override
	protected void resetRepository(String repoURL) {
		System.err.println("reseting local top revision of repository: " + repoURL
				+ "\n(as elements in it were removed or new metamodels were added to Hawk)");
		currLocalTopRevisions.put(repoURL, UNKNOWN_LOCAL_REVISION);
	}

	@Override
	protected boolean synchronise(IVcsManager vcsManager) throws Exception {
		boolean success = true;

		String currentRevision = currReposTopRevisions.get(vcsManager.getLocation());
		try {
			/*
			 * Try to fetch the current revision from the VCS, if not, keep the latest seen
			 * revision.
			 */
			currentRevision = vcsManager.getCurrentRevision();
			currReposTopRevisions.put(vcsManager.getLocation(), currentRevision);
		} catch (Exception e) {
			console.printerrln(e);
			success = false;
		}
	
		if (!currentRevision.equals(currLocalTopRevisions.get(vcsManager.getLocation()))) {
			final Collection<VcsCommitItem> files = vcsManager.getDelta(currLocalTopRevisions.get(vcsManager.getLocation()));
			latestUpdateFoundChanges = true;
			boolean updatersOK = synchroniseFiles(currentRevision, vcsManager, files);
	
			if (updatersOK) {
				currLocalTopRevisions.put(vcsManager.getLocation(),
						currReposTopRevisions.get(vcsManager.getLocation()));
			} else {
				success = false;
				currLocalTopRevisions.put(vcsManager.getLocation(), UNKNOWN_LOCAL_REVISION);
			}
		}
	
		return success;
	}

}
