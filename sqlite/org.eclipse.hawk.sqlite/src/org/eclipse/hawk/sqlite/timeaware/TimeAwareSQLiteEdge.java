/*******************************************************************************
 * Copyright (c) 2022-2023 The University of York.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.sqlite.timeaware;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;

import org.eclipse.hawk.core.graph.IGraphEdge;
import org.eclipse.hawk.core.graph.IGraphNode;
import org.eclipse.hawk.sqlite.SQLiteEdge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimeAwareSQLiteEdge implements IGraphEdge {

	private static final Logger LOGGER = LoggerFactory.getLogger(SQLiteEdge.class);
	protected final TimeAwareSQLiteDatabase db;
	private final int id, from, to;
	private final String type;
	private final long time;

	public TimeAwareSQLiteEdge(TimeAwareSQLiteDatabase db, int edgeId, int from, int to, String type, long time) {
		this.db = db;
		this.id = edgeId;
		this.type = type;

		this.from = from;
		this.to = to;

		this.time = time;
	}

	@Override
	public Object getId() {
		return id;
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public Set<String> getPropertyKeys() {
		try {
			final PreparedStatement stmt = db.getQueries().getEdgePropKeysStatement(id, time);
			stmt.execute();
			return db.getConnection().getStrings(stmt);
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			return Collections.emptySet();
		}
	}

	@Override
	public Object getProperty(String name) {
		try {
			PreparedStatement stmt = db.getQueries().getEdgePropValueStatement(id, name, time);
			stmt.execute();
			return db.getConnection().getPropertyValue(stmt);
		} catch (SQLException | ClassNotFoundException | IOException e) {
			LOGGER.error(e.getMessage(), e);
		}
		
		return null;
	}

	@Override
	public void setProperty(String name, Object value) {
		try {
			value = db.getConnection().preprocessPropertyValue(value);
			PreparedStatement stmt = db.getQueries().getUpsertEdgePropStatement(id, name, value, time);
			int rowCount = stmt.executeUpdate();
			assert rowCount == 1 : "One row should have been inserted/updated when setting the property";
		} catch (SQLException | IOException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	@Override
	public IGraphNode getStartNode() {
		return new TimeAwareSQLiteNode(db, from, time);
	}

	@Override
	public IGraphNode getEndNode() {
		return new TimeAwareSQLiteNode(db, to, time);
	}

	@Override
	public void delete() {
		try {
			db.getQueries().getDeleteEdgePropsStatement(id, time).executeUpdate();
			db.getQueries().getDeleteEdgeStatement(id, time).executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	@Override
	public void removeProperty(String name) {
		try {
			PreparedStatement stmt = db.getQueries().getDeleteEdgePropStatement(id, name, time);
			stmt.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, time);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimeAwareSQLiteEdge other = (TimeAwareSQLiteEdge) obj;
		return id == other.id && time == other.time;
	}

	@Override
	public String toString() {
		return "TimeAwareSQLiteEdge [id=" + id + ", time=" + time + "]";
	}
	
}
