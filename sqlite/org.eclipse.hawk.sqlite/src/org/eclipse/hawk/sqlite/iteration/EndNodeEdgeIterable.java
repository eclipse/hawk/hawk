/*******************************************************************************
 * Copyright (c) 2023 The University of York.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.sqlite.iteration;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Function;

import org.eclipse.hawk.core.graph.IGraphEdge;
import org.eclipse.hawk.core.graph.IGraphNode;

public class EndNodeEdgeIterable implements Iterable<IGraphEdge> {
	private final Iterable<IGraphNode> endNodes;
	private final Function<IGraphNode, IGraphEdge> endNodeToEdge;

	public EndNodeEdgeIterable(Iterable<IGraphNode> endNodes, Function<IGraphNode, IGraphEdge> endNodeToEdge) {
		this.endNodes = endNodes;
		this.endNodeToEdge = endNodeToEdge;
	}

	@Override
	public Iterator<IGraphEdge> iterator() {
		Iterator<IGraphNode> itEndNodes = endNodes.iterator();
		
		return new Iterator<IGraphEdge>() {
			IGraphEdge next = null;

			@Override
			public boolean hasNext() {
				if (next == null) {
					while (next == null && itEndNodes.hasNext()) {
						IGraphNode endNode = itEndNodes.next();
						next = endNodeToEdge.apply(endNode);
					}
				}
				return next != null;
			}

			@Override
			public IGraphEdge next() {
				if (!hasNext()) {
					throw new NoSuchElementException();
				}

				IGraphEdge ret = next;
				next = null;
				return ret;
			}
		};
	}
}