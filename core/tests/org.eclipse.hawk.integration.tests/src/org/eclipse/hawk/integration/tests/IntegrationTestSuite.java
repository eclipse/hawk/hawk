/*******************************************************************************
 * Copyright (c) 2015-2017 The University of York, Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.integration.tests;

import java.io.File;
import java.util.Collections;

import org.eclipse.hawk.backend.tests.factories.IGraphDatabaseFactory;
import org.eclipse.hawk.backend.tests.factories.OrientDatabaseFactory;
import org.eclipse.hawk.integration.tests.bpmn.ModelVersioningTest;
import org.eclipse.hawk.integration.tests.emf.CountInstancesTest;
import org.eclipse.hawk.integration.tests.emf.DerivedFeatureTest;
import org.eclipse.hawk.integration.tests.emf.DerivedFromMetaPropertiesTest;
import org.eclipse.hawk.integration.tests.emf.EnumSupportTest;
import org.eclipse.hawk.integration.tests.emf.IndexedAttributeTest;
import org.eclipse.hawk.integration.tests.emf.MetamodelQueryTest;
import org.eclipse.hawk.integration.tests.emf.ScopedQueryTest;
import org.eclipse.hawk.integration.tests.emf.SubtreeContextTest;
import org.eclipse.hawk.integration.tests.emf.TreeUpdateTest;
import org.eclipse.hawk.integration.tests.modelio.ModelioMetamodelPopulationTest;
import org.eclipse.hawk.integration.tests.modelio.ModelioProxyResolutionTest;
import org.eclipse.hawk.integration.tests.uml.UMLIndexingTest;
import org.eclipse.hawk.integration.tests.uml.UMLWorkspaceIndexingTest;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runners.Suite.SuiteClasses;

import com.github.peterwippermann.junit4.parameterizedsuite.ParameterContext;
import com.github.peterwippermann.junit4.parameterizedsuite.ParameterizedSuite;

@RunWith(ParameterizedSuite.class)
@SuiteClasses({
	CountInstancesTest.class,
	DerivedFeatureTest.class,
	IndexedAttributeTest.class,
	DerivedFromMetaPropertiesTest.class,
	EnumSupportTest.class,
	MetamodelQueryTest.class,
	ModelioProxyResolutionTest.class,
	ModelioMetamodelPopulationTest.class,
	ModelVersioningTest.class,
	ScopedQueryTest.class,
	SubtreeContextTest.class,
	TreeUpdateTest.class,
	UMLIndexingTest.class,
	UMLWorkspaceIndexingTest.class,
})
public class IntegrationTestSuite {

	@Parameters(name="{1}")
	public static Object[][] params() {
		return new Object[][] {
			{ new File("."), new OrientDatabaseFactory() },
		};
	}

	public static Iterable<Object[]> caseParams() {
		if (ParameterContext.isParameterSet()) {
			return Collections.singletonList(ParameterContext.getParameter(Object[].class));
		} else {
			return Collections.singletonList(IntegrationTestSuite.params()[0]);
		}
	}

	@Parameter(0)
	public File baseDirectory;

	@Parameter(1)
	public IGraphDatabaseFactory dbFactory;
	
}
