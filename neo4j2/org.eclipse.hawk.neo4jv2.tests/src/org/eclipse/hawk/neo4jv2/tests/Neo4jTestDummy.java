/*******************************************************************************
 * Copyright (c) 2020 The University of York, Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.neo4jv2.tests;

import java.io.File;
import java.util.Collections;

import org.eclipse.hawk.backend.tests.factories.IGraphDatabaseFactory;
import org.eclipse.hawk.integration.tests.uml.UMLWorkspaceIndexingTest;
import org.junit.runners.Parameterized.Parameters;

/**
 * Not for CI: this is only so we can easily run a single integration test
 * using Neo4j, by changing the class we extend.
 */
public class Neo4jTestDummy extends UMLWorkspaceIndexingTest {

	@Parameters(name="{1}")
	public static Iterable<Object[]> params() {
		return Collections.singletonList( new Object[] {
			new File("../../core/tests/org.eclipse.hawk.integration.tests"), new Neo4JDatabaseFactory()
		});
	}

	public Neo4jTestDummy(File baseDir, IGraphDatabaseFactory dbf) {
		super(baseDir, dbf);
		// TODO Auto-generated constructor stub
	}

}
