/*******************************************************************************
 * Copyright (c) 2023 The University of York.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.sqlite.timeaware;

import java.sql.Connection;
import java.util.Map;

import org.eclipse.hawk.core.graph.IGraphEdge;
import org.eclipse.hawk.core.graph.IGraphIterable;
import org.eclipse.hawk.core.graph.IGraphNodeIndex;
import org.eclipse.hawk.core.graph.timeaware.ITimeAwareGraphDatabase;
import org.eclipse.hawk.core.graph.timeaware.ITimeAwareGraphNode;
import org.eclipse.hawk.core.graph.timeaware.ITimeAwareGraphNodeIndex;
import org.eclipse.hawk.sqlite.AbstractSQLiteDatabase;
import org.eclipse.hawk.sqlite.iteration.StatementGraphNodeIterable;
import org.eclipse.hawk.sqlite.queries.IQueries;
import org.eclipse.hawk.sqlite.queries.TimeAwareQueries;
import org.eclipse.hawk.sqlite.schema.ISchema;
import org.eclipse.hawk.sqlite.schema.TimeAwareSchema;

public class TimeAwareSQLiteDatabase extends AbstractSQLiteDatabase implements ITimeAwareGraphDatabase {

	private long timepoint = 0;

	@Override
	protected IGraphNodeIndex createNodeIndex(String name) {
		return new TimeAwareSQLiteNodeIndex(this, name, timepoint);
	}

	@Override
	protected IQueries createQueries(Connection db) {
		return new TimeAwareQueries(db, this::getTime);
	}

	@Override
	protected ISchema createSchema(Connection db) {
		return new TimeAwareSchema(db);
	}

	@Override
	protected TimeAwareSQLiteNode createNode(int nodeId) {
		return new TimeAwareSQLiteNode(this, nodeId, timepoint);
	}

	@Override
	protected IGraphEdge createEdge(String type, final int startId, final int endId, int edgeId) {
		return new TimeAwareSQLiteEdge(this, edgeId, startId, endId, type, timepoint);
	}

	@Override
	public IGraphIterable<? extends ITimeAwareGraphNode> allNodes(String label, long time) {
		return new StatementGraphNodeIterable<TimeAwareSQLiteNode>(
			() -> getQueries().getNodeIDsByLabelStatement(label, time),
			() -> getQueries().getNodeCountByLabelStatement(label, time),
			() -> getQueries().getFirstNodeIDByLabelStatement(label, time),
			(o) -> this.createNode((int) o)
		);
	}

	@Override
	public long getTime() {
		return timepoint;
	}

	@Override
	public void setTime(long time) {
		this.timepoint = time;
	}

	@Override
	public ITimeAwareGraphNodeIndex getOrCreateNodeIndex(String name) {
		return (ITimeAwareGraphNodeIndex) super.getOrCreateNodeIndex(name);
	}

	@Override
	public ITimeAwareGraphNode createNode(Map<String, Object> props, String label) {
		return (ITimeAwareGraphNode) super.createNode(props, label);
	}

	@Override
	public ITimeAwareGraphNode getNodeById(Object id) {
		return (ITimeAwareGraphNode) super.getNodeById(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public IGraphIterable<? extends ITimeAwareGraphNode> allNodes(String label) {
		return (IGraphIterable<ITimeAwareGraphNode>) super.allNodes(label);
	}

	@Override
	public ITimeAwareGraphNodeIndex getFileIndex() {
		return (ITimeAwareGraphNodeIndex) super.getFileIndex();
	}

	protected TimeAwareQueries getQueries() {
		return (TimeAwareQueries) getConnection().getQueries();
	}

	@Override
	public String getHumanReadableName() {
		return "Time-Aware SQLite Database";
	}

}
