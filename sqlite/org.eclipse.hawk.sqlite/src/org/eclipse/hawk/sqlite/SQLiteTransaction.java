/*******************************************************************************
 * Copyright (c) 2022-2023 The University of York.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.sqlite;

import java.sql.SQLException;

import org.eclipse.hawk.core.graph.IGraphTransaction;
import org.eclipse.hawk.sqlite.AbstractSQLiteDatabase.SQLiteConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SQLiteTransaction implements IGraphTransaction {

	private static final Logger LOGGER = LoggerFactory.getLogger(SQLiteTransaction.class);
	private SQLiteConnection conn;

	public SQLiteTransaction(AbstractSQLiteDatabase db) {
		this.conn = db.getGraph();
	}

	@Override
	public void success() {
		try {
			conn.commit();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	@Override
	public void failure() {
		try {
			conn.rollback();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	@Override
	public void close() {
		// no-op: we don't want to close the whole connection for this thread
	}

}
