/*******************************************************************************
 * Copyright (c) 2023 University of York.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.core.graph.timeaware;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.function.Function;

import org.eclipse.hawk.core.graph.IGraphEdge;
import org.eclipse.hawk.core.graph.IGraphNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Iterable that loops over all the edges that were valid within a certain version range from a node.
 */
public final class VersionRangeFilterIGraphEdgeIterable implements Iterable<IGraphEdge> {
	private static final Logger LOGGER = LoggerFactory.getLogger(VersionRangeFilterIGraphEdgeIterable.class);
	
	private final List<ITimeAwareGraphNode> versions;
	private final Function<IGraphNode, Iterable<IGraphEdge>> node2edges;
	private Function<IGraphEdge, Object> edge2id;

	public VersionRangeFilterIGraphEdgeIterable(List<ITimeAwareGraphNode> versions,
			Function<IGraphNode, Iterable<IGraphEdge>> node2edges,
			Function<IGraphEdge, Object> edge2id) {
		this.node2edges = node2edges;
		this.versions = versions;
		this.edge2id = edge2id;
	}

	@Override
	public Iterator<IGraphEdge> iterator() {
		try {
			final Iterator<ITimeAwareGraphNode> itVersion = versions.iterator();

			final Set<Object> seenEndIDs = new HashSet<>();
			return new Iterator<IGraphEdge>() {
				Iterator<IGraphEdge> itEdge = null;
				IGraphEdge next = null;

				@Override
				public boolean hasNext() {
					while (next == null && ((itEdge != null && itEdge.hasNext()) || itVersion.hasNext())) {
						if (itEdge == null) {
							final ITimeAwareGraphNode version = itVersion.next();
							final Iterable<IGraphEdge> edges = node2edges.apply(version);
							itEdge = edges.iterator();
						}
						if (itEdge.hasNext()) {
							IGraphEdge candidateEdge = itEdge.next();
							if (seenEndIDs.add(edge2id.apply(candidateEdge))) {
								next = candidateEdge;
							}
						} else {
							itEdge = null;
						}
					}
					return next != null;
				}

				@Override
				public IGraphEdge next() {
					if (!hasNext()) throw new NoSuchElementException();

					IGraphEdge ret = next;
					next = null;
					return ret;
				}
			};
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return Collections.emptyIterator();
		}
	}
}