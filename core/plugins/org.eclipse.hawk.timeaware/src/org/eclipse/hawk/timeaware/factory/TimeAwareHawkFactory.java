/*******************************************************************************
 * Copyright (c) 2018-2020 Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.timeaware.factory;

import java.io.File;
import java.util.List;

import org.eclipse.hawk.core.IConsole;
import org.eclipse.hawk.core.ICredentialsStore;
import org.eclipse.hawk.core.IHawk;
import org.eclipse.hawk.core.runtime.LocalHawkFactory;

public class TimeAwareHawkFactory extends LocalHawkFactory {

	@Override
	public IHawk create(String name, File storageFolder, String location, ICredentialsStore credStore, IConsole console,
			List<String> enabledPlugins) throws Exception {
		return new TimeAwareHawk(name, storageFolder, credStore, console);
	}

	@Override
	public String getHumanReadableName() {
		return "Time-aware local Hawk";
	}

}
