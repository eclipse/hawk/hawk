/*******************************************************************************
 * Copyright (c) 2019 Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.integration.tests.emf;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.eclipse.hawk.backend.tests.factories.IGraphDatabaseFactory;
import org.eclipse.hawk.integration.tests.IntegrationTestSuite;
import org.eclipse.hawk.integration.tests.ModelIndexingTest;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;

/**
 * Tests for supporting enums in metamodels.
 */
public class EnumSupportTest extends ModelIndexingTest {

	@Parameters(name="{1}")
	public static Iterable<Object[]> params() {
		return IntegrationTestSuite.caseParams();
	}

	public EnumSupportTest(File baseDir, IGraphDatabaseFactory dbf) {
		super(baseDir, dbf, new EMFModelSupportFactory());
	} 

	@Test
	public void queryEnumValues() throws Throwable {
		indexer.registerMetamodels(new File(baseDir, "resources/metamodels/Ecore.ecore"),
				new File(baseDir, "resources/metamodels/ColoredTree.ecore"));
		requestFolderIndex(new File(baseDir, "resources/models/enum-support"));

		scheduleAndWait(() -> {
			assertEquals(1, eol("return Tree.all.select(t|t.color = Color#red).size;"));
			return null;
		});
	}
}
