/*******************************************************************************
 * Copyright (c) 2011-2023 The University of York.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Konstantinos Barmpis - initial API and implementation
 *     Antonio Garcia-Dominguez - add methods for edges with node filters
 ******************************************************************************/
package org.eclipse.hawk.core.graph;

import static org.eclipse.hawk.core.graph.NodeFilterIGraphEdgeIterable.MATCH_END_NODE;
import static org.eclipse.hawk.core.graph.NodeFilterIGraphEdgeIterable.MATCH_START_NODE;

import java.util.Set;

public interface IGraphNode {

	Object getId();

	Set<String> getPropertyKeys();

	Object getProperty(String name);

	void setProperty(String name, Object value);

	Iterable<IGraphEdge> getEdges();

	default Iterable<IGraphEdge> getEdges(Iterable<IGraphNode> nodes) {
		return new NodeFilterIGraphEdgeIterable(getEdges(), nodes, MATCH_START_NODE | MATCH_END_NODE);
	}

	Iterable<IGraphEdge> getEdgesWithType(String type);

	default Iterable<IGraphEdge> getEdgesWithType(String type, Iterable<IGraphNode> nodes) {
		return new NodeFilterIGraphEdgeIterable(getEdgesWithType(type), nodes, MATCH_START_NODE | MATCH_END_NODE);
	}

	Iterable<IGraphEdge> getOutgoingWithType(String type);

	default Iterable<IGraphEdge> getOutgoingWithType(String type, Iterable<IGraphNode> endNodes) {
		return new NodeFilterIGraphEdgeIterable(getOutgoingWithType(type), endNodes, MATCH_END_NODE);
	}

	Iterable<IGraphEdge> getIncomingWithType(String type);

	default Iterable<IGraphEdge> getIncomingWithType(String type, Iterable<IGraphNode> startNodes) {
		return new NodeFilterIGraphEdgeIterable(getIncomingWithType(type), startNodes, MATCH_START_NODE);
	}

	Iterable<IGraphEdge> getIncoming();

	default Iterable<IGraphEdge> getIncoming(Iterable<IGraphNode> startNodes) {
		return new NodeFilterIGraphEdgeIterable(getIncoming(), startNodes, MATCH_START_NODE);
	}

	Iterable<IGraphEdge> getOutgoing();

	default Iterable<IGraphEdge> getOutgoing(Iterable<IGraphNode> endNodes) {
		return new NodeFilterIGraphEdgeIterable(getOutgoing(), endNodes, MATCH_END_NODE);
	}

	void delete();

	@Override
	boolean equals(Object o);

	IGraphDatabase getGraph();

	void removeProperty(String name);
}
