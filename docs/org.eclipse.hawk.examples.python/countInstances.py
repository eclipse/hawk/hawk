#!/usr/bin/env python3

from thrift.transport import THttpClient
from thrift.protocol.TBinaryProtocol import TBinaryProtocol

from api import Hawk
from api.ttypes import *

import pprint as pp

# Set up client
transport = THttpClient.THttpClient('http://localhost:8080/thrift/hawk/binary')
protocol = TBinaryProtocol(transport)
client = Hawk.Client(protocol)

# Set up query parameters
queryString = 'return Model.allInstances.size;'

# Use the appropriate query engine
timeAware = False
if timeAware:
    queryLanguage = 'org.eclipse.hawk.timeaware.queries.TimeAwareEOLQueryEngine'
else:
    queryLanguage = 'org.eclipse.hawk.epsilon.emc.EOLQueryEngine'

# Default options
#
# More info: https://www.eclipse.org/hawk/server/api/#hawkqueryoptions
queryOptions = HawkQueryOptions()

try:
    # Open the connection
    transport.open()

    # Fetch details of the first Hawk instance on the server
    firstInstance = client.listInstances()[0]

    # Run the EOL query on the server and pretty-print the results
    queryResults = client.timedQuery(firstInstance.name, queryString, queryLanguage, queryOptions)

    print("Raw result:")
    pp.pprint(queryResults)

    print("\nExtracted value:")
    print(f"There are {queryResults.result.vInteger} model elements in instance '{firstInstance.name}'")
finally:
    # Close the connection
    transport.close()
