/*******************************************************************************
* Copyright (c) 2023 The University of York.
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* http://www.eclipse.org/legal/epl-2.0.
*
* This Source Code may also be made available under the following Secondary
* Licenses when the conditions for such availability set forth in the Eclipse
* Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
*
* SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
*
* Contributors:
*     Antonio Garcia-Dominguez - initial API and implementation
******************************************************************************/
package org.eclipse.hawk.sqlite.schema;

import java.sql.SQLException;

/**
* Automatically generated schema interface. DO NOT EDIT MANUALLY.
* @generated
*/
public interface ISchema {

	public void createSchema() throws SQLException;
	
	public void createNodeIndex(String name) throws SQLException;
	
	public void clearNodeIndexTable(String name) throws SQLException;
	
	public void dropNodeIndexTable(String name) throws SQLException;
	
}