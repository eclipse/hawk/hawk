# Notices for Eclipse Hawk

This content is produced and maintained by the Eclipse Hawk project.

  * Project home: https://projects.eclipse.org/projects/modeling.hawk
  * Project website: https://eclipse.org/hawk

## Trademarks

Eclipse Hawk is a trademark of the Eclipse Foundation. Eclipse, and the Eclipse
Logo are registered trademarks of the Eclipse Foundation.

## Copyright

All content is the property of the respective authors or their employers.
For more information regarding authorship of content, please consult the
listed source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

The source code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License, version 3.

SPDX-License-Identifier: EPL-2.0 OR GPL-3.0

## Source Code

The project maintains the following source code repositories:

 * https://gitlab.eclipse.org/eclipse/hawk/
 * https://git.eclipse.org/c/www.eclipse.org/hawk.git

## Third-party Content

Apache Artemis (2.6.4)

* License: Apache License, 2.0

Apache Commons Codec (1.10)

* License: Apache License, 2.0

Apache Commons Pool2 (2.4.2)

* License: Apache License, 2.0

Apache HttpComponents HttpClient (4.3.6)

* License: Apache License, 2.0

Apache HttpComponents HttpCore (4.3.3)

* License: Apache License, 2.0

Apache HttpComponents HttpCore NIO (4.1.0)

* License: Apache License, 2.0

Apache Lucene (7.2.1)

* License: Apache License, 2.0

Apache Shiro (1.2.4)

* License: Apache License, 2.0

Apache Thrift (0.13.0)

* License: Apache License, 2.0

cglib (3.1)

* License: Apache License, 2.0
* Project: https://github.com/cglib/cglib

Greycat (18)

* License: Apache License, 2.0
* Project: https://greycat.ai/
* Source: https://github.com/datathings/greycat
* With deployment changes in: https://github.com/bluezio/greycat

MapDB (1.0.9)

* License: Apache License, 2.0
* Project: http://www.mapdb.org/
* Source: https://github.com/jankotek/mapdb/

Neo4j (2.0.5) - WORKSWITH

* License: GPLv2
* Project: https://neo4j.com/
* Source: https://github.com/neo4j/neo4j

OrientDB (2.2.30)

* License: Apache License, 2.0
* Project: https://orientdb.com/
* Source: https://github.com/orientechnologies/orientdb

Simple Logging Facade for Java (SLF4J) (1.7.2)

* License: MIT License
* Project: http://www.slf4j.org/
* Source: https://github.com/qos-ch/slf4j

SQLite (3.36.0) - WORKSWITH

* License: Public Domain (https://sqlite.org/copyright.html)
* Project: https://sqlite.org/
* Source: https://github.com/sqlite/sqlite/tree/version-3.36.0

sqlite-jdbc (3.36.0.3) - WORKSWITH

* License: Apache Software License 1.1
* Project: https://github.com/xerial/sqlite-jdbc/
* Source: https://github.com/xerial/sqlite-jdbc/tree/3.36.0.3

TMateSoft SQLJet (1.1.10) - WORKSWITH

* License: GPL2
* Project: https://sqljet.com/
* Source: https://svn.sqljet.com/repos/sqljet/

TMateSoft SVNKit (1.8.6) - WORKSWITH

* License: TMate Open Source License
* Project: https://svnkit.com/
* Source: https://git.tmatesoft.com/repos/svnkit.git

xstream (1.4.19)

* License: Modified BSD
* Project: https://x-stream.github.io/
* Source: https://github.com/x-stream/xstream
