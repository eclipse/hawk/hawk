/*******************************************************************************
 * Copyright (c) 2020-2023 The University of York, Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.sqlite.tests;

import java.io.File;

import org.eclipse.hawk.timeaware.tests.TimeAwareTestSuite;
import org.junit.runners.Parameterized.Parameters;

public class SQLiteTimeAwareTestSuite extends TimeAwareTestSuite {

	private static final String TIMEAWARE_TESTS_PATH = "../../core/tests/org.eclipse.hawk.integration.tests";

	@Parameters(name="{1}")
	public static Object[][] params() {
		return new Object[][] {
			{ new File(TIMEAWARE_TESTS_PATH), new TimeAwareSQLiteDatabaseFactory() },
		};
	}

}
