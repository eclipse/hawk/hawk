/*******************************************************************************
 * Copyright (c) 2015-2020 The University of York, Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.integration.tests.emf;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.eclipse.hawk.backend.tests.factories.IGraphDatabaseFactory;
import org.eclipse.hawk.graph.ProxyReferenceList;
import org.eclipse.hawk.graph.ProxyReferenceList.ProxyReference;
import org.eclipse.hawk.graph.syncValidationListener.SyncValidationListener;
import org.eclipse.hawk.integration.tests.IntegrationTestSuite;
import org.eclipse.hawk.integration.tests.ModelIndexingTest;
import org.eclipse.hawk.localfolder.LocalFolder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;

/**
 * Tests the support for {@code Model.getProxies()} and the {@code .hawkProxies}
 * advanced attribute.
 */
public class ProxiesTest extends ModelIndexingTest {

	@Rule
	public GraphChangeListenerRule<SyncValidationListener> syncValidation
		= new GraphChangeListenerRule<>(new SyncValidationListener());

	@Parameters(name="{1}")
	public static Iterable<Object[]> params() {
		return IntegrationTestSuite.caseParams();
	}

	public ProxiesTest(File baseDir, IGraphDatabaseFactory dbf) {
		super(baseDir, dbf, new EMFModelSupportFactory());
	}

	@Test
	public void proxies() throws Throwable {
		indexer.registerMetamodels(
			new File(baseDir, "resources/metamodels/Ecore.ecore"),
			new File(baseDir, "resources/metamodels/crossrefs.ecore"));
		LocalFolder vcs = requestFolderIndex(new File(baseDir, "resources/models/proxies"));

		scheduleAndWait(() -> {
			assertNoErrors(syncValidation.getListener());

			// 2 proxies to the commented out modelA.model element
			// 2 proxies to the commented out modelB.model element
			assertEquals(4, eol("return Model.proxies.size;"));

			// Try the version with repository argument
			assertEquals(4, eol(String.format("return Model.getProxies('%s').size;", vcs.getLocation())));

			// Try the version from a model element
			assertEquals(1, eol("return Element.all.selectOne(e|e.id = 0).hawkProxies.size;"));
			assertEquals(0, eol("return Element.all.selectOne(e|e.id = 60).hawkProxies.size;"));

			// Try going through the values of a specific proxy reference
			ProxyReferenceList l = (ProxyReferenceList) eol("return Element.all.selectOne(e|e.id = 0).hawkProxies.first;");
			assertEquals("modelB.model", l.getTargetFile().getFilePath());
			assertEquals(1, l.getReferences().size());

			ProxyReference firstRef = l.getReferences().get(0);
			assertEquals("xrefs", firstRef.getEdgeLabel());
			assertEquals(vcs.getLocation(), firstRef.getTarget().getRepositoryURL());
			assertEquals("modelB.model", firstRef.getTarget().getFilePath());
			assertEquals("_9r4mcF04Eeqfb-c5mrgLZw", firstRef.getTarget().getFragment());

			return null;
		});
	}

}
