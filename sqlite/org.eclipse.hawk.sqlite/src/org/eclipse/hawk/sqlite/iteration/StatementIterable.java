/*******************************************************************************
 * Copyright (c) 2022-2023 The University of York.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.sqlite.iteration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StatementIterable<T> implements Iterable<T> {
	private static final Logger LOGGER = LoggerFactory.getLogger(StatementIterable.class);
	private final StatementSupplier stmtSupplier;
	private final ResultSetFunction<T> resultToEdge;

	public StatementIterable(StatementSupplier stmtSupplier, ResultSetFunction<T> fn) {
		this.stmtSupplier = stmtSupplier;
		this.resultToEdge = fn;
	}

	@Override
	public Iterator<T> iterator() {
		try {
			final PreparedStatement stmt = stmtSupplier.get();
			final List<T> results = new ArrayList<>();
			try (ResultSet rs = stmt.executeQuery()) {
				while (rs.next()) {
					results.add(resultToEdge.apply(rs));
				}
			}
			return results.iterator();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			return Collections.emptyIterator();
		}
	}
}