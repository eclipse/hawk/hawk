/*******************************************************************************
 * Copyright (c) 2015-2019 The University of York, Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.backend.tests;

import java.util.Collections;

import org.eclipse.hawk.backend.tests.factories.IGraphDatabaseFactory;
import org.eclipse.hawk.backend.tests.factories.OrientDatabaseFactory;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runners.Suite.SuiteClasses;

import com.github.peterwippermann.junit4.parameterizedsuite.ParameterContext;
import com.github.peterwippermann.junit4.parameterizedsuite.ParameterizedSuite;

/**
 * General test suite for basic requirements for all Hawk backends. This also
 * serves as a starting point for any other test case which should be run for
 * every backend available. Each backend other than the default one should
 * inherit from this class for its test suite.
 */
@RunWith(ParameterizedSuite.class)
@SuiteClasses({
	IndexTest.class,
	DatabaseManagementTest.class,
	GraphPopulationTest.class,
})
public class BackendTestSuite {

	@Parameters(name="{0}")
	public static Object[][] params() {
		return new Object[][] {
			{ new OrientDatabaseFactory() },
		};
	}

	public static Iterable<Object[]> caseParams() {
		if (ParameterContext.isParameterSet()) {
			return Collections.singletonList(ParameterContext.getParameter(Object[].class));
		} else {
			return Collections.singletonList(BackendTestSuite.params()[0]);
		}
	}

	@Parameter(0)
	public IGraphDatabaseFactory dbFactory;
}
