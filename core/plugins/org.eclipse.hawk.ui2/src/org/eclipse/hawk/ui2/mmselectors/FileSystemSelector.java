/*******************************************************************************
 * Copyright (c) 2015-2020 University of York, Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *    Antonio Garcia-Dominguez - initial API and implementation
 *******************************************************************************/
package org.eclipse.hawk.ui2.mmselectors;

import java.io.File;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.hawk.core.IModelIndexer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

public class FileSystemSelector implements IHawkMetaModelResourceSelector {

	@Override
	public boolean isApplicableTo(IModelIndexer indexer) {
		return true;
	}

	@Override
	public void selectMetamodels(Shell parentShell, IModelIndexer indexer) throws Exception {
		final FileDialog fd = new FileDialog(parentShell, SWT.MULTI);

		final IPath workspaceRoot = ResourcesPlugin.getWorkspace().getRoot().getLocation();
		fd.setFilterPath(workspaceRoot.toFile().toString());
		fd.setFilterExtensions(IHawkMetaModelResourceSelector.getKnownMetamodelFilePatterns(indexer));
		fd.setText("Select metamodels");
		String result = fd.open();

		if (result != null) {
			String[] metaModels = fd.getFileNames();
			File[] metaModelFiles = new File[metaModels.length];
			for (int i = 0; i < metaModels.length; i++) {
				metaModelFiles[i] = new File(fd.getFilterPath(), metaModels[i]);
			}

			indexer.registerMetamodels(metaModelFiles);
		}
	}


}
