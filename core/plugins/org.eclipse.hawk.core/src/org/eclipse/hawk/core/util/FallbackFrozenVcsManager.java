/*******************************************************************************
 * Copyright (c) 2023 The University of York.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 * 	   Antonio Garcia-Dominguez  - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.core.util;

import java.io.File;
import java.util.Collection;
import java.util.Collections;

import org.eclipse.hawk.core.ICredentialsStore;
import org.eclipse.hawk.core.IModelIndexer;
import org.eclipse.hawk.core.IVcsManager;
import org.eclipse.hawk.core.VcsCommitItem;
import org.eclipse.hawk.core.VcsRepositoryDelta;

public class FallbackFrozenVcsManager implements IVcsManager {

	private static final String DUMMY_REVISION = "0";

	private IModelIndexer indexer;
	private boolean isActive = false;
	private String location, username, password;

	@Override
	public String getHumanReadableName() {
		return "Fallback Frozen VCS";
	}

	@Override
	public String getCurrentRevision() throws Exception {
		return DUMMY_REVISION;
	}

	@Override
	public String getFirstRevision() throws Exception {
		return DUMMY_REVISION;
	}

	@Override
	public Collection<VcsCommitItem> getDelta(String startRevision) throws Exception {
		return Collections.emptySet();
	}

	@Override
	public VcsRepositoryDelta getDelta(String startRevision, String endRevision) throws Exception {
		VcsRepositoryDelta delta = new VcsRepositoryDelta(Collections.emptySet());
		delta.setManager(this);
		return delta;
	}

	@Override
	public File importFile(String revision, String path, File optionalTemp) {
		return new File(optionalTemp, "dummy");
	}

	@Override
	public boolean isActive() {
		return isActive;
	}

	@Override
	public void init(String vcsloc, IModelIndexer hawk) throws Exception {
		this.isActive = true;
		this.indexer = hawk;
		this.location = vcsloc;
	}

	@Override
	public void run() throws Exception {
		indexer.getConsole().printerrln("Using fallback frozen VCS for " + location);
	}

	@Override
	public void shutdown() {
		this.isActive = false;
		this.indexer = null;
		this.location = null;
	}

	@Override
	public String getLocation() {
		return location;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public void setCredentials(String username, String password, ICredentialsStore credStore) {
		this.username = username;
		this.password = password;
	}

	@Override
	public boolean isAuthSupported() {
		return false;
	}

	@Override
	public boolean isPathLocationAccepted() {
		return false;
	}

	@Override
	public boolean isURLLocationAccepted() {
		return false;
	}

	@Override
	public String getRepositoryPath(String rawPath) {
		return rawPath;
	}

	@Override
	public boolean isFrozen() {
		return true;
	}

	@Override
	public void setFrozen(boolean frozen) {
		indexer.getConsole().printerrln("setFrozen() called on fallback VCS, ignoring");
	}

}
