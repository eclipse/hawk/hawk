/*******************************************************************************
 * Copyright (c) 2020 Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *    Antonio Garcia-Dominguez - initial API and implementation
 *******************************************************************************/
package org.eclipse.hawk.ui.uml;

import org.eclipse.hawk.core.IVcsManager;
import org.eclipse.hawk.osgiserver.HModel;
import org.eclipse.hawk.ui2.dialog.HVCSDialog;
import org.eclipse.hawk.ui2.vcs.IVcsConfigurationBlock;
import org.eclipse.hawk.uml.vcs.PredefinedUMLLibraries;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

public class UMLConfigurationBlock implements IVcsConfigurationBlock {

	private HVCSDialog dialog;

	@Override
	public boolean isApplicableTo(IVcsManager manager) {
		return manager instanceof PredefinedUMLLibraries;
	}

	@Override
	public void createBlock(Composite container, HVCSDialog dialog) {
		this.dialog = dialog;

		container.setLayout(new FillLayout());
		Label label = new Label(container, SWT.NONE);
		label.setText("No options to configure.");
	}

	@Override
	public void okPressed(HModel hawkModel, boolean isFrozen) {
		final IVcsManager managerToEdit = dialog.getManagerToEdit();
		if (managerToEdit == null) {
			final String vcsType = dialog.getSelectedVCSManager().getClass().getName();
			hawkModel.addVCS("unused", vcsType, "", "", isFrozen);
		} else {
			managerToEdit.setFrozen(isFrozen);
		}
	}

}
