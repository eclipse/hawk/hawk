/*******************************************************************************
 * Copyright (c) 2015-2022 The University of York, Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.integration.tests.bpmn;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.concurrent.Callable;

import org.eclipse.hawk.backend.tests.factories.IGraphDatabaseFactory;
import org.eclipse.hawk.graph.syncValidationListener.SyncValidationListener;
import org.eclipse.hawk.integration.tests.IntegrationTestSuite;
import org.eclipse.hawk.integration.tests.ModelIndexingTest;
import org.eclipse.hawk.integration.tests.emf.EMFModelSupportFactory;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runners.Parameterized.Parameters;

/**
 * Integration test case that indexes a sequence of versions of a BPMN model.
 */
public class ModelVersioningTest extends ModelIndexingTest {

	@Rule
	public GraphChangeListenerRule<SyncValidationListener> syncValidation
		= new GraphChangeListenerRule<>(new SyncValidationListener());

	@Rule
	public TemporaryFolder modelFolder = new TemporaryFolder();

	@Parameters(name="{1}")
	public static Iterable<Object[]> params() {
		return IntegrationTestSuite.caseParams();
	}

	public ModelVersioningTest(File baseDir, IGraphDatabaseFactory dbf) {
		super(baseDir, dbf, new EMFModelSupportFactory());
	}

	private Path modelPath;

	public void prepare(String baseModel) throws Throwable {
		modelPath = new File(modelFolder.getRoot(), new File(baseModel).getName()).toPath();
		Files.copy(new File(baseDir, "resources/models/" + baseModel).toPath(), modelPath);
		requestFolderIndex(modelFolder.getRoot());

		scheduleAndWait(() -> {
			assertNoErrors(syncValidation.getListener());
			return null;
		});
	}

	@Test
	public void bpmn() throws Throwable {
		prepare("bpmn/v0-B.2.0.bpmn");
		final Callable<Object> noErrors = new Callable<Object>() {
			@Override
			public Object call() throws Exception {
				assertNoErrors(syncValidation.getListener());
				return null;
			}
		};

		for (int i = 1; i <= 8; i++) {
			replaceWith("bpmn/v" + i + "-B.2.0.bpmn");
			indexer.requestImmediateSync();
			scheduleAndWait(noErrors);
		}
	}

	private void replaceWith(final String replacement) throws IOException {
		final File replacementFile = new File(baseDir, "resources/models/" + replacement);
		Files.copy(replacementFile.toPath(), modelPath,
				StandardCopyOption.REPLACE_EXISTING);
		System.err.println("Copied " + replacementFile + " over " + modelPath);
	}
}
