/*******************************************************************************
 * Copyright (c) 2023 The University of York.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.epsilon.emc.wrappers;

import org.eclipse.hawk.epsilon.emc.EOLQueryEngine;
import org.eclipse.hawk.graph.Slot;

public class SlotWrapper {

	private final Slot slot;
	private final EOLQueryEngine model;

	public SlotWrapper(Slot slot, EOLQueryEngine eolQueryEngine) {
		this.slot = slot;
		this.model = eolQueryEngine;
	}

	public String getName() {
		return slot.getName();
	}

	public String getInstanceTypeName() {
		return slot.getType();
	}

	public TypeNodeWrapper getType() {
		return new TypeNodeWrapper(slot.getReferenceTargetTypeNode(), model);
	}

	public boolean isMany() {
		return slot.isMany();
	}

	public boolean isOrdered() {
		return slot.isOrdered();
	}

	public boolean isAttribute() {
		return slot.isAttribute();
	}

	public boolean isReference() {
		return slot.isReference();
	}

	public boolean isUnique() {
		return slot.isUnique();
	}

}
