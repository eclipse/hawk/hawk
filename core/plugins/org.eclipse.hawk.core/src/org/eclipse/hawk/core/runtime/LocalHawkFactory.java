/*******************************************************************************
 * Copyright (c) 2015 The University of York.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.core.runtime;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.hawk.core.IConsole;
import org.eclipse.hawk.core.ICredentialsStore;
import org.eclipse.hawk.core.IHawk;
import org.eclipse.hawk.core.IHawkFactory;
import org.eclipse.hawk.core.IHawkPlugin;
import org.eclipse.hawk.core.IStateListener.HawkState;
import org.eclipse.hawk.core.util.HawkProperties;
import org.eclipse.hawk.core.util.XStreamUtils;

import com.thoughtworks.xstream.XStream;

public class LocalHawkFactory implements IHawkFactory {

	/**
	 * Unique identifier for this factory, as defined in the <code>plugin.xml</code> file.
	 */
	public final static String ID = "org.eclipse.hawk.core.hawkFactory.local";

	@Override
	public IHawk create(String name, File localStorageFolder, String location, ICredentialsStore credStore, IConsole console, List<String> plugins) throws Exception {
		/**
		 * This implementation ignores the plugins parameter, as it is not OSGi-aware:
		 * the OSGi-awareness is done in the HModel class itself. 
		 */
		return new LocalHawk(name, localStorageFolder, credStore, console);
	}

	@Override
	public boolean instancesAreExtensible() {
		return true;
	}

	@Override
	public boolean instancesCreateGraph() {
		return true;
	}

	@Override
	public boolean instancesUseLocation() {
		return true;
	}

	@Override
	public InstanceInfo[] listInstances(String location) throws FileNotFoundException {
		final File basePath = new File(location);
		if (!basePath.exists()) {
			throw new FileNotFoundException(location + " does not exist");
		}
		if (!basePath.isDirectory()) {
			throw new IllegalArgumentException(location + " is not a directory");
		}

		final List<InstanceInfo> entries = new ArrayList<>();
		if (basePath.exists()) {
			for (File f : basePath.listFiles()) {
				if (f.isDirectory()) {
					File fProps = new File(f, "properties.xml");
					if (fProps.canRead()) {
						try {
							XStream stream = XStreamUtils.createXStreamLoader(HawkProperties.class);
							HawkProperties hp = (HawkProperties) stream.fromXML(fProps);
							entries.add(new InstanceInfo(f.getName(), hp.getDbType(), HawkState.STOPPED));
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
				}
			}
		} else {
			System.err.println(basePath + " does not exist: returning an empty set");
		}
		return entries.toArray(new InstanceInfo[entries.size()]);
	}

	@Override
	public List<String> listBackends(String location) throws Exception {
		// We can't say from here: ask elsewhere!
		return null;
	}

	@Override
	public List<IHawkPlugin> listPlugins(String location) throws Exception {
		// We can't say from here: ask elsewhere!
		return null;
	}

	@Override
	public boolean isRemote() {
		return false;
	}

	@Override
	public String getHumanReadableName() {
		return "Local Hawk";
	}

}
