/*******************************************************************************
 * Copyright (c) 2023 The University of York.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.sqlite.timeaware;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.hawk.core.graph.IGraphIterable;
import org.eclipse.hawk.core.graph.IGraphNode;
import org.eclipse.hawk.core.graph.timeaware.ITimeAwareGraphNode;
import org.eclipse.hawk.core.graph.timeaware.ITimeAwareGraphNodeIndex;
import org.eclipse.hawk.sqlite.AbstractSQLiteDatabase.SQLiteConnection;
import org.eclipse.hawk.sqlite.iteration.StatementGraphNodeIterable;
import org.eclipse.hawk.sqlite.iteration.StatementSupplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimeAwareSQLiteNodeIndex implements ITimeAwareGraphNodeIndex {

	private static final Logger LOGGER = LoggerFactory.getLogger(TimeAwareSQLiteNodeIndex.class);

	private final TimeAwareSQLiteDatabase db;
	private final String name;
	private final long time;

	public TimeAwareSQLiteNodeIndex(TimeAwareSQLiteDatabase db, String name, long time) {
		this.db = db;
		this.name = name;
		this.time = time;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public IGraphIterable<? extends IGraphNode> query(String key, Object valueOrPattern) {
		if (valueOrPattern instanceof String && ((String)valueOrPattern).contains("*")) {
			final String pattern = (String)valueOrPattern;

			if("*".equals(key) && "*".equals(pattern)) {
				return new StatementGraphNodeIterable<>(
					() -> db.getQueries().getQueryIndexValueAllPairsStatement(name, time),
					() -> db.getQueries().getQueryIndexValueAllPairsCountStatement(name, time),
					() -> db.getQueries().getQueryIndexValueAllPairsSingleStatement(name, time),
					this::getNodeById
				);
			} else if ("*".equals(pattern)) {
				return new StatementGraphNodeIterable<>(
					() -> db.getQueries().getQueryIndexValueAllValuesStatement(name, key, time),
					() -> db.getQueries().getQueryIndexValueAllValuesCountStatement(name, key, time),
					() -> db.getQueries().getQueryIndexValueAllValuesSingleStatement(name, key, time),
					this::getNodeById
				);
			} else {
				return new StatementGraphNodeIterable<>(
					() -> db.getQueries().getQueryIndexValuePatternStatement(name, key, pattern, time),
					() -> db.getQueries().getQueryIndexValuePatternCountStatement(name, key, pattern, time),
					() -> db.getQueries().getQueryIndexValuePatternSingleStatement(name, key, pattern, time),
					this::getNodeById
				);
			}
		}

		// Exact value
		return get(key, valueOrPattern);
	}

	@Override
	public IGraphIterable<? extends IGraphNode> query(String key, Number from, Number to, boolean fromInclusive, boolean toInclusive) {
		return new StatementGraphNodeIterable<>(
			() -> db.getQueries().getQueryIndexNumberRangeStatement(name, key, fromInclusive, from, toInclusive, to, time),
			() -> db.getQueries().getQueryIndexNumberRangeCountStatement(name, key, fromInclusive, from, toInclusive, to, time),
			() -> db.getQueries().getQueryIndexNumberRangeSingleStatement(name, key, fromInclusive, from, toInclusive, to, time),
			this::getNodeById
		);
	}

	@Override
	public IGraphIterable<? extends IGraphNode> get(String key, Object exactValue) {
		return new StatementGraphNodeIterable<>(
			() -> db.getQueries().getQueryIndexValueExactStatement(name, key, exactValue, time),
			() -> db.getQueries().getQueryIndexValueExactCountStatement(name, key, exactValue, time),
			() -> db.getQueries().getQueryIndexValueExactSingleStatement(name, key, exactValue, time),
			this::getNodeById
		);
	}

	@Override
	public void add(IGraphNode n, String key, Object value) {
		int rowCount = executeUpdate(() -> db.getQueries().getAddNodeIndexEntryStatement(name, key, (int) n.getId(), value, time));
		assert rowCount == 1 : "One row should be inserted when adding an index entry";
	}

	@Override
	public void remove(IGraphNode n) {
		executeUpdate(() -> db.getQueries().getRemoveNodeFromIndexStatement(name, (int) n.getId(), time));
	}

	@Override
	public void remove(IGraphNode n, String key, Object value) {
		if (key == null) {
			if (value == null) {
				remove(n);
			} else {
				executeUpdate(() -> db.getQueries().getRemoveNodeValueFromIndexStatement(name, (int) n.getId(), value, time));
			}
		} else if (value == null) {
			executeUpdate(() -> db.getQueries().getRemoveNodeFieldFromIndexStatement(name, (int) n.getId(), key, time));
		}

		executeUpdate(() -> db.getQueries().getRemoveNodeIndexEntryStatement(name, key, (int) n.getId(), value, time));
	}

	@Override
	public void flush() {
		// nothing to do
	}

	@Override
	public void delete() {
		try {
			final SQLiteConnection conn = db.getConnection();

			PreparedStatement stmt = db.getQueries().getDeleteNodeIndexStatement(name);
			int rowCount = stmt.executeUpdate();
			assert rowCount == 1 : "A row should have been deleted when deleting a node index";

			conn.getSchema().clearNodeIndexTable(name);
			conn.dropIndexTableOnCommit(name);
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	private int executeUpdate(StatementSupplier stmt) {
		try {
			return stmt.get().executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
		}
		return 0;
	}

	@Override
	public ITimeAwareGraphNodeIndex travelInTime(long timepoint) {
		return new TimeAwareSQLiteNodeIndex((TimeAwareSQLiteDatabase) db, getName(), timepoint);
	}

	@Override
	public List<Long> getVersions(ITimeAwareGraphNode n, String key, Object exactValue, long startTimepointIncluded) {
		final ITimeAwareGraphNode sqliteNode = (ITimeAwareGraphNode) n;
		final TimeAwareSQLiteDatabase taDB = (TimeAwareSQLiteDatabase) db;

		try {
			PreparedStatement stmt = taDB.getQueries().getIndexVersionsStatement(
				getName(), (int) sqliteNode.getId(), key, exactValue, startTimepointIncluded);

			try (ResultSet rs = stmt.executeQuery()) {
				List<Long> results = new ArrayList<>();
				while (rs.next()) {
					final long validFrom = rs.getLong(1);
					// DB ranges are [validFrom, validTo), getInstantsBetween is [from, to]
					final long validTo = rs.getObject(2) == null ? Long.MAX_VALUE : rs.getLong(2) - 1;

					// Use the time node to allow for composability (e.g. with whenAnnotated)
					List<Long> lInstants = n.getInstantsBetween(validFrom, validTo);
					results.addAll(lInstants);
				}
				return results;
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			return Collections.emptyList();
		}
	}

	@Override
	public Long getEarliestVersionSince(ITimeAwareGraphNode taNode, String key, Object exactValue) {
		final ITimeAwareGraphNode sqliteNode = (ITimeAwareGraphNode) taNode;
		final TimeAwareSQLiteDatabase taDB = (TimeAwareSQLiteDatabase) db;

		try {
			PreparedStatement stmt = taDB.getQueries().getIndexEarliestVersionSinceStatement(
				getName(), (int) sqliteNode.getId(), key, exactValue, sqliteNode.getTime());

			try (ResultSet rs = stmt.executeQuery()) {
				if (rs.next()) {
					final long validFrom = rs.getLong(1);
					// DB ranges are [validFrom, validTo), getInstantsBetween is [from, to]
					final long validTo = rs.getObject(2) == null ? Long.MAX_VALUE : rs.getLong(2) - 1;

					// Use the time node to allow for composability (e.g. with untilAnnotated)
					List<Long> lInstants = taNode.getInstantsBetween(validFrom, validTo);
					if (lInstants.isEmpty()) {
						return null;
					}
					return lInstants.get(lInstants.size() - 1);
				}
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
		}
		
		return ITimeAwareGraphNode.NO_SUCH_INSTANT;
	}

	@Override
	public void annotate(ITimeAwareGraphNode elementNode, String key) {
		executeUpdate(() -> db.getQueries().getAnnotateNodeIndexEntryStatement(name, key,
			(int) elementNode.getId(), true,
			elementNode.getTime() + 1, elementNode.getTime()));
	}

	private ITimeAwareGraphNode getNodeById(Object id) {
		return new TimeAwareSQLiteNode(db, (int) id, time);
	}
}
