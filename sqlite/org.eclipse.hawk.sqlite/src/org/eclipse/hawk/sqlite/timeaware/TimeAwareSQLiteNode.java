/*******************************************************************************
 * Copyright (c) 2023 The University of York.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.sqlite.timeaware;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.function.Function;

import org.eclipse.hawk.core.graph.IGraphEdge;
import org.eclipse.hawk.core.graph.IGraphNode;
import org.eclipse.hawk.core.graph.timeaware.ITimeAwareGraphNode;
import org.eclipse.hawk.sqlite.iteration.EndNodeEdgeIterable;
import org.eclipse.hawk.sqlite.iteration.StatementIterable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimeAwareSQLiteNode implements ITimeAwareGraphNode {

	private static final Logger LOGGER = LoggerFactory.getLogger(TimeAwareSQLiteNode.class);
	private final TimeAwareSQLiteDatabase db;
	private final int id;
	private final long time;

	public TimeAwareSQLiteNode(TimeAwareSQLiteDatabase db, int nodeId, long time) {
		this.db = db;
		this.id = nodeId;
		this.time = time;
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public Set<String> getPropertyKeys() {
		try {
			PreparedStatement stmt = db.getQueries().getNodePropKeysStatement(id, time);
			stmt.execute();
			return db.getConnection().getStrings(stmt);
		} catch (SQLException ex) {
			LOGGER.error(ex.getMessage(), ex);
			return Collections.emptySet();
		}
	}

	@Override
	public Object getProperty(String name) {
		try {
			PreparedStatement stmt = db.getQueries().getNodePropValueStatement(id, name, time);
			stmt.execute();
			return db.getConnection().getPropertyValue(stmt);
		} catch (SQLException | IOException | ClassNotFoundException e) {
			LOGGER.error(e.getMessage(), e);
		}

		return null;
	}

	@Override
	public void setProperty(String name, Object value) {
		if (value == null) {
			removeProperty(name);
		} else {
			try {
				value = db.getConnection().preprocessPropertyValue(value);
				PreparedStatement stmt = db.getQueries().getUpsertNodePropStatement(id, name, value, time);
				int rows = stmt.executeUpdate();
				assert rows == 1 : "One row should have been inserted or updated";
			} catch (SQLException | IOException ex) {
				LOGGER.error(ex.getMessage(), ex);
			}
		}
	}

	@Override
	public Iterable<IGraphEdge> getEdges() {
		return new StatementIterable<>(() -> db.getQueries().getEdgesStatement(id, time), (rs) -> {
			final int edgeId = rs.getInt(1);
			final int startId = rs.getInt(2);
			final int endId = rs.getInt(3);
			final String type = rs.getString(4);
			return new TimeAwareSQLiteEdge(db, edgeId, startId, endId, type, time);
		});
	}

	@Override
	public Iterable<IGraphEdge> getEdgesWithType(String type) {
		return new StatementIterable<>(() -> db.getQueries().getEdgesWithTypeStatement(type, id, time), (rs) -> {
			final int edgeId = rs.getInt(1);
			final int startId = rs.getInt(2);
			final int endId = rs.getInt(3);
			return new TimeAwareSQLiteEdge(db, edgeId, startId, endId, type, time);
		});
	}

	@Override
	public Iterable<IGraphEdge> getOutgoingWithType(String type) {
		return new StatementIterable<>(() -> db.getQueries().getOutgoingEdgesWithTypeStatement(type, id, time),
				(rs) -> {
					final int edgeId = rs.getInt(1);
					final int endId = rs.getInt(2);
					return new TimeAwareSQLiteEdge(db, edgeId, id, endId, type, time);
				});
	}

	@Override
	public Iterable<IGraphEdge> getOutgoingWithType(String type, Iterable<IGraphNode> endNodes) {
		Function<IGraphNode, IGraphEdge> endNodeToEdge = (node) -> {
			try {
				PreparedStatement stmt = db.getQueries()
					.getOutgoingEdgesWithTypeAndNodeStatement(type, id, (int) node.getId());

				try (ResultSet rs = stmt.executeQuery()) {
					if (rs.next()) {
						final int edgeId = rs.getInt(1);
						final int endId = rs.getInt(2);
						return new TimeAwareSQLiteEdge(db, edgeId, id, endId, type, time);
					}
				}
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
			}

			return null;
		};

		return new EndNodeEdgeIterable(endNodes, endNodeToEdge);
	}

	@Override
	public Iterable<IGraphEdge> getOutgoingWithTypeBetween(String type, long fromInclusive, long toInclusive) throws Exception {
		return new StatementIterable<>(() -> db.getQueries().getOutgoingEdgesWithTypeBetweenStatement(type, id, fromInclusive, toInclusive + 1),
			(rs) -> {
				final int edgeId = rs.getInt(1);
				final int endId = rs.getInt(2);
				final long validFrom = rs.getLong(3);
				return new TimeAwareSQLiteEdge(db, edgeId, id, endId, type, validFrom);
			});
	}

	@Override
	public Iterable<IGraphEdge> getIncomingWithTypeBetween(String type, long fromInclusive, long toInclusive) throws Exception {
		return new StatementIterable<>(() -> db.getQueries().getIncomingEdgesWithTypeBetweenStatement(type, id, fromInclusive, toInclusive + 1),
			(rs) -> {
				final int edgeId = rs.getInt(1);
				final int startId = rs.getInt(2);
				final long validFrom = rs.getLong(3);
				return new TimeAwareSQLiteEdge(db, edgeId, startId, id, type, validFrom);
			});
	}

	@Override
	public Iterable<IGraphEdge> getOutgoingWithTypeCreatedBetween(String type, long fromInclusive, long toInclusive) throws Exception {
		return new StatementIterable<>(() -> db.getQueries().getOutgoingEdgesWithTypeCreatedBetweenStatement(type, id, fromInclusive, toInclusive + 1),
			(rs) -> {
				final int edgeId = rs.getInt(1);
				final int endId = rs.getInt(2);
				final long validFrom = rs.getLong(3);
				return new TimeAwareSQLiteEdge(db, edgeId, id, endId, type, validFrom);
			});
	}

	@Override
	public Iterable<IGraphEdge> getIncomingWithTypeCreatedBetween(String type, long fromInclusive, long toInclusive) throws Exception {
		return new StatementIterable<>(() -> db.getQueries().getIncomingEdgesWithTypeCreatedBetweenStatement(type, id, fromInclusive, toInclusive + 1),
			(rs) -> {
				final int edgeId = rs.getInt(1);
				final int startId = rs.getInt(2);
				final long validFrom = rs.getLong(3);
				return new TimeAwareSQLiteEdge(db, edgeId, startId, id, type, validFrom);
			});
	}

	@Override
	public Iterable<IGraphEdge> getIncomingWithType(String type) {
		return new StatementIterable<>(() -> db.getQueries().getIncomingEdgesWithTypeStatement(type, id, time),
				(rs) -> {
					final int edgeId = rs.getInt(1);
					final int startId = rs.getInt(2);
					return new TimeAwareSQLiteEdge(db, edgeId, startId, id, type, time);
				});
	}

	@Override
	public Iterable<IGraphEdge> getIncoming() {
		return new StatementIterable<>(() -> db.getQueries().getIncomingEdgesStatement(id, time), (rs) -> {
			final int edgeId = rs.getInt(1);
			final int startId = rs.getInt(2);
			final String type = rs.getString(3);
			return new TimeAwareSQLiteEdge(db, edgeId, startId, id, type, time);
		});
	}

	@Override
	public Iterable<IGraphEdge> getOutgoing() {
		return new StatementIterable<>(() -> db.getQueries().getOutgoingEdgesStatement(id, time), (rs) -> {
			final int edgeId = rs.getInt(1);
			final int endId = rs.getInt(2);
			final String type = rs.getString(3);
			return new TimeAwareSQLiteEdge(db, edgeId, id, endId, type, time);
		});
	}

	@Override
	public void delete() {
		try {
			db.getQueries().getDeleteNodePropsStatement(id, time).executeUpdate();
			db.getQueries().getDeleteEdgePropsForNodeStatement(id, time).executeUpdate();
			db.getQueries().getDeleteEdgesForNodeStatement(id, time).executeUpdate();
			db.getQueries().getDeleteNodeStatement(id, time).executeUpdate();
			for (String idxName : db.getNodeIndexNames()) {
				db.getOrCreateNodeIndex(idxName).travelInTime(time).remove(this);
			}
		} catch (SQLException ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
	}

	@Override
	public TimeAwareSQLiteDatabase getGraph() {
		return db;
	}

	@Override
	public void removeProperty(String name) {
		try {
			PreparedStatement stmt = db.getQueries().getDeleteNodePropStatement(id, name, time);
			stmt.executeUpdate();
		} catch (SQLException ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
	}

	@Override
	public boolean isAlive() {
		try {
			PreparedStatement stmt = db.getQueries().getIsAliveStatement(id, time);
			try (ResultSet rs = stmt.executeQuery()) {
				return rs.getBoolean(1);
			}
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
			return false;
		}
	}

	@Override
	public long getTime() {
		return time;
	}

	@Override
	public List<Long> getAllInstants() throws Exception {
		StatementIterable<Long> iterInstants = new StatementIterable<>(
				() -> getGraph().getQueries().getAllInstantsForNodeStatement(getId()), (rs) -> {
					return rs.getLong(1);
				});

		List<Long> results = new ArrayList<>();
		iterInstants.forEach(results::add);
		return results;
	}

	@Override
	public long getEarliestInstant() throws Exception {
		return getInstant(db.getQueries().getEarliestInstantStatement(id));
	}

	@Override
	public long getPreviousInstant() throws Exception {
		return getInstant(db.getQueries().getPrevInstantStatement(id, time));
	}

	@Override
	public long getLatestInstant() throws Exception {
		return getInstant(db.getQueries().getLatestInstantStatement(id));
	}

	@Override
	public long getNextInstant() throws Exception {
		return getInstant(db.getQueries().getNextInstantStatement(id, time));
	}

	protected long getInstant(PreparedStatement stmt) throws SQLException {
		stmt.execute();
		try (ResultSet rs = stmt.getResultSet()) {
			if (rs.next()) {
				return rs.getLong(1);
			}
		}
		return ITimeAwareGraphNode.NO_SUCH_INSTANT;
	}

	@Override
	public void end() {
		travelInTime(time + 1).delete();
	}

	@Override
	public ITimeAwareGraphNode travelInTime(long time) {
		if (time == NO_SUCH_INSTANT) {
			return null;
		}
		return new TimeAwareSQLiteNode(getGraph(), getId(), time);
	}

	@Override
	public List<Long> getInstantsBetween(long fromInclusive, long toInclusive) {
		return getInstants(() -> db.getQueries().getInstantsBetweenStatement(id, fromInclusive, toInclusive));
	}

	@Override
	public List<Long> getInstantsFrom(long fromInclusive) {
		return getInstants(() -> db.getQueries().getInstantsFromStatement(id, fromInclusive));
	}

	@Override
	public List<Long> getInstantsUpTo(long toInclusive) {
		return getInstants(() -> db.getQueries().getInstantsUpToStatement(id, toInclusive));
	}

	protected List<Long> getInstants(Callable<PreparedStatement> supplier) {
		try {
			PreparedStatement stmt = supplier.call();
			try (ResultSet rs = stmt.executeQuery()) {
				final List<Long> results = new ArrayList<>();
				while (rs.next()) {
					results.add(rs.getLong(1));
				}
				return results;
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return Collections.emptyList();
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, time);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimeAwareSQLiteNode other = (TimeAwareSQLiteNode) obj;
		return id == other.id && time == other.time;
	}

	@Override
	public String toString() {
		return "TimeAwareSQLiteNode [id=" + id + ", time=" + time + "]";
	}

}
