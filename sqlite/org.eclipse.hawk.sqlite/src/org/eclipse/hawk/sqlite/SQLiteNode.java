/*******************************************************************************
 * Copyright (c) 2022-2023 The University of York.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.sqlite;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

import org.eclipse.hawk.core.graph.IGraphDatabase;
import org.eclipse.hawk.core.graph.IGraphEdge;
import org.eclipse.hawk.core.graph.IGraphNode;
import org.eclipse.hawk.sqlite.AbstractSQLiteDatabase.SQLiteConnection;
import org.eclipse.hawk.sqlite.iteration.EndNodeEdgeIterable;
import org.eclipse.hawk.sqlite.iteration.StatementIterable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SQLiteNode implements IGraphNode {

	private static final Logger LOGGER = LoggerFactory.getLogger(SQLiteNode.class);
	private final AbstractSQLiteDatabase db;
	private final int id;

	public SQLiteNode(AbstractSQLiteDatabase db, int nodeId) {
		this.db = db;
		this.id = nodeId;
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public Set<String> getPropertyKeys() {
		try {
			PreparedStatement stmt = db.getConnection().getQueries().getNodePropKeysStatement(id);
			stmt.execute();
			return db.getConnection().getStrings(stmt);
		} catch (SQLException ex) {
			LOGGER.error(ex.getMessage(), ex);
			return Collections.emptySet();
		}
	}

	@Override
	public Object getProperty(String name) {
		try {
			final SQLiteConnection conn = db.getConnection();
			PreparedStatement stmt = conn.getQueries().getNodePropValueStatement(id, name);
			stmt.execute();

			return conn.getPropertyValue(stmt);
		} catch (SQLException|IOException|ClassNotFoundException e) {
			LOGGER.error(e.getMessage(), e);
		}
		
		return null;
	}

	@Override
	public void setProperty(String name, Object value) {
		if (value == null) {
			removeProperty(name);
		} else {
			try {
				final SQLiteConnection conn = db.getConnection();
				value = conn.preprocessPropertyValue(value);
				PreparedStatement stmt = conn.getQueries().getUpsertNodePropStatement(id, name, value);
				int rows = stmt.executeUpdate();
				assert rows == 1 : "One row should have been inserted or updated";
			} catch (SQLException|IOException ex) {
				LOGGER.error(ex.getMessage(), ex);
			}
		}
	}

	@Override
	public Iterable<IGraphEdge> getEdges() {
		return new StatementIterable<>(
			() -> db.getConnection().getQueries().getEdgesStatement(id),
			(rs) -> {
				final int edgeId = rs.getInt(1);
				final int startId = rs.getInt(2);
				final int endId = rs.getInt(3);
				final String type = rs.getString(4);
				return new SQLiteEdge(db, edgeId, startId, endId, type);
			}
		);
	}

	@Override
	public Iterable<IGraphEdge> getEdgesWithType(String type) {
		return new StatementIterable<>(
			() -> db.getConnection().getQueries().getEdgesWithTypeStatement(type, id),
			(rs) -> {
				final int edgeId = rs.getInt(1);
				final int startId = rs.getInt(2);
				final int endId = rs.getInt(3);
				return new SQLiteEdge(db, edgeId, startId, endId, type);
			}
		);
	}

	@Override
	public Iterable<IGraphEdge> getOutgoingWithType(String type) {
		return new StatementIterable<>(
			() -> db.getConnection().getQueries().getOutgoingEdgesWithTypeStatement(type, id),
			(rs) -> {
				final int edgeId = rs.getInt(1);
				final int endId = rs.getInt(2);
				return new SQLiteEdge(db, edgeId, id, endId, type);
			});
	}

	@Override
	public Iterable<IGraphEdge> getOutgoingWithType(String type, Iterable<IGraphNode> endNodes) {
		Function<IGraphNode, IGraphEdge> endNodeToEdge = (node) -> {
			try {
				PreparedStatement stmt = db.getConnection().getQueries()
					.getOutgoingEdgesWithTypeAndNodeStatement(type, id, (int) node.getId());
				stmt.execute();

				try (ResultSet rs = stmt.getResultSet()) {
					if (rs.next()) {
						final int edgeId = rs.getInt(1);
						final int endId = rs.getInt(2);
						return new SQLiteEdge(db, edgeId, id, endId, type);
					}
				}
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
			}

			return null;
		};

		return new EndNodeEdgeIterable(endNodes, endNodeToEdge);
	}
	
	@Override
	public Iterable<IGraphEdge> getIncomingWithType(String type) {
		return new StatementIterable<>(
			() -> db.getConnection().getQueries().getIncomingEdgesWithTypeStatement(type, id),
			(rs) -> {
				final int edgeId = rs.getInt(1);
				final int startId = rs.getInt(2);
				return new SQLiteEdge(db, edgeId, startId, id, type);
			});
	}

	@Override
	public Iterable<IGraphEdge> getIncoming() {
		return new StatementIterable<>(
			() -> db.getConnection().getQueries().getIncomingEdgesStatement(id),
			(rs) -> {
				final int edgeId = rs.getInt(1);
				final int startId = rs.getInt(2);
				final String type = rs.getString(3);
				return new SQLiteEdge(db, edgeId, startId, id, type);
			}
		);
	}

	@Override
	public Iterable<IGraphEdge> getOutgoing() {
		return new StatementIterable<>(
			() -> db.getConnection().getQueries().getOutgoingEdgesStatement(id),
			(rs) -> {
				final int edgeId = rs.getInt(1);
				final int endId = rs.getInt(2);
				final String type = rs.getString(3);
				return new SQLiteEdge(db, edgeId, id, endId, type);
			}
		);
	}

	@Override
	public void delete() {
		try {
			final SQLiteConnection conn = db.getConnection();
			conn.getQueries().getDeleteNodePropsStatement(id).executeUpdate();
			conn.getQueries().getDeleteEdgePropsForNodeStatement(id).executeUpdate();
			conn.getQueries().getDeleteEdgesForNodeStatement(id).executeUpdate();
			conn.getQueries().getDeleteNodeStatement(id).executeUpdate();

			for (String idxName : db.getNodeIndexNames()) {
				db.getOrCreateNodeIndex(idxName).remove(this);
			}
		} catch (SQLException ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
	}

	@Override
	public IGraphDatabase getGraph() {
		return db;
	}

	@Override
	public void removeProperty(String name) {
		try {
			PreparedStatement stmt = db.getConnection().getQueries().getDeleteNodePropStatement(id, name);
			stmt.executeUpdate();
		} catch (SQLException ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SQLiteNode other = (SQLiteNode) obj;
		return id == other.id;
	}

	@Override
	public String toString() {
		return String.format("SQLiteNode [id=%s]", id);
	}
}
