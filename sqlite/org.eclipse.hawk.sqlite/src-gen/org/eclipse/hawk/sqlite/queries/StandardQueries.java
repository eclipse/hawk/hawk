/*******************************************************************************
* Copyright (c) 2022-2023 The University of York.
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* http://www.eclipse.org/legal/epl-2.0.
*
* This Source Code may also be made available under the following Secondary
* Licenses when the conditions for such availability set forth in the Eclipse
* Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
*
* SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
*
* Contributors:
*     Antonio Garcia-Dominguez - initial API and implementation
******************************************************************************/
package org.eclipse.hawk.sqlite.queries;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Blob;

import java.util.Map;
import java.util.HashMap;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
* Automatically generated query set. DO NOT EDIT MANUALLY.
* @generated
*/
public class StandardQueries implements IQueries {
	public static final String TYPE_BLOB_BASE64 = "blobBase64";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(StandardQueries.class);
	
	private final Connection connection;
	private PreparedStatement stmtInsertNode;
	private PreparedStatement stmtNodeIDsByLabel;
	private PreparedStatement stmtNodeCountByLabel;
	private PreparedStatement stmtFirstNodeIDByLabel;
	private PreparedStatement stmtNodePropKeys;
	private PreparedStatement stmtNodePropValue;
	private PreparedStatement stmtUpsertNodeProp;
	private PreparedStatement stmtDeleteNodeProp;
	private PreparedStatement stmtDeleteNode;
	private PreparedStatement stmtDeleteNodeProps;
	private PreparedStatement stmtInsertEdge;
	private PreparedStatement stmtDeleteEdge;
	private PreparedStatement stmtOutgoingEdgesWithType;
	private PreparedStatement stmtOutgoingEdgesWithTypeAndNode;
	private PreparedStatement stmtOutgoingEdges;
	private PreparedStatement stmtIncomingEdgesWithType;
	private PreparedStatement stmtIncomingEdges;
	private PreparedStatement stmtEdges;
	private PreparedStatement stmtEdgesWithType;
	private PreparedStatement stmtEdgePropKeys;
	private PreparedStatement stmtEdgePropValue;
	private PreparedStatement stmtUpsertEdgeProp;
	private PreparedStatement stmtDeleteEdgeProp;
	private PreparedStatement stmtDeleteEdgeProps;
	private PreparedStatement stmtDeleteEdgePropsForNode;
	private PreparedStatement stmtDeleteEdgesForNode;
	private PreparedStatement stmtUpsertNodeIndex;
	private PreparedStatement stmtDeleteNodeIndex;
	private PreparedStatement stmtAllNodeIndices;
	private Map<String, PreparedStatement> stmtAddNodeIndexEntry = new HashMap<>();
	private Map<String, PreparedStatement> stmtRemoveNodeIndexEntry = new HashMap<>();
	private Map<String, PreparedStatement> stmtRemoveNodeFromIndex = new HashMap<>();
	private Map<String, PreparedStatement> stmtRemoveNodeFieldFromIndex = new HashMap<>();
	private Map<String, PreparedStatement> stmtRemoveNodeValueFromIndex = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValuePattern = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValuePatternCount = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValuePatternSingle = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValueExact = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValueExactCount = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValueExactSingle = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValueAllValues = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValueAllValuesCount = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValueAllValuesSingle = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValueAllPairs = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValueAllPairsCount = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValueAllPairsSingle = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexNumberRange = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexNumberRangeCount = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexNumberRangeSingle = new HashMap<>();
	
	public StandardQueries(Connection connection) {
		this.connection = connection;
	}
	
	@Override
	public PreparedStatement getInsertNodeStatement(String label) throws SQLException {
		if (stmtInsertNode == null) {
			stmtInsertNode = connection.prepareStatement("INSERT INTO nodes (label) VALUES (?);");
		}
		stmtInsertNode.setString(1, label);
		return stmtInsertNode;
	}
	
	@Override
	public PreparedStatement getNodeIDsByLabelStatement(String label) throws SQLException {
		if (stmtNodeIDsByLabel == null) {
			stmtNodeIDsByLabel = connection.prepareStatement("SELECT rowid FROM nodes WHERE label = ?;");
		}
		stmtNodeIDsByLabel.setString(1, label);
		return stmtNodeIDsByLabel;
	}
	
	@Override
	public PreparedStatement getNodeCountByLabelStatement(String label) throws SQLException {
		if (stmtNodeCountByLabel == null) {
			stmtNodeCountByLabel = connection.prepareStatement("SELECT COUNT(1) FROM nodes WHERE label = ?;");
		}
		stmtNodeCountByLabel.setString(1, label);
		return stmtNodeCountByLabel;
	}
	
	@Override
	public PreparedStatement getFirstNodeIDByLabelStatement(String label) throws SQLException {
		if (stmtFirstNodeIDByLabel == null) {
			stmtFirstNodeIDByLabel = connection.prepareStatement("SELECT rowid FROM nodes WHERE label = ? LIMIT 1;");
		}
		stmtFirstNodeIDByLabel.setString(1, label);
		return stmtFirstNodeIDByLabel;
	}
	
	@Override
	public PreparedStatement getNodePropKeysStatement(int nodeID) throws SQLException {
		if (stmtNodePropKeys == null) {
			stmtNodePropKeys = connection.prepareStatement("SELECT key FROM nodeprops WHERE nodeid = ?;");
		}
		stmtNodePropKeys.setInt(1, nodeID);
		return stmtNodePropKeys;
	}
	
	@Override
	public PreparedStatement getNodePropValueStatement(int nodeID, String name) throws SQLException {
		if (stmtNodePropValue == null) {
			stmtNodePropValue = connection.prepareStatement("SELECT type, value FROM nodeprops WHERE nodeid = ? AND key = ? LIMIT 1;");
		}
		stmtNodePropValue.setInt(1, nodeID);
		stmtNodePropValue.setString(2, name);
		return stmtNodePropValue;
	}
	
	@Override
	public PreparedStatement getUpsertNodePropStatement(int nodeID, String key, Object value) throws SQLException, IOException {
		if (stmtUpsertNodeProp == null) {
			stmtUpsertNodeProp = connection.prepareStatement("INSERT INTO nodeprops (nodeid, key, type, value) VALUES (?, ?, ?, ?) ON CONFLICT (nodeid, key) DO UPDATE SET value = ?;");
		}
		stmtUpsertNodeProp.setInt(1, nodeID);
		stmtUpsertNodeProp.setString(2, key);
		stmtUpsertNodeProp.setString(3, getPropertyType(value));
		if (value instanceof Blob) {
			String value_base64 = base64(value);
			stmtUpsertNodeProp.setString(4, value_base64);
			stmtUpsertNodeProp.setString(5, value_base64);
		} else {
			stmtUpsertNodeProp.setObject(4, value);
			stmtUpsertNodeProp.setObject(5, value);
		}
		return stmtUpsertNodeProp;
	}
	
	@Override
	public PreparedStatement getDeleteNodePropStatement(int nodeID, String name) throws SQLException {
		if (stmtDeleteNodeProp == null) {
			stmtDeleteNodeProp = connection.prepareStatement("DELETE FROM nodeprops WHERE nodeid = ? AND key = ?;");
		}
		stmtDeleteNodeProp.setInt(1, nodeID);
		stmtDeleteNodeProp.setString(2, name);
		return stmtDeleteNodeProp;
	}
	
	@Override
	public PreparedStatement getDeleteNodeStatement(int nodeID) throws SQLException {
		if (stmtDeleteNode == null) {
			stmtDeleteNode = connection.prepareStatement("DELETE FROM nodes WHERE rowid = ?;");
		}
		stmtDeleteNode.setInt(1, nodeID);
		return stmtDeleteNode;
	}
	
	@Override
	public PreparedStatement getDeleteNodePropsStatement(int nodeID) throws SQLException {
		if (stmtDeleteNodeProps == null) {
			stmtDeleteNodeProps = connection.prepareStatement("DELETE FROM nodeprops WHERE nodeid = ?;");
		}
		stmtDeleteNodeProps.setInt(1, nodeID);
		return stmtDeleteNodeProps;
	}
	
	@Override
	public PreparedStatement getInsertEdgeStatement(int startID, int endID, String label) throws SQLException {
		if (stmtInsertEdge == null) {
			stmtInsertEdge = connection.prepareStatement("INSERT INTO edges (startId, endId, label) VALUES (?, ?, ?) ON CONFLICT DO NOTHING;");
		}
		stmtInsertEdge.setInt(1, startID);
		stmtInsertEdge.setInt(2, endID);
		stmtInsertEdge.setString(3, label);
		return stmtInsertEdge;
	}
	
	@Override
	public PreparedStatement getDeleteEdgeStatement(int edgeID) throws SQLException {
		if (stmtDeleteEdge == null) {
			stmtDeleteEdge = connection.prepareStatement("DELETE FROM edges WHERE rowid = ?;");
		}
		stmtDeleteEdge.setInt(1, edgeID);
		return stmtDeleteEdge;
	}
	
	@Override
	public PreparedStatement getOutgoingEdgesWithTypeStatement(String type, int fromID) throws SQLException {
		if (stmtOutgoingEdgesWithType == null) {
			stmtOutgoingEdgesWithType = connection.prepareStatement("SELECT rowid, endId FROM edges WHERE label = ? AND startId = ?;");
		}
		stmtOutgoingEdgesWithType.setString(1, type);
		stmtOutgoingEdgesWithType.setInt(2, fromID);
		return stmtOutgoingEdgesWithType;
	}
	
	@Override
	public PreparedStatement getOutgoingEdgesWithTypeAndNodeStatement(String type, int fromID, int toID) throws SQLException {
		if (stmtOutgoingEdgesWithTypeAndNode == null) {
			stmtOutgoingEdgesWithTypeAndNode = connection.prepareStatement("SELECT rowid, endId FROM edges WHERE label = ? AND startId = ? AND endId = ?;");
		}
		stmtOutgoingEdgesWithTypeAndNode.setString(1, type);
		stmtOutgoingEdgesWithTypeAndNode.setInt(2, fromID);
		stmtOutgoingEdgesWithTypeAndNode.setInt(3, toID);
		return stmtOutgoingEdgesWithTypeAndNode;
	}
	
	@Override
	public PreparedStatement getOutgoingEdgesStatement(int fromID) throws SQLException {
		if (stmtOutgoingEdges == null) {
			stmtOutgoingEdges = connection.prepareStatement("SELECT rowid, endId, label FROM edges WHERE startId = ?;");
		}
		stmtOutgoingEdges.setInt(1, fromID);
		return stmtOutgoingEdges;
	}
	
	@Override
	public PreparedStatement getIncomingEdgesWithTypeStatement(String type, int toID) throws SQLException {
		if (stmtIncomingEdgesWithType == null) {
			stmtIncomingEdgesWithType = connection.prepareStatement("SELECT rowid, startId FROM edges WHERE label = ? AND endId = ?;");
		}
		stmtIncomingEdgesWithType.setString(1, type);
		stmtIncomingEdgesWithType.setInt(2, toID);
		return stmtIncomingEdgesWithType;
	}
	
	@Override
	public PreparedStatement getIncomingEdgesStatement(int toID) throws SQLException {
		if (stmtIncomingEdges == null) {
			stmtIncomingEdges = connection.prepareStatement("SELECT rowid, startId, label FROM edges WHERE endId = ?;");
		}
		stmtIncomingEdges.setInt(1, toID);
		return stmtIncomingEdges;
	}
	
	@Override
	public PreparedStatement getEdgesStatement(int startOrEndID) throws SQLException {
		if (stmtEdges == null) {
			stmtEdges = connection.prepareStatement("SELECT rowid, startId, endId, label FROM edges WHERE startId = ? OR endId = ?;");
		}
		stmtEdges.setInt(1, startOrEndID);
		stmtEdges.setInt(2, startOrEndID);
		return stmtEdges;
	}
	
	@Override
	public PreparedStatement getEdgesWithTypeStatement(String type, int startOrEndID) throws SQLException {
		if (stmtEdgesWithType == null) {
			stmtEdgesWithType = connection.prepareStatement("SELECT rowid, startId, endid FROM edges WHERE label = ? AND (startId = ? OR endId = ?);");
		}
		stmtEdgesWithType.setString(1, type);
		stmtEdgesWithType.setInt(2, startOrEndID);
		stmtEdgesWithType.setInt(3, startOrEndID);
		return stmtEdgesWithType;
	}
	
	@Override
	public PreparedStatement getEdgePropKeysStatement(int edgeID) throws SQLException {
		if (stmtEdgePropKeys == null) {
			stmtEdgePropKeys = connection.prepareStatement("SELECT key FROM edgeprops WHERE edgeid = ?;");
		}
		stmtEdgePropKeys.setInt(1, edgeID);
		return stmtEdgePropKeys;
	}
	
	@Override
	public PreparedStatement getEdgePropValueStatement(int edgeID, String name) throws SQLException {
		if (stmtEdgePropValue == null) {
			stmtEdgePropValue = connection.prepareStatement("SELECT type, value FROM edgeprops WHERE edgeid = ? AND key = ? LIMIT 1;");
		}
		stmtEdgePropValue.setInt(1, edgeID);
		stmtEdgePropValue.setString(2, name);
		return stmtEdgePropValue;
	}
	
	@Override
	public PreparedStatement getUpsertEdgePropStatement(int edgeID, String key, Object value) throws SQLException, IOException {
		if (stmtUpsertEdgeProp == null) {
			stmtUpsertEdgeProp = connection.prepareStatement("INSERT INTO edgeprops (edgeid, key, type, value) VALUES (?, ?, ?, ?) ON CONFLICT (edgeid, key) DO UPDATE SET value = ?;");
		}
		stmtUpsertEdgeProp.setInt(1, edgeID);
		stmtUpsertEdgeProp.setString(2, key);
		stmtUpsertEdgeProp.setString(3, getPropertyType(value));
		if (value instanceof Blob) {
			String value_base64 = base64(value);
			stmtUpsertEdgeProp.setString(4, value_base64);
			stmtUpsertEdgeProp.setString(5, value_base64);
		} else {
			stmtUpsertEdgeProp.setObject(4, value);
			stmtUpsertEdgeProp.setObject(5, value);
		}
		return stmtUpsertEdgeProp;
	}
	
	@Override
	public PreparedStatement getDeleteEdgePropStatement(int edgeID, String name) throws SQLException {
		if (stmtDeleteEdgeProp == null) {
			stmtDeleteEdgeProp = connection.prepareStatement("DELETE FROM edgeprops WHERE edgeId = ? AND key = ?;");
		}
		stmtDeleteEdgeProp.setInt(1, edgeID);
		stmtDeleteEdgeProp.setString(2, name);
		return stmtDeleteEdgeProp;
	}
	
	@Override
	public PreparedStatement getDeleteEdgePropsStatement(int edgeID) throws SQLException {
		if (stmtDeleteEdgeProps == null) {
			stmtDeleteEdgeProps = connection.prepareStatement("DELETE FROM edgeprops WHERE edgeId = ?;");
		}
		stmtDeleteEdgeProps.setInt(1, edgeID);
		return stmtDeleteEdgeProps;
	}
	
	@Override
	public PreparedStatement getDeleteEdgePropsForNodeStatement(int nodeID) throws SQLException {
		if (stmtDeleteEdgePropsForNode == null) {
			stmtDeleteEdgePropsForNode = connection.prepareStatement("DELETE FROM edgeprops WHERE edgeid IN ( SELECT rowid FROM edges WHERE startId = ? OR endId = ? );");
		}
		stmtDeleteEdgePropsForNode.setInt(1, nodeID);
		stmtDeleteEdgePropsForNode.setInt(2, nodeID);
		return stmtDeleteEdgePropsForNode;
	}
	
	@Override
	public PreparedStatement getDeleteEdgesForNodeStatement(int nodeID) throws SQLException {
		if (stmtDeleteEdgesForNode == null) {
			stmtDeleteEdgesForNode = connection.prepareStatement("DELETE FROM edges WHERE startId = ? OR endId = ?;");
		}
		stmtDeleteEdgesForNode.setInt(1, nodeID);
		stmtDeleteEdgesForNode.setInt(2, nodeID);
		return stmtDeleteEdgesForNode;
	}
	
	@Override
	public PreparedStatement getUpsertNodeIndexStatement(String name) throws SQLException {
		if (stmtUpsertNodeIndex == null) {
			stmtUpsertNodeIndex = connection.prepareStatement("INSERT INTO nodeindices (name) VALUES (?) ON CONFLICT (name) DO NOTHING;");
		}
		stmtUpsertNodeIndex.setString(1, name);
		return stmtUpsertNodeIndex;
	}
	
	@Override
	public PreparedStatement getDeleteNodeIndexStatement(String name) throws SQLException {
		if (stmtDeleteNodeIndex == null) {
			stmtDeleteNodeIndex = connection.prepareStatement("DELETE FROM nodeindices WHERE name = ?;");
		}
		stmtDeleteNodeIndex.setString(1, name);
		return stmtDeleteNodeIndex;
	}
	
	@Override
	public PreparedStatement getAllNodeIndicesStatement() throws SQLException {
		if (stmtAllNodeIndices == null) {
			stmtAllNodeIndices = connection.prepareStatement("SELECT name FROM nodeindices;");
		}
		return stmtAllNodeIndices;
	}
	
	@Override
	public PreparedStatement getAddNodeIndexEntryStatement(String indexName, String key, int nodeId, Object value) throws SQLException {
		PreparedStatement stmt = stmtAddNodeIndexEntry.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("INSERT INTO `idx_%s` (key, nodeId, value) VALUES (?, ?, ?);", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setInt(2, nodeId);
		stmt.setObject(3, value);
		return stmt;
	}
	
	@Override
	public PreparedStatement getRemoveNodeIndexEntryStatement(String indexName, String key, int nodeId, Object value) throws SQLException {
		PreparedStatement stmt = stmtRemoveNodeIndexEntry.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("DELETE FROM `idx_%s` WHERE key = ? AND nodeId = ? AND value = ?;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setInt(2, nodeId);
		stmt.setObject(3, value);
		return stmt;
	}
	
	@Override
	public PreparedStatement getRemoveNodeFromIndexStatement(String indexName, int nodeId) throws SQLException {
		PreparedStatement stmt = stmtRemoveNodeFromIndex.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("DELETE FROM `idx_%s` WHERE nodeId = ?;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setInt(1, nodeId);
		return stmt;
	}
	
	@Override
	public PreparedStatement getRemoveNodeFieldFromIndexStatement(String indexName, int nodeId, String key) throws SQLException {
		PreparedStatement stmt = stmtRemoveNodeFieldFromIndex.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("DELETE FROM `idx_%s` WHERE nodeId = ? AND key = ?;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setInt(1, nodeId);
		stmt.setString(2, key);
		return stmt;
	}
	
	@Override
	public PreparedStatement getRemoveNodeValueFromIndexStatement(String indexName, int nodeId, Object value) throws SQLException {
		PreparedStatement stmt = stmtRemoveNodeValueFromIndex.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("DELETE FROM `idx_%s` WHERE nodeId = ? AND value = ?;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setInt(1, nodeId);
		stmt.setObject(2, value);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValuePatternStatement(String indexName, String key, String value) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValuePattern.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT DISTINCT nodeId FROM `idx_%s` WHERE key = ? AND value GLOB ?;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setString(2, value);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValuePatternCountStatement(String indexName, String key, String value) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValuePatternCount.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT DISTINCT COUNT(1) FROM `idx_%s` WHERE key = ? AND value GLOB ?;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setString(2, value);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValuePatternSingleStatement(String indexName, String key, String value) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValuePatternSingle.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT nodeId FROM `idx_%s` WHERE key = ? AND value GLOB ? LIMIT 1;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setString(2, value);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValueExactStatement(String indexName, String key, Object value) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValueExact.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT DISTINCT nodeId FROM `idx_%s` WHERE key = ? AND value = ?;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setObject(2, value);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValueExactCountStatement(String indexName, String key, Object value) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValueExactCount.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT COUNT(DISTINCT nodeId) FROM `idx_%s` WHERE key = ? AND value = ?;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setObject(2, value);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValueExactSingleStatement(String indexName, String key, Object value) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValueExactSingle.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT nodeId FROM `idx_%s` WHERE key = ? AND value = ? LIMIT 1;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setObject(2, value);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValueAllValuesStatement(String indexName, String key) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValueAllValues.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT DISTINCT nodeId FROM `idx_%s` WHERE key = ?;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValueAllValuesCountStatement(String indexName, String key) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValueAllValuesCount.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT COUNT(DISTINCT nodeId) FROM `idx_%s` WHERE key = ?;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValueAllValuesSingleStatement(String indexName, String key) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValueAllValuesSingle.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT nodeId FROM `idx_%s` WHERE key = ? LIMIT 1;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValueAllPairsStatement(String indexName) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValueAllPairs.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT DISTINCT nodeId FROM `idx_%s`;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValueAllPairsCountStatement(String indexName) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValueAllPairsCount.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT COUNT(DISTINCT nodeId) FROM `idx_%s`;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValueAllPairsSingleStatement(String indexName) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValueAllPairsSingle.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT nodeId FROM `idx_%s` LIMIT 1;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexNumberRangeStatement(String indexName, String key, boolean fromInclusive, Number from, boolean toInclusive, Number to) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexNumberRange.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT DISTINCT nodeId FROM `idx_%s` WHERE key = ? AND (? AND value >= ? OR value > ?) AND (? AND value <= ? OR value < ?);", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setBoolean(2, fromInclusive);
		stmt.setObject(3, from);
		stmt.setObject(4, from);
		stmt.setBoolean(5, toInclusive);
		stmt.setObject(6, to);
		stmt.setObject(7, to);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexNumberRangeCountStatement(String indexName, String key, boolean fromInclusive, Number from, boolean toInclusive, Number to) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexNumberRangeCount.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT COUNT(DISTINCT nodeId) FROM `idx_%s` WHERE key = ? AND (? AND value >= ? OR value > ?) AND (? AND value <= ? OR value < ?);", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setBoolean(2, fromInclusive);
		stmt.setObject(3, from);
		stmt.setObject(4, from);
		stmt.setBoolean(5, toInclusive);
		stmt.setObject(6, to);
		stmt.setObject(7, to);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexNumberRangeSingleStatement(String indexName, String key, boolean fromInclusive, Number from, boolean toInclusive, Number to) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexNumberRangeSingle.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT DISTINCT nodeId FROM `idx_%s` WHERE key = ? AND (? AND value >= ? OR value > ?) AND (? AND value <= ? OR value < ?) LIMIT 1;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setBoolean(2, fromInclusive);
		stmt.setObject(3, from);
		stmt.setObject(4, from);
		stmt.setBoolean(5, toInclusive);
		stmt.setObject(6, to);
		stmt.setObject(7, to);
		return stmt;
	}
	
	protected String getPropertyType(Object value) {
		if (value instanceof Blob) {
			return TYPE_BLOB_BASE64;
		} else {
			return value == null ? "unknown" : value.getClass().getSimpleName();
		}
	}
	
	/* From https://www.baeldung.com/java-inputstream-to-outputstream (Java 8 alternative to InputStream.transferTo) */
	protected void copy(InputStream source, OutputStream target) throws IOException {
		byte[] buf = new byte[8192];
		int length;
		while ((length = source.read(buf)) > 0) {
			target.write(buf, 0, length);
		}
	}
	
	/**
	|* Xerial SQLite JDBC driver does not support BLOB - we encode those as base64 strings.
	|* @throws SQLException There was an error reading the blob.
	|* @throws IOException There was an error copying the bytes.
	\*/
	protected String base64(Object value) throws IOException, SQLException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		copy(((Blob) value).getBinaryStream(), bos);
		return Base64.encodeBase64String(bos.toByteArray());
	}
	
}