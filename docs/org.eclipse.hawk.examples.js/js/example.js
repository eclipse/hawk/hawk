(() => {
  let transport = new Thrift.Transport(
    "http://localhost:8080/thrift/hawk/json",
  );
  let protocol = new Thrift.Protocol(transport);
  let client = new HawkClient(protocol);

  let reportThriftError = function (jq, err) {
    jq.html("Error while invoking Thrift. See log.");
    jq.css("color", "red");
    console.error("Error during Thrift call", err);
  };

  $("#listInstances").on("click", function () {
    let jq = $("#listInstancesResult");
    try {
      result = client.listInstances();
      jq.html(
        "Result: <pre>" + JSON.stringify(result, undefined, 2) + "</pre>"
      );
      jq.css("color", "black");
    } catch (err) {
      reportThriftError(jq, err);
    }
  });

  $("#runQuery").on("click", function () {
    let jq = $("#queryResult");
    let instanceName = $("#instanceName").val();
    let query = $("#queryText").val();
    let queryLanguage = $("#queryLanguage").val();
    let qOptions = new HawkQueryOptions();

    try {
      result = client.timedQuery(instanceName, query, queryLanguage, qOptions);
      jq.html(
        "Results obtained in " +
          result.wallMillis +
          "ms: <pre>" +
          JSON.stringify(result, undefined, 2) +
          "</pre>"
      );
      jq.css("color", "black");
    } catch (err) {
      reportThriftError(jq, err);
    }
  });
})();
