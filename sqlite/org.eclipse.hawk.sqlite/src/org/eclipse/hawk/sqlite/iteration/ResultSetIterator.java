/*******************************************************************************
 * Copyright (c) 2022-2023 The University of York.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.sqlite.iteration;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResultSetIterator<T> implements Iterator<T> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ResultSetIterator.class);
	private final ResultSet rs;
	private ResultSetFunction<T> resultToEdge;
	private Boolean hasNext = null;

	public ResultSetIterator(ResultSet rs, ResultSetFunction<T> resultToEdge) {
		this.rs = rs;
		this.resultToEdge = resultToEdge;
	}

	@Override
	public boolean hasNext() {
		if (hasNext == null) {
			try {
				hasNext = rs.next();
				if (!hasNext) {
					rs.close();
				}
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				hasNext = false;
			}
		}

		return hasNext;
	}

	@Override
	public T next() {
		if (hasNext()) {
			try {
				final T mapped = resultToEdge.apply(rs);
				hasNext = null;
				return mapped;
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
		throw new NoSuchElementException();
	}
}