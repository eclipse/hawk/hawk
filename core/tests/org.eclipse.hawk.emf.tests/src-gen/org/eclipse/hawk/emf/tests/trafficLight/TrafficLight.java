/**
 */
package org.eclipse.hawk.emf.tests.trafficLight;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Traffic Light</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.hawk.emf.tests.trafficLight.TrafficLight#getColor <em>Color</em>}</li>
 * </ul>
 *
 * @see org.eclipse.hawk.emf.tests.trafficLight.TrafficLightPackage#getTrafficLight()
 * @model
 * @generated
 */
public interface TrafficLight extends EObject {
	/**
	 * Returns the value of the '<em><b>Color</b></em>' attribute.
	 * The default value is <code>"RED"</code>.
	 * The literals are from the enumeration {@link org.eclipse.hawk.emf.tests.trafficLight.LightColor}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Color</em>' attribute.
	 * @see org.eclipse.hawk.emf.tests.trafficLight.LightColor
	 * @see #setColor(LightColor)
	 * @see org.eclipse.hawk.emf.tests.trafficLight.TrafficLightPackage#getTrafficLight_Color()
	 * @model default="RED"
	 * @generated
	 */
	LightColor getColor();

	/**
	 * Sets the value of the '{@link org.eclipse.hawk.emf.tests.trafficLight.TrafficLight#getColor <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Color</em>' attribute.
	 * @see org.eclipse.hawk.emf.tests.trafficLight.LightColor
	 * @see #getColor()
	 * @generated
	 */
	void setColor(LightColor value);

} // TrafficLight
