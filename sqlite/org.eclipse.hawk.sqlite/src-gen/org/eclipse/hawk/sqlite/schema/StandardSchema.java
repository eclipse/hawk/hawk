/*******************************************************************************
* Copyright (c) 2023 The University of York.
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* http://www.eclipse.org/legal/epl-2.0.
*
* This Source Code may also be made available under the following Secondary
* Licenses when the conditions for such availability set forth in the Eclipse
* Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
*
* SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
*
* Contributors:
*     Antonio Garcia-Dominguez - initial API and implementation
******************************************************************************/
package org.eclipse.hawk.sqlite.schema;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
* Automatically generated schema class. DO NOT EDIT MANUALLY.
* @generated
*/
public class StandardSchema implements ISchema {

	private Connection connection;
	
	public StandardSchema(Connection connection) {
		this.connection = connection;
	}
	
	@Override
	public void createSchema() throws SQLException {
		try (Statement stmt = connection.createStatement()) {
			stmt.execute("CREATE TABLE IF NOT EXISTS nodes (label VARCHAR NOT NULL);");
			stmt.execute("CREATE INDEX IF NOT EXISTS nodes_by_label ON nodes (label);");
			stmt.execute("CREATE TABLE IF NOT EXISTS nodeprops (nodeid INTEGER NOT NULL, key VARCHAR NOT NULL, type VARCHAR NOT NULL, value);");
			stmt.execute("CREATE UNIQUE INDEX IF NOT EXISTS nodeprops_by_id_key ON nodeprops (nodeid, key);");
			stmt.execute("CREATE TABLE IF NOT EXISTS edges (label VARCHAR NOT NULL, startId INTEGER NOT NULL, endId INTEGER NOT NULL);");
			stmt.execute("CREATE UNIQUE INDEX IF NOT EXISTS edges_unique ON edges (label, startId, endId);");
			stmt.execute("CREATE INDEX IF NOT EXISTS edges_label_end ON edges (label, endId);");
			stmt.execute("CREATE INDEX IF NOT EXISTS edges_start ON edges (startId);");
			stmt.execute("CREATE INDEX IF NOT EXISTS edges_end ON edges (endId);");
			stmt.execute("CREATE TABLE IF NOT EXISTS edgeprops (edgeid INTEGER NOT NULL, key VARCHAR NOT NULL, type VARCHAR NOT NULL, value VARCHAR);");
			stmt.execute("CREATE UNIQUE INDEX IF NOT EXISTS edgeprops_by_id_key ON edgeprops (edgeid, key);");
			stmt.execute("CREATE TABLE IF NOT EXISTS nodeindices (name VARCHAR UNIQUE NOT NULL);");
			connection.commit();
		}
	}
	
	@Override
	public void createNodeIndex(String name) throws SQLException {
		try (Statement stmt = connection.createStatement()) {
			stmt.execute(String.format("CREATE TABLE IF NOT EXISTS `idx_%s` (key VARCHAR NOT NULL, nodeid INTEGER NOT NULL, value);", name));
			stmt.execute(String.format("CREATE INDEX IF NOT EXISTS `idx_%s_key` ON `idx_%s` (key);", name, name));
			stmt.execute(String.format("CREATE INDEX IF NOT EXISTS `idx_%s_node` ON `idx_%s` (nodeid);", name, name));
			stmt.execute(String.format("CREATE INDEX IF NOT EXISTS `idx_%s_val` ON `idx_%s` (value);", name, name));
		}
	}
	
	@Override
	public void clearNodeIndexTable(String name) throws SQLException {
		try (Statement stmt = connection.createStatement()) {
			stmt.execute(String.format("DELETE FROM `idx_%s`;", name));
		}
	}
	
	@Override
	public void dropNodeIndexTable(String name) throws SQLException {
		try (Statement stmt = connection.createStatement()) {
			stmt.execute(String.format("DROP TABLE `idx_%s`;", name));
		}
	}
}
