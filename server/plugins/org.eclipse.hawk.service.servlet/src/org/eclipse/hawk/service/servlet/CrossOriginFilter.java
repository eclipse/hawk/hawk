/*******************************************************************************
 * Copyright (c) 2023 The University of York.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.service.servlet;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

public class CrossOriginFilter implements Filter {

	private static final String CUSTOM_ALLOW_ORIGIN = "hawk.http.allowOrigin";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		if (response instanceof HttpServletResponse) {
			String customAllowOrigin = System.getProperty(CUSTOM_ALLOW_ORIGIN);
			HttpServletResponse httpResponse = (HttpServletResponse) response;

			httpResponse.addHeader(
				"Access-Control-Allow-Origin",
				customAllowOrigin == null ? "*" : customAllowOrigin);

			httpResponse.addHeader(
				"Access-Control-Allow-Headers",
				"Content-Type");
		}

		chain.doFilter(request, response);
	}

}
