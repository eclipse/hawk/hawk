/*******************************************************************************
 * Copyright (c) 2015-2020 University of York, Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *    Antonio Garcia-Dominguez - initial API and implementation
 *******************************************************************************/
package org.eclipse.hawk.ui2.vcs;

import org.eclipse.hawk.core.IVcsManager;
import org.eclipse.hawk.osgiserver.HModel;
import org.eclipse.hawk.ui2.dialog.HVCSDialog;
import org.eclipse.swt.widgets.Composite;

/**
 * Generic interface for a block that configures a new {@link IVcsManager} for use.
 */
public interface IVcsConfigurationBlock {

	/**
	 * Eclipse extension point ID for all VCS configuration blocks.
	 */
	String EXT_ID = "org.eclipse.hawk.ui.vcsConfigBlock";

	/**
	 * Name of the class that refers to the configuration block class.
	 */
	String EXT_ATTR_CLASS = "class";

	/**
	 * Returns {@code true} iff this component can set up the provided {@link IVcsManager}.
	 */
	boolean isApplicableTo(IVcsManager manager);

	/**
	 * Creates and populates the container with all the controls needed to set up
	 * the {@link IVcsManager}. The component should retain references to the
	 * various widgets inside, for the later call to
	 * {@link #configure(IVcsManager)}.
	 */
	void createBlock(Composite container, HVCSDialog dialog);

	/**
	 * Configures the provided manager based on the current state of the widgets inside
	 * the container returned by {@link #createBlock()}. 
	 */
	public void okPressed(HModel hawkModel, boolean isFrozen);

}
