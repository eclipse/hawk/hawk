/*******************************************************************************
 * Copyright (c) 2022-2023 Aston University, University of York.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.emf.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.hawk.core.model.IHawkAttribute;
import org.eclipse.hawk.core.model.IHawkClass;
import org.eclipse.hawk.core.model.IHawkReference;
import org.eclipse.hawk.emf.EMFClass;
import org.eclipse.hawk.emf.EMFObject;
import org.eclipse.hawk.emf.EMFWrapperFactory;
import org.eclipse.hawk.emf.tests.trafficLight.LightColor;
import org.eclipse.hawk.emf.tests.trafficLight.TrafficLight;
import org.eclipse.hawk.emf.tests.trafficLight.TrafficLightFactory;
import org.junit.Test;

public class EMFObjectTest {

	@Test
	public void getUnsetReferenceReturnsNull() {
		EClass eClass = EcoreFactory.eINSTANCE.createEClass();
		EMFObject emfOb = new EMFObject(eClass, new EMFWrapperFactory());

		EMFClass type = (EMFClass) emfOb.getType();
		IHawkReference pkgRef = null;
		for (IHawkReference ref : type.getAllReferences()) {
			if ("ePackage".equals(ref.getName())) {
				pkgRef = ref;
				break;
			}
		}
		assertNotNull("The package reference should be defined in EClass", pkgRef);
		assertNull("Getting the value of an unset reference should return null", emfOb.get(pkgRef, false));
	}

	@Test
	public void fallbackToDefaultValue() {
		TrafficLight realTL = TrafficLightFactory.eINSTANCE.createTrafficLight();
		EStructuralFeature rawSF = realTL.eClass().getEStructuralFeature("color");

		/*
		 * Use Mockito to emulate issue in SysML2 models where:
		 *
		 * 1. A structure feature `sf` with a default value is not set
		 * 2. `eGet(sf)` returns null rather than the default value, as normally expected.
		 */
		TrafficLight tlMock = mock(TrafficLight.class);
		when(tlMock.eIsSet(rawSF)).thenReturn(false);
		when(tlMock.eGet(rawSF)).thenReturn(null);
		when(tlMock.eClass()).thenReturn(realTL.eClass());

		/*
		 * As a workaround for this issue, Hawk will fall back on the
		 * feature's default value.
		 */
		EMFObject eob = new EMFObject(tlMock, new EMFWrapperFactory());
		final IHawkAttribute hawkSF = (IHawkAttribute)
			((IHawkClass) eob.getType()).getStructuralFeature("color");
		assertTrue(eob.isSet(hawkSF));
		assertEquals(LightColor.RED, eob.get(hawkSF));
	}

}
