package org.eclipse.hawk.integration.tests.emf;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.File;

import org.eclipse.hawk.backend.tests.factories.IGraphDatabaseFactory;
import org.eclipse.hawk.epsilon.emc.optimisation.OptimisableCollection;
import org.eclipse.hawk.integration.tests.IntegrationTestSuite;
import org.eclipse.hawk.integration.tests.ModelIndexingTest;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;

public class IndexedAttributeTest extends ModelIndexingTest {

	@Parameters(name="{1}")
	public static Iterable<Object[]> params() {
		return IntegrationTestSuite.caseParams();
	}

	public IndexedAttributeTest(File baseDir, IGraphDatabaseFactory dbf) {
		super(baseDir, dbf, new EMFModelSupportFactory());
	} 

	@Test
	public void lookupWithIndex() throws Throwable {
		indexer.registerMetamodels(new File(baseDir, "resources/metamodels/Ecore.ecore"),
				new File(baseDir, "resources/metamodels/crossrefs.ecore"));
		indexer.addIndexedAttribute("http://github.com/mondo-hawk/testing/xrefs", "Element", "id");
		requestFolderIndex(new File(baseDir, "resources/models/scopedQuery"));

		scheduleAndWait(() -> {
			assertEquals(OptimisableCollection.class.getSimpleName(), eol("return Element.all.class.simpleName;"));
			assertEquals(1, eol("return Element.all.select(e|e.id=1).size();"));
			return null;
		});
	}
	
	@Test
	public void addThenIndex() throws Throwable {
		indexer.registerMetamodels(new File(baseDir, "resources/metamodels/Ecore.ecore"),
				new File(baseDir, "resources/metamodels/crossrefs.ecore"));
		requestFolderIndex(new File(baseDir, "resources/models/scopedQuery"));
		indexer.addIndexedAttribute("http://github.com/mondo-hawk/testing/xrefs", "Element", "id");
		
		scheduleAndWait(() -> {
			assertEquals(OptimisableCollection.class.getSimpleName(), eol("return Element.all.class.simpleName;"));
			assertEquals(1, eol("return Element.all.select(e|e.id=1).size();"));
			return null;
		});
	}
	
	@Test
	public void lookupWithoutIndex() throws Throwable {
		indexer.registerMetamodels(new File(baseDir, "resources/metamodels/Ecore.ecore"),
				new File(baseDir, "resources/metamodels/crossrefs.ecore"));
		requestFolderIndex(new File(baseDir, "resources/models/scopedQuery"));

		scheduleAndWait(() -> {
			assertNotEquals(OptimisableCollection.class.getSimpleName(), eol("return Element.all.class.simpleName;"));
			assertEquals(1, eol("return Element.all.select(e|e.id=1).size();"));
			return null;
		});
	}
	
	// TODO: more indexed lookup tests
	
}
