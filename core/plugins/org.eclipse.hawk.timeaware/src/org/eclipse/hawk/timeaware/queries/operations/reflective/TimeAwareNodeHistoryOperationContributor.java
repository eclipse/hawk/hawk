/*******************************************************************************
 * Copyright (c) 2018-2023 Aston University, University of York
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 *     Jonathan Co - update for Epsilon 2.0 compatibility
 ******************************************************************************/
package org.eclipse.hawk.timeaware.queries.operations.reflective;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.execute.operations.contributors.OperationContributor;
import org.eclipse.hawk.core.graph.IGraphEdge;
import org.eclipse.hawk.core.graph.IGraphNodeReference;
import org.eclipse.hawk.core.graph.timeaware.ITimeAwareGraphNode;
import org.eclipse.hawk.epsilon.emc.EOLQueryEngine.GraphNodeWrapper;
import org.eclipse.hawk.epsilon.emc.wrappers.GraphEdgeWrapper;
import org.eclipse.hawk.timeaware.queries.RiskyFunction;
import org.eclipse.hawk.timeaware.queries.TimeAwareEOLQueryEngine;

public class TimeAwareNodeHistoryOperationContributor extends OperationContributor {
	private TimeAwareEOLQueryEngine model;

	public TimeAwareNodeHistoryOperationContributor(TimeAwareEOLQueryEngine q) {
		this.model = q;
	}

	@Override
	public boolean contributesTo(Object target) {
		return target instanceof GraphNodeWrapper && ((GraphNodeWrapper)target).getNode() instanceof ITimeAwareGraphNode;
	}

	public List<IGraphNodeReference> getVersionsBetween(long fromInclusive, long toInclusive) throws EolRuntimeException {
		return getRelatedVersions((taNode) -> taNode.getVersionsBetween(fromInclusive, toInclusive));
	}

	public List<IGraphNodeReference> getVersionsFrom(long fromInclusive) throws EolRuntimeException {
		return getRelatedVersions((taNode) -> taNode.getVersionsFrom(fromInclusive));
	}

	public List<IGraphNodeReference> getVersionsUpTo(long toInclusive) throws EolRuntimeException {
		return getRelatedVersions((taNode) -> taNode.getVersionsUpTo(toInclusive));
	}

	public IGraphNodeReference travelInTime(long time) {
		return model.wrap(castTarget(getTarget()).travelInTime(time));
	}

	public List<GraphEdgeWrapper> getOutgoingEdgesWithTypeBetween(String type, long fromInclusive, long toInclusive) throws EolRuntimeException {
		final ITimeAwareGraphNode taNode = castTarget(getTarget());
		return getEdgeList(() -> taNode.getOutgoingWithTypeBetween(type, fromInclusive, toInclusive));
	}

	public List<GraphEdgeWrapper> getIncomingEdgesWithTypeBetween(String type, long fromInclusive, long toInclusive) throws EolRuntimeException {
		final ITimeAwareGraphNode taNode = castTarget(getTarget());
		return getEdgeList(() -> taNode.getIncomingWithTypeBetween(type, fromInclusive, toInclusive));
	}

	public List<GraphEdgeWrapper> getOutgoingEdgesWithTypeCreatedBetween(String type, long fromInclusive, long toInclusive) throws EolRuntimeException {
		final ITimeAwareGraphNode taNode = castTarget(getTarget());
		return getEdgeList(() -> taNode.getOutgoingWithTypeCreatedBetween(type, fromInclusive, toInclusive));
	}

	public List<GraphEdgeWrapper> getIncomingEdgesWithTypeCreatedBetween(String type, long fromInclusive, long toInclusive) throws EolRuntimeException {
		final ITimeAwareGraphNode taNode = castTarget(getTarget());
		return getEdgeList(() -> taNode.getIncomingWithTypeCreatedBetween(type, fromInclusive, toInclusive));
	}
	
	private List<GraphEdgeWrapper> getEdgeList(final Callable<Iterable<IGraphEdge>> edgesCallable)
			throws EolRuntimeException {
		final List<GraphEdgeWrapper> results = new ArrayList<>();
		try {
			final Iterable<IGraphEdge> edges = edgesCallable.call();
			for (IGraphEdge e : edges) {
				results.add(new GraphEdgeWrapper(e, model));
			}
			return results;
		} catch (Exception e) {
			throw new EolRuntimeException(e.getMessage());
		}
	}
	
	private List<IGraphNodeReference> getRelatedVersions(
			final RiskyFunction<ITimeAwareGraphNode, List<ITimeAwareGraphNode>> f) throws EolRuntimeException {
		final ITimeAwareGraphNode taNode = castTarget(getTarget());
		final List<IGraphNodeReference> results = new ArrayList<>();
		try {
			for (ITimeAwareGraphNode version : f.call(taNode)) {
				results.add(model.wrap(version));
			}
		} catch (Exception e) {
			throw new EolRuntimeException(e.getMessage());
		}
		return results;
	}

	private ITimeAwareGraphNode castTarget(Object target) {
		return (ITimeAwareGraphNode) ((GraphNodeWrapper)target).getNode();
	}
}