/*******************************************************************************
 * Copyright (c) 2011-2019 The University of York, Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Konstantinos Barmpis - initial API and implementation
 *     Antonio Garcia-Dominguez - extend from IHawkPlugin
 ******************************************************************************/
package org.eclipse.hawk.core;

import java.io.File;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;

import org.eclipse.hawk.core.model.IHawkMetaModelResource;
import org.eclipse.hawk.core.model.IHawkPackage;

public interface IMetaModelResourceFactory extends IHawkPlugin {

	String DUMPED_PKG_PREFIX = "resource_from_epackage_";

	/** adds a new metamodel resource to hawk, from a file */
	IHawkMetaModelResource parse(File f) throws Exception;

	/** provides any static metamodel resources to hawk */
	Set<IHawkMetaModelResource> getStaticMetamodels();

	/**
	 * Provides a metamodel resource from its internal registry.
	 *
	 * @param nsURI Namespace URI of the {@link IHawkPackage} whose resource we want.
	 * @return Resource with the requested package, or {@code null} if not found.
	 */
	default IHawkMetaModelResource getMetamodel(String nsURI) {
		return null;
	}

	void shutdown();

	boolean canParse(File f);

	Collection<String> getMetaModelExtensions();

	/** adds a new metamodel resource to hawk, given a string representation */
	IHawkMetaModelResource parseFromString(String name, String contents) throws Exception;

	/**
	 * Attempts to dump a package to a string, for later re-loading. It may
	 * not be possible to dump some packages. These will need to be provided
	 * by other means on restart of the index (e.g. through the static metamodels
	 * of the metamodel resource factories, or global registries).
	 */
	Optional<String> dumpPackageToString(IHawkPackage ePackage) throws Exception;

	@Override
	default Category getCategory() {
		return Category.METAMODEL_RESOURCE_FACTORY;
	}
	
}
