/**
 */
package org.eclipse.hawk.emf.tests.trafficLight.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.hawk.emf.tests.trafficLight.LightColor;
import org.eclipse.hawk.emf.tests.trafficLight.TrafficLight;
import org.eclipse.hawk.emf.tests.trafficLight.TrafficLightFactory;
import org.eclipse.hawk.emf.tests.trafficLight.TrafficLightPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TrafficLightPackageImpl extends EPackageImpl implements TrafficLightPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass trafficLightEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum lightColorEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.hawk.emf.tests.trafficLight.TrafficLightPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TrafficLightPackageImpl() {
		super(eNS_URI, TrafficLightFactory.eINSTANCE);
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link TrafficLightPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TrafficLightPackage init() {
		if (isInited) return (TrafficLightPackage)EPackage.Registry.INSTANCE.getEPackage(TrafficLightPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredTrafficLightPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		TrafficLightPackageImpl theTrafficLightPackage = registeredTrafficLightPackage instanceof TrafficLightPackageImpl ? (TrafficLightPackageImpl)registeredTrafficLightPackage : new TrafficLightPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theTrafficLightPackage.createPackageContents();

		// Initialize created meta-data
		theTrafficLightPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTrafficLightPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TrafficLightPackage.eNS_URI, theTrafficLightPackage);
		return theTrafficLightPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTrafficLight() {
		return trafficLightEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTrafficLight_Color() {
		return (EAttribute)trafficLightEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getLightColor() {
		return lightColorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TrafficLightFactory getTrafficLightFactory() {
		return (TrafficLightFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		trafficLightEClass = createEClass(TRAFFIC_LIGHT);
		createEAttribute(trafficLightEClass, TRAFFIC_LIGHT__COLOR);

		// Create enums
		lightColorEEnum = createEEnum(LIGHT_COLOR);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(trafficLightEClass, TrafficLight.class, "TrafficLight", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTrafficLight_Color(), this.getLightColor(), "color", "RED", 0, 1, TrafficLight.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(lightColorEEnum, LightColor.class, "LightColor");
		addEEnumLiteral(lightColorEEnum, LightColor.RED);
		addEEnumLiteral(lightColorEEnum, LightColor.YELLOW);
		addEEnumLiteral(lightColorEEnum, LightColor.GREEN);

		// Create resource
		createResource(eNS_URI);
	}

} //TrafficLightPackageImpl
