/**
 */
package org.eclipse.hawk.emf.tests.trafficLight;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.hawk.emf.tests.trafficLight.TrafficLightPackage
 * @generated
 */
public interface TrafficLightFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TrafficLightFactory eINSTANCE = org.eclipse.hawk.emf.tests.trafficLight.impl.TrafficLightFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Traffic Light</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Traffic Light</em>'.
	 * @generated
	 */
	TrafficLight createTrafficLight();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TrafficLightPackage getTrafficLightPackage();

} //TrafficLightFactory
