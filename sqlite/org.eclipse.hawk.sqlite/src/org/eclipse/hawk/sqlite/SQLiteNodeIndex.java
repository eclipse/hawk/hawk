/*******************************************************************************
 * Copyright (c) 2022-2023 The University of York.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.sqlite;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.eclipse.hawk.core.graph.IGraphIterable;
import org.eclipse.hawk.core.graph.IGraphNode;
import org.eclipse.hawk.core.graph.IGraphNodeIndex;
import org.eclipse.hawk.sqlite.AbstractSQLiteDatabase.SQLiteConnection;
import org.eclipse.hawk.sqlite.iteration.StatementGraphNodeIterable;
import org.eclipse.hawk.sqlite.iteration.StatementSupplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SQLiteNodeIndex implements IGraphNodeIndex {

	private static final Logger LOGGER = LoggerFactory.getLogger(SQLiteNodeIndex.class);
	private final SQLiteDatabase db;
	private final String name;

	public SQLiteNodeIndex(SQLiteDatabase db, String name) {
		this.db = db;
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public IGraphIterable<? extends IGraphNode> query(String key, Object valueOrPattern) {
		if (valueOrPattern instanceof String && ((String)valueOrPattern).contains("*")) {
			final String pattern = (String)valueOrPattern;

			if("*".equals(key) && "*".equals(pattern)) {
				return new StatementGraphNodeIterable<>(
					() -> db.getConnection().getQueries().getQueryIndexValueAllPairsStatement(name),
					() -> db.getConnection().getQueries().getQueryIndexValueAllPairsCountStatement(name),
					() -> db.getConnection().getQueries().getQueryIndexValueAllPairsSingleStatement(name),
					db::getNodeById
				);
			} else if ("*".equals(pattern)) {
				return new StatementGraphNodeIterable<>(
					() -> db.getConnection().getQueries().getQueryIndexValueAllValuesStatement(name, key),
					() -> db.getConnection().getQueries().getQueryIndexValueAllValuesCountStatement(name, key),
					() -> db.getConnection().getQueries().getQueryIndexValueAllValuesSingleStatement(name, key),
					db::getNodeById
				);
			} else {
				return new StatementGraphNodeIterable<>(
					() -> db.getConnection().getQueries().getQueryIndexValuePatternStatement(name, key, pattern),
					() -> db.getConnection().getQueries().getQueryIndexValuePatternCountStatement(name, key, pattern),
					() -> db.getConnection().getQueries().getQueryIndexValuePatternSingleStatement(name, key, pattern),
					db::getNodeById
				);
			}
		}

		// Exact value
		return get(key, valueOrPattern);
	}

	@Override
	public IGraphIterable<? extends IGraphNode> query(String key, Number from, Number to, boolean fromInclusive, boolean toInclusive) {
		return new StatementGraphNodeIterable<>(
			() -> db.getConnection().getQueries().getQueryIndexNumberRangeStatement(name, key, fromInclusive, from, toInclusive, to),
			() -> db.getConnection().getQueries().getQueryIndexNumberRangeCountStatement(name, key, fromInclusive, from, toInclusive, to),
			() -> db.getConnection().getQueries().getQueryIndexNumberRangeSingleStatement(name, key, fromInclusive, from, toInclusive, to),
			db::getNodeById
		);
	}

	@Override
	public IGraphIterable<? extends IGraphNode> get(String key, Object exactValue) {
		return new StatementGraphNodeIterable<>(
			() -> db.getConnection().getQueries().getQueryIndexValueExactStatement(name, key, exactValue),
			() -> db.getConnection().getQueries().getQueryIndexValueExactCountStatement(name, key, exactValue),
			() -> db.getConnection().getQueries().getQueryIndexValueExactSingleStatement(name, key, exactValue),
			db::getNodeById
		);
	}

	@Override
	public void add(IGraphNode n, String key, Object value) {
		int rowCount = executeUpdate(() -> db.getConnection().getQueries().getAddNodeIndexEntryStatement(name, key, (int) n.getId(), value));
		assert rowCount == 1 : "One row should be inserted when adding an index entry";
	}

	@Override
	public void remove(IGraphNode n) {
		executeUpdate(() -> db.getConnection().getQueries().getRemoveNodeFromIndexStatement(name, (int) n.getId()));
	}

	@Override
	public void remove(IGraphNode n, String key, Object value) {
		if (key == null) {
			if (value == null) {
				remove(n);
			} else {
				executeUpdate(() -> db.getConnection().getQueries().getRemoveNodeValueFromIndexStatement(name, (int) n.getId(), value));
			}
		} else if (value == null) {
			executeUpdate(() -> db.getConnection().getQueries().getRemoveNodeFieldFromIndexStatement(name, (int) n.getId(), key));
		}

		executeUpdate(() -> db.getConnection().getQueries().getRemoveNodeIndexEntryStatement(name, key, (int) n.getId(), value));
	}

	@Override
	public void flush() {
		// nothing to do
	}

	@Override
	public void delete() {
		try {
			final SQLiteConnection conn = db.getConnection();

			PreparedStatement stmt = db.getConnection().getQueries().getDeleteNodeIndexStatement(name);
			int rowCount = stmt.executeUpdate();
			assert rowCount == 1 : "A row should have been deleted when deleting a node index";

			conn.getSchema().clearNodeIndexTable(name);
			conn.dropIndexTableOnCommit(name);
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	private int executeUpdate(StatementSupplier stmt) {
		try {
			return stmt.get().executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
		}
		return 0;
	}
}
