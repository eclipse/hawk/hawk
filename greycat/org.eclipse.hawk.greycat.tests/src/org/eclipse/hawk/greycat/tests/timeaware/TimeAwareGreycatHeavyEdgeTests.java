/*******************************************************************************
 * Copyright (c) 2018-2023 Aston University, University of York.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.greycat.tests.timeaware;

import static org.junit.Assert.assertFalse;

import java.io.File;
import java.util.Collections;

import org.eclipse.hawk.backend.tests.LogbackOnlyErrorsRule;
import org.eclipse.hawk.backend.tests.RedirectSystemErrorRule;
import org.eclipse.hawk.backend.tests.TemporaryDatabaseTest;
import org.eclipse.hawk.backend.tests.factories.IGraphDatabaseFactory;
import org.eclipse.hawk.core.graph.IGraphEdge;
import org.eclipse.hawk.core.graph.IGraphNode;
import org.eclipse.hawk.core.graph.IGraphTransaction;
import org.eclipse.hawk.core.graph.timeaware.ITimeAwareGraphDatabase;
import org.eclipse.hawk.core.graph.timeaware.ITimeAwareGraphNode;
import org.eclipse.hawk.greycat.tests.LevelDBGreycatDatabaseFactory;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;

public class TimeAwareGreycatHeavyEdgeTests extends TemporaryDatabaseTest {

	private ITimeAwareGraphDatabase taDB;

	public TimeAwareGreycatHeavyEdgeTests(File baseDir, IGraphDatabaseFactory dbf) {
		super(dbf);
	}

	@Rule
	public RedirectSystemErrorRule errRule = new RedirectSystemErrorRule();

	@Rule
	public LogbackOnlyErrorsRule logRule = new LogbackOnlyErrorsRule();

	@Parameters(name = "{1}")
	public static Iterable<Object[]> params() {
		return Collections.singletonList(new Object[] { null, new LevelDBGreycatDatabaseFactory() });
	}

	@Override
	public void setup() throws Exception {
		super.setup();
		taDB = (ITimeAwareGraphDatabase)db;
	}
	
	@Test
	public void createDeleteHeavyEdgeBeginningOfTime() throws Exception {
		taDB.setTime(0L);
		createDeleteHeavyEdge();
	}

	@Test
	public void createDeleteHeavyEdgeNonZeroTime() throws Exception {
		taDB.setTime(1L);
		createDeleteHeavyEdge();
	}

	private void createDeleteHeavyEdge() throws Exception {
		Object heavyEdgeId;
		try (IGraphTransaction tx = db.beginTransaction()) {
			IGraphNode n1 = db.createNode(null, "example");
			IGraphNode n2 = db.createNode(null, "example");
			IGraphEdge e = db.createRelationship(n1, n2, "example", Collections.singletonMap("key", "value"));
			heavyEdgeId = e.getId();
			e.delete();
			tx.success();
		}

		try (IGraphTransaction tx = db.beginTransaction()) {
			ITimeAwareGraphNode backingNode = taDB.getNodeById(heavyEdgeId);
			assertFalse(backingNode.isAlive());
			tx.success();
		}
	}

}
