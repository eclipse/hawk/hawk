/*******************************************************************************
 * Copyright (c) 2022-2023 The University of York.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.sqlite.iteration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Function;

import org.eclipse.hawk.core.graph.IGraphIterable;
import org.eclipse.hawk.core.graph.IGraphNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StatementGraphNodeIterable<T extends IGraphNode> implements IGraphIterable<T> {
	private static final Logger LOGGER = LoggerFactory.getLogger(StatementGraphNodeIterable.class);

	private final StatementSupplier stmtIterator, stmtSize, stmtSingle;
	private final Function<Object, T> id2Node;

	public StatementGraphNodeIterable(StatementSupplier stmtIterator, StatementSupplier stmtSize, StatementSupplier stmtSingle, Function<Object, T> id2Node) {
		this.stmtIterator = stmtIterator;
		this.stmtSize = stmtSize;
		this.stmtSingle = stmtSingle;
		this.id2Node = id2Node;
	}

	@Override
	public Iterator<T> iterator() {
		try {
			PreparedStatement stmt = stmtIterator.get();
			stmt.execute();
			return new ResultSetIterator<T>(stmt.getResultSet(), this::getNodeFromResultSet);
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			return Collections.emptyIterator();
		}
	}

	@Override
	public int size() {
		try {
			PreparedStatement stmt = stmtSize.get();
			stmt.execute();
			try (ResultSet rs = stmt.getResultSet()) {
				rs.next();
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			return 0;
		}
	}

	@Override
	public T getSingle() {
		try {
			PreparedStatement stmt = stmtSingle.get();
			stmt.execute();
			try (ResultSet rs = stmt.getResultSet()) {
				if (rs.next()) {
					return getNodeFromResultSet(rs);
				}
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
		}
		throw new NoSuchElementException();
	}

	private T getNodeFromResultSet(ResultSet rs) throws SQLException {
		final int nodeId = rs.getInt(1);
		return id2Node.apply(nodeId);
	}
}