/*******************************************************************************
 * Copyright (c) 2018-2020 Aston University.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.localfolder;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;

import org.eclipse.hawk.core.IModelIndexer;
import org.eclipse.hawk.core.VcsChangeType;
import org.eclipse.hawk.core.VcsCommit;
import org.eclipse.hawk.core.VcsCommitItem;
import org.eclipse.hawk.core.VcsRepositoryDelta;

/**
 * VCS manager that watches over a single file. Created for TTC 2018.
 */
public class LocalFile extends FileBasedLocation {

	private byte[] initialVersion;
	private byte[] lastIndexed;
	private File monitoredFile;

	@Override
	public void init(String vcsloc, IModelIndexer indexer) throws Exception {
		console = indexer.getConsole();

		// Accept both regular paths and file:// URIs
		Path path;
		try {
			path = Paths.get(new URI(vcsloc));
		} catch (URISyntaxException | IllegalArgumentException ex) {
			path = Paths.get(vcsloc);
		}

		monitoredFile = path.toFile().getCanonicalFile();
		initialVersion = digestFile(monitoredFile);
		String repositoryURI = path.toUri().toString();

		// If the file doesn't exist, it might be because this is a local folder in
		// a remote server - try to preserve the provided vcsloc as is. Otherwise,
		// if the server and the client use different operating systems we could end
		// up with an unusable URL in the server.
		if (monitoredFile.exists()) {
			repositoryURL = repositoryURI;
		} else {
			repositoryURL = vcsloc;
		}
	}

	@Override
	protected String getCurrentRevision(boolean alter) {
		try {
			final byte[] digest = digestFile(monitoredFile);
			if (alter) {
				lastIndexed = digest;
			}
			return Base64.getEncoder().encodeToString(digest);
		} catch (Exception ex) {
			console.printerrln(ex);
			return FIRST_REV;
		}
	}

	@Override
	public File importFile(String revision, String p, File temp) {
		return monitoredFile;
	}

	@Override
	public boolean isActive() {
		return monitoredFile != null && monitoredFile.exists();
	}

	@Override
	public void shutdown() {
		monitoredFile = null;
	}

	@Override
	public String getHumanReadableName() {
		return "Local File Monitor";
	}

	@Override
	public VcsRepositoryDelta getDelta(String startRevision, String endRevision) throws Exception {
		VcsCommit commit = new VcsCommit();
		VcsRepositoryDelta delta = new VcsRepositoryDelta(Collections.singleton(commit));
		delta.setManager(this);

		commit.setAuthor("i am a local file driver - no authors recorded");
		commit.setJavaDate(new Date());
		commit.setMessage("i am a local file driver - no messages recorded");

		VcsCommitItem c = new VcsCommitItem();
		c.setCommit(commit);
		c.setPath("/" + monitoredFile.getName());

		final byte[] digest = digestFile(monitoredFile);
		if (!Arrays.equals(digest, lastIndexed)) {
			final String sDigest = Base64.getEncoder().encodeToString(digest);
			if (Arrays.equals(digest, initialVersion)) {
				c.setChangeType(VcsChangeType.ADDED);
				commit.setRevision(sDigest);
				commit.getItems().add(c);
			} else {
				c.setChangeType(VcsChangeType.UPDATED);
				commit.setRevision(sDigest);
				commit.getItems().add(c);
			}
		}

		lastIndexed = digest;

		return delta;
	}

	@Override
	public String getDefaultLocation() {
		return "file://path/to/file";
	}
}
