#!/bin/bash

VERSION=$(grep Bundle-Version "$WORKSPACE"/core/plugins/org.eclipse.hawk.core/META-INF/MANIFEST.MF | \
              cut --delim=: -f2 | \
              sed -re 's/ *([0-9]+)[.]([0-9]+)[.].*/\1.\2.0/')
DIST_DIR="/home/data/httpd/download.eclipse.org/hawk/$VERSION"
SQLITE_DIR="$DIST_DIR/sqlite"
SSH_USER=genie.hawk@projects-storage.eclipse.org

echo "Creating downloads for Hawk SQLite backend ${VERSION} at ${DIST_DIR}"

# Use rsync with delete rather than a straight up 'rm', in case SSH fails intermittently
ssh "$SSH_USER" mkdir -p "$SQLITE_DIR"
rsync -rvhz "$WORKSPACE/sqlite/org.eclipse.hawk.sqlite.updatesite/target/repository/" "$SSH_USER":"${SQLITE_DIR}/" --delete
scp "$WORKSPACE"/sqlite/org.eclipse.hawk.sqlite.updatesite/target/org.eclipse.hawk.sqlite.updatesite.*.zip "$SSH_USER":"${DIST_DIR}/"
