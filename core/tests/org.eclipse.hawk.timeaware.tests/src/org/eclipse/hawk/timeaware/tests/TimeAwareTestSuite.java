package org.eclipse.hawk.timeaware.tests;

import java.io.File;
import java.util.Collections;

import org.eclipse.hawk.backend.tests.factories.IGraphDatabaseFactory;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Suite.SuiteClasses;

import com.github.peterwippermann.junit4.parameterizedsuite.ParameterContext;
import com.github.peterwippermann.junit4.parameterizedsuite.ParameterizedSuite;

/**
 * Base class for time-awareness test suites for certain backends.
 * Backends wishing to reuse these tests should inherit from this class
 * and add an appropriate params method.
 */
@RunWith(ParameterizedSuite.class)
@SuiteClasses({
	DerivedAttributeHistoryTest.class,
	FileContextTimeAwareEOLQueryEngineTest.class,
	GitNodeHistoryTest.class,
	SubversionNodeHistoryTest.class,
	TimeAwareBackendTest.class,
	VCSManagerIndexTest.class,
})
public class TimeAwareTestSuite {

	public static Iterable<Object[]> caseParams() {
		if (ParameterContext.isParameterSet()) {
			return Collections.singletonList(ParameterContext.getParameter(Object[].class));
		} else {
			return Collections.emptyList();
		}
	}

	@Parameter(0)
	public File baseDirectory;

	@Parameter(1)
	public IGraphDatabaseFactory dbFactory;

}
