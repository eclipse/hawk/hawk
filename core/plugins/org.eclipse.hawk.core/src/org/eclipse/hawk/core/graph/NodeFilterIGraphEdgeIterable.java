/*******************************************************************************
 * Copyright (c) 2011-2023 The University of York.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.core.graph;

import java.util.HashSet;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 * <p>
 * Filters an {@code Iterable<IGraphEdge>} by only letting through the elements
 * that involve a certain set of nodes (whether as start nodes or as end nodes).
 * The type of matching is controlled by a set of OR'ed flags.
 * </p>
 * 
 * <p>
 * This class is intended to provide a safe default implementation for the new
 * node-aware edge accessors in {@link IGraphNode}. Backends can introduce more
 * efficient versions of these methods that use the backend's underlying
 * capabilities.
 * </p>
 */
public final class NodeFilterIGraphEdgeIterable implements Iterable<IGraphEdge> {
	private final Iterable<IGraphEdge> allEdges;
	private final Set<Object> ids;
	private final int flags;

	public static final int MATCH_START_NODE = 0x1;
	public static final int MATCH_END_NODE = 0x2;

	public NodeFilterIGraphEdgeIterable(Iterable<IGraphEdge> allEdges, Iterable<IGraphNode> targets, int flags) {
		this.allEdges = allEdges;

		this.ids = new HashSet<>();
		for (IGraphNode target : targets) {
			ids.add(target.getId());
		}

		this.flags = flags;
	}

	@Override
	public Iterator<IGraphEdge> iterator() {
		Iterator<IGraphEdge> itAllEdges = allEdges.iterator();

		return new Iterator<IGraphEdge>() {
			IGraphEdge next = null;
			
			@Override
			public boolean hasNext() {
				if (next == null) {
					while (next == null && itAllEdges.hasNext()) {
						IGraphEdge candidate = itAllEdges.next();
						if ((flags & MATCH_START_NODE) != 0 && ids.contains(candidate.getStartNode().getId())) {
							next = candidate;
						} else if ((flags & MATCH_END_NODE) != 0 && ids.contains(candidate.getEndNode().getId())) {
							next = candidate;
						}
					}
				}
				return next != null;
			}

			@Override
			public IGraphEdge next() {
				if (!hasNext()) {
					throw new NoSuchElementException();
				}

				IGraphEdge ret = next;
				next = null;
				return ret;
			}
		};
	}
}