#!/bin/bash

SCRIPT_DIR=$(readlink -f "$(dirname "$0")")
THRIFT_PY=$(readlink -f "$SCRIPT_DIR/../../server/plugins/org.eclipse.hawk.service.api/src-gen-py")

if ! test -d env; then
    virtualenv env
fi
source env/bin/activate

export PYTHONPATH=$THRIFT_PY:.
pip install -q -r requirements.txt
./countInstances.py
