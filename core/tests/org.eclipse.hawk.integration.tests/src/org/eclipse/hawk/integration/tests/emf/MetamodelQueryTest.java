/*******************************************************************************
 * Copyright (c) 2018 Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.integration.tests.emf;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;

import org.eclipse.hawk.backend.tests.factories.IGraphDatabaseFactory;
import org.eclipse.hawk.core.query.InvalidQueryException;
import org.eclipse.hawk.core.query.QueryExecutionException;
import org.eclipse.hawk.emf.EMFClass;
import org.eclipse.hawk.graph.syncValidationListener.SyncValidationListener;
import org.eclipse.hawk.integration.tests.IntegrationTestSuite;
import org.eclipse.hawk.integration.tests.ModelIndexingTest;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;

/**
 * Tests for querying the metamodel with Hawk.
 */
public class MetamodelQueryTest extends ModelIndexingTest {

	private static final String JDTAST_PT = "org.amma.dsl.jdt.primitiveTypes";
	private static final String JDTAST_DOM = "org.amma.dsl.jdt.dom";
	private static final String JDTAST_CORE = "org.amma.dsl.jdt.core";
	@Rule
	public GraphChangeListenerRule<SyncValidationListener> syncValidation
		= new GraphChangeListenerRule<>(new SyncValidationListener());

	@Parameters(name="{1}")
	public static Iterable<Object[]> params() {
		return IntegrationTestSuite.caseParams();
	}

	public MetamodelQueryTest(File baseDir, IGraphDatabaseFactory dbf) {
		super(baseDir, dbf, new EMFModelSupportFactory());
	}

	@Before
	public void setUp() throws Throwable {
		indexer.registerMetamodels(
			new File(baseDir, "resources/metamodels/Ecore.ecore"),
			new File(baseDir, "resources/metamodels/JDTAST.ecore"));
	}

	@Test
	public void metamodels() throws Exception {
		assertEquals("org.eclipse.hawk.emf.metamodel.EMFMetaModelResourceFactory", eolmm(JDTAST_CORE, ".metamodelType"));
		assertNotNull(eolmm(JDTAST_CORE, ".resource"));
		assertEquals(new HashSet<>(Arrays.asList(JDTAST_DOM, JDTAST_PT)),
				eolmm(JDTAST_CORE, ".dependencies.collect(dep|dep.uri).asSet"));

		assertEquals(0, eolmm(JDTAST_PT, ".types.size"));
		assertEquals(21, eolmm(JDTAST_CORE, ".types.size"));
	}

	@Test
	public void slotInstanceTypeName() throws Exception {
		assertEquals(EMFClass.NULL_INSTANCE_TYPE, eolmm(JDTAST_CORE,
			".types.selectOne(t | t.name='IJavaModel')"
			+ ".references.selectOne(r|r.name='javaProjects').instanceTypeName"));
		assertEquals(String.class.getCanonicalName(), eolmm(JDTAST_CORE,
				".types.selectOne(t | t.name='IJavaModel')"
				+ ".attributes.selectOne(r|r.name='path').instanceTypeName"));
	}

	@Test
	public void slotType() throws Exception {
		assertEquals("IJavaProject", eolmm(JDTAST_CORE,
			".types.selectOne(t | t.name='IJavaModel')"
			+ ".references.selectOne(r|r.name='javaProjects').type.name"));
	}

	@Test
	public void typeSupertypes() throws Exception {
		assertEquals(2, eolmm(JDTAST_CORE,
			".types.selectOne(t | t.name='IJavaProject').allSupertypes.size"));
	}

	@Test
	public void typeSubtypes() throws Exception {
		assertEquals(9, eolmm(JDTAST_CORE,
			".types.selectOne(t | t.name='PhysicalElement').allSubtypes.size"));
	}

	private Object eolmm(String uri, String suffix) throws InvalidQueryException, QueryExecutionException {
		return eol(String.format("return Model.metamodels.selectOne(mm|mm.uri='%s')%s;", uri, suffix));
	}
}
