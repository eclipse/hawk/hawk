/*******************************************************************************
 * Copyright (c) 2011-2016 The University of York.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.epsilon.emc.wrappers;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.eclipse.hawk.core.graph.IGraphNode;
import org.eclipse.hawk.core.graph.IGraphNodeReference;
import org.eclipse.hawk.core.query.IQueryEngine;
import org.eclipse.hawk.epsilon.emc.EOLQueryEngine;
import org.eclipse.hawk.graph.FileNode;
import org.eclipse.hawk.graph.ModelElementNode;

public class FileNodeWrapper implements IGraphNodeReference {

	private FileNode fileNode;
	private EOLQueryEngine model;

	public FileNodeWrapper(FileNode fileNode, EOLQueryEngine eolQueryEngine) {
		this.fileNode = fileNode;
		this.model = eolQueryEngine;
	}

	@Override
	public String getId() {
		return fileNode.getNode().getId().toString();
	}

	@Override
	public IGraphNode getNode() {
		return fileNode.getNode();
	}

	@Override
	public IQueryEngine getContainerModel() {
		return model;
	}

	@Override
	public String getTypeName() {
		return "_hawkFileNode";
	}

	public String getPath() {
		return fileNode.getFilePath();
	}

	public String getRepository() {
		return fileNode.getRepositoryURL();
	}

	public List<IGraphNodeReference> getRoots() {
		List<IGraphNodeReference> results = new ArrayList<>();
		for (ModelElementNode n : fileNode.getRootModelElements()) {
			results.add(model.wrap(n.getNode()));
		}
		return results;
	}

	public List<IGraphNodeReference> getContents() {
		List<IGraphNodeReference> results = new ArrayList<>();
		for (ModelElementNode n : fileNode.getModelElements()) {
			results.add(model.wrap(n.getNode()));
		}
		return results;
	}

	@Override
	public String toString() {
		return String.format("FNW|id:%s|type:%s", getId(), getTypeName());
	}

	@Override
	public int hashCode() {
		return Objects.hash(fileNode, model);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FileNodeWrapper other = (FileNodeWrapper) obj;
		return Objects.equals(fileNode, other.fileNode) && Objects.equals(model, other.model);
	}
}
