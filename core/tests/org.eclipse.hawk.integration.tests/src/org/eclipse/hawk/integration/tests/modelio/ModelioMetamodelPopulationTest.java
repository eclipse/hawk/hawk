/*******************************************************************************
 * Copyright (c) 2015-2018 The University of York, Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.integration.tests.modelio;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.hawk.backend.tests.factories.IGraphDatabaseFactory;
import org.eclipse.hawk.core.graph.IGraphTransaction;
import org.eclipse.hawk.core.model.IHawkClassifier;
import org.eclipse.hawk.graph.GraphWrapper;
import org.eclipse.hawk.graph.MetamodelNode;
import org.eclipse.hawk.graph.TypeNode;
import org.eclipse.hawk.graph.syncValidationListener.SyncValidationListener;
import org.eclipse.hawk.integration.tests.IntegrationTestSuite;
import org.eclipse.hawk.integration.tests.ModelIndexingTest;
import org.eclipse.hawk.modelio.exml.listeners.ModelioGraphChangeListener;
import org.eclipse.hawk.modelio.exml.metamodel.ModelioPackage;
import org.eclipse.hawk.modelio.exml.metamodel.register.MetamodelRegister;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;

public class ModelioMetamodelPopulationTest extends ModelIndexingTest {

	@Rule
	public GraphChangeListenerRule<SyncValidationListener> validationListener = new GraphChangeListenerRule<>(new SyncValidationListener());

	@Rule
	public GraphChangeListenerRule<ModelioGraphChangeListener> modelioListener = new GraphChangeListenerRule<>(new ModelioGraphChangeListener());

	@Parameters(name="{1}")
	public static Iterable<Object[]> params() {
		return IntegrationTestSuite.caseParams();
	}

	public ModelioMetamodelPopulationTest(File baseDir, IGraphDatabaseFactory dbf) {
		super(baseDir, dbf, new ModelioModelSupportFactory());
	}

	@Override
	public void setup() throws Throwable {
		super.setup();
		indexer.registerMetamodels(new File(new File(baseDir, ModelioModelSupportFactory.METAMODEL_PATH), "metamodel_descriptor.xml"));
	}

	@Test
	public void metamodel() throws Exception {

		int nTypes = 0;
		final Collection<ModelioPackage> pkgs = MetamodelRegister.INSTANCE.getRegisteredPackages();
		try (IGraphTransaction tx = db.beginTransaction()) {
			nTypes = visitPackages(nTypes, pkgs);
			tx.success();
		}

		// From 'grep -c MClass MMetamodel.java' on modelio-metamodel-lib
		assertEquals(409, nTypes);
	}

	protected int visitPackages(int nTypes, final Collection<ModelioPackage> pkgs) {
		final GraphWrapper gw = new GraphWrapper(db);

		for (ModelioPackage mpkg : pkgs) {
			MetamodelNode mmNode = gw.getMetamodelNodeByNsURI(mpkg.getNsURI());

			final Set<String> types = new HashSet<>();
			for (TypeNode typeNode : mmNode.getTypes()) {
				types.add(typeNode.getTypeName());
				++nTypes;
			}

			for (IHawkClassifier mc : mpkg.getClasses()) {
				assertTrue(types.contains(mc.getName()));
			}
		}

		return nTypes;
	}

	@Test
	public void zoo() throws Throwable {
		requestFolderIndex(new File(baseDir, "resources/models/zoo"));

		scheduleAndWait(() -> {
			assertNoErrors(validationListener.getListener());
			assertEquals(6, eol("return Class.all.size;"));
			return null;
		});
	}
}
