/*******************************************************************************
 * Copyright (c) 2023 The University of York.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.sqlite;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;

import org.apache.commons.codec.binary.Base64;
import org.eclipse.hawk.core.IConsole;
import org.eclipse.hawk.core.graph.IGraphDatabase;
import org.eclipse.hawk.core.graph.IGraphEdge;
import org.eclipse.hawk.core.graph.IGraphIterable;
import org.eclipse.hawk.core.graph.IGraphNode;
import org.eclipse.hawk.core.graph.IGraphNodeIndex;
import org.eclipse.hawk.core.graph.IGraphTransaction;
import org.eclipse.hawk.sqlite.iteration.StatementGraphNodeIterable;
import org.eclipse.hawk.sqlite.queries.IQueries;
import org.eclipse.hawk.sqlite.schema.ISchema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractSQLiteDatabase implements IGraphDatabase  {

	public class SQLiteConnection implements AutoCloseable {
		public static final String TYPE_BLOB_BASE64 = "blobBase64";

		private final Connection rawConn;
		private final Set<String> dropIndicesOnCommit = new HashSet<>();

		private final IQueries queries;
		private final ISchema schema;

		public SQLiteConnection(Connection rawConn, IQueries queries, ISchema schema) {
			this.rawConn = rawConn;
			this.queries = queries;
			this.schema = schema;
		}

		@Override
		public void close() throws SQLException {
			rawConn.close();
		}

		public Statement createStatement() throws SQLException {
			return rawConn.createStatement();
		}

		public IQueries getQueries() {
			return queries;
		}

		public ISchema getSchema() {
			return schema;
		}
		
		public void commit() throws SQLException {
			for (String idx : dropIndicesOnCommit) {
				schema.dropNodeIndexTable(idx);
			}
			dropIndicesOnCommit.clear();
			rawConn.commit();
		}

		public void rollback() throws SQLException {
			dropIndicesOnCommit.clear();
			rawConn.rollback();
		}

		public Object preprocessPropertyValue(Object value) throws IOException, SerialException, SQLException {
			if (value.getClass().isArray()) {
				// SQLite cannot store arrays - use Java serialisation for them
				ByteArrayOutputStream bOS = new ByteArrayOutputStream();
				try (ObjectOutputStream oOS = new ObjectOutputStream(bOS)) {
					oOS.writeObject(value);
				}
				value = new SerialBlob(bOS.toByteArray());
			}
			return value;
		}

		public Object getPropertyValue(PreparedStatement stmt) throws SQLException, IOException, ClassNotFoundException {
			try (ResultSet rs = stmt.getResultSet()) {
				if (rs.next()) {
					String type = rs.getString(1);

					switch (type) {
					case "Boolean":
						// SQLite has no boolean type - it stores true values as 1s
						return rs.getInt(2) == 1;
					case SQLiteConnection.TYPE_BLOB_BASE64:
						// SQLite has no array types - we serialise and store them as base64-encoded strings
						byte[] bytes = Base64.decodeBase64(rs.getString(2));
						try (ObjectInputStream oIS = new ObjectInputStream(new ByteArrayInputStream(bytes))) {
							return oIS.readObject();
						}
					default:
						return rs.getObject(2);
					}
				}
			}

			return null;
		}

		public Set<String> getStrings(PreparedStatement stmt) throws SQLException {
			try (ResultSet rs = stmt.getResultSet()) {
				Set<String> results = new HashSet<>();
				while (rs.next()) {
					results.add(rs.getString(1));
				}
				return results;
			}
		}

		public void dropIndexTableOnCommit(String name) {
			dropIndicesOnCommit.add(name);
		}
	}	

	private static final String SQLITE_FILE = "hawk.sqlite";

	protected abstract IQueries createQueries(Connection db);

	protected abstract ISchema createSchema(Connection db);

	private static final Logger LOGGER = LoggerFactory.getLogger(SQLiteDatabase.class);
	private Mode currentMode = Mode.TX_MODE;
	private File storageFolder;
	private File tempFolder;
	private SQLiteConnection conn;

	private static void deleteRecursively(File f) throws IOException {
		if (!f.exists()) return;

		Files.walkFileTree(f.toPath(), new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}
	
			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				Files.delete(dir);
				return FileVisitResult.CONTINUE;
			}
		});
	}

	@Override
	public String getPath() {
		return storageFolder.getAbsolutePath();
	}

	@Override
	public void run(File parentfolder, IConsole c) {
		run(parentfolder, "jdbc:sqlite:" + new File(parentfolder, SQLITE_FILE).getAbsolutePath());
	}

	/**
	 * SQLite-specific method that allows for providing a custom JDBC connection string.
	 */
	public void run(File parentfolder, String jdbcConnectionString) {
		this.storageFolder = parentfolder;
		this.tempFolder = new File(storageFolder, "temp");
		if (!tempFolder.exists()) {
			tempFolder.mkdirs();
		}

		try {
			Connection db = DriverManager.getConnection(jdbcConnectionString);
			db.setAutoCommit(false);
			conn = new SQLiteConnection(db, createQueries(db), createSchema(db));
			conn.getSchema().createSchema();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	@Override
	public void shutdown() throws Exception {
		if (conn != null) {
			conn.close();
			conn = null;
		}
	}

	@Override
	public void delete() throws Exception {
		shutdown();
		deleteRecursively(storageFolder);
	}

	@Override
	public IGraphNodeIndex getOrCreateNodeIndex(String name) {
		try {
			PreparedStatement stmt = conn.getQueries().getUpsertNodeIndexStatement(name);
			int rowCount = stmt.executeUpdate();
			if (rowCount > 0) {
				conn.getSchema().createNodeIndex(name);
			}
			return createNodeIndex(name);
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			return null;
		}
	}

	protected abstract IGraphNodeIndex createNodeIndex(String name);

	@Override
	public IGraphNodeIndex getMetamodelIndex() {
		return getOrCreateNodeIndex("_hawk_metamodels");
	}

	@Override
	public IGraphNodeIndex getFileIndex() {
		return getOrCreateNodeIndex("_hawk_files");
	}

	@Override
	public IGraphTransaction beginTransaction() throws Exception {
		return new SQLiteTransaction(this);
	}

	@Override
	public boolean isTransactional() {
		return true;
	}

	@Override
	public void enterBatchMode() {
		currentMode = Mode.NO_TX_MODE;
	}

	@Override
	public void exitBatchMode() {
		currentMode = Mode.TX_MODE;
	}

	@Override
	public IGraphIterable<? extends IGraphNode> allNodes(String label) {
		return new StatementGraphNodeIterable<>(
			() -> conn.getQueries().getNodeIDsByLabelStatement(label),
			() -> conn.getQueries().getNodeCountByLabelStatement(label),
			() -> conn.getQueries().getFirstNodeIDByLabelStatement(label),
			(o) -> this.createNode((int) o)
		);
	}

	@Override
	public IGraphNode createNode(Map<String, Object> props, String label) {
		try {
			PreparedStatement insertNodeStmt = conn.getQueries().getInsertNodeStatement(label);
			final int rowCount = insertNodeStmt.executeUpdate();
			assert rowCount == 1 : "A row should have been inserted for the node";
			int nodeId = insertNodeStmt.getGeneratedKeys().getInt(1);
	
			IGraphNode node = createNode(nodeId);
			if (props != null) {
				for (Entry<String, Object> e : props.entrySet()) {
					node.setProperty(e.getKey(), e.getValue());
				}
			}
	
			return node;
		} catch (SQLException ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
	
		return null;
	}

	protected abstract IGraphNode createNode(int nodeId);

	@Override
	public IGraphEdge createRelationship(IGraphNode start, IGraphNode end, String type, Map<String, Object> props) {
		final int startId = (int) start.getId();
		final int endId = (int) end.getId();
	
		try {
			PreparedStatement stmt = conn.getQueries().getInsertEdgeStatement(startId, endId, type);
			int rowCount = stmt.executeUpdate();
			if (rowCount == 0) {
				// Edge already existed: return it
				return start.getOutgoingWithType(type, Collections.singleton(end)).iterator().next();
			}

			assert rowCount == 1 : "One row should have been inserted for the edge";
			int edgeId = stmt.getGeneratedKeys().getInt(1);
			final IGraphEdge edge = createEdge(type, startId, endId, edgeId);
			if (props != null) {
				for (Map.Entry<String, Object> e : props.entrySet()) {
					edge.setProperty(e.getKey(), e.getValue());
				}
			}
	
			return edge;
		} catch (SQLException ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		
		return null;
	}

	protected abstract IGraphEdge createEdge(String type, final int startId, final int endId, int edgeId);

	@Override
	public SQLiteConnection getGraph() {
		return conn;
	}

	@Override
	public IGraphNode getNodeById(Object id) {
		if (id instanceof String) {
			return createNode(Integer.parseInt((String) id)); 
		} else {
			return createNode((int) id);
		}
	}

	@Override
	public String getTempDir() {
		return tempFolder.getAbsolutePath();
	}

	@Override
	public Mode currentMode() {
		return currentMode;
	}

	@Override
	public Set<String> getNodeIndexNames() {
		try {
			PreparedStatement stmt = conn.getQueries().getAllNodeIndicesStatement();
			stmt.execute();
			return conn.getStrings(stmt);
		} catch (SQLException ex) {
			LOGGER.error(ex.getMessage(), ex);
			return Collections.emptySet();
		}
	}

	public SQLiteConnection getConnection() {
		return conn;
	}

	public AbstractSQLiteDatabase() {
		super();
	}

}