package org.eclipse.hawk.service.artemis.server;

import org.eclipse.core.runtime.Plugin;

public class ArtemisServerPlugin extends Plugin {

	private static ArtemisServerPlugin instance;

	public static ArtemisServerPlugin getInstance() {
		return instance;
	}

	public ArtemisServerPlugin() {
		instance = this;
	}

}
