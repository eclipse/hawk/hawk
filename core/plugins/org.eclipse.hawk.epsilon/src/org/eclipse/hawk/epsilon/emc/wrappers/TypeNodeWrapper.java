/*******************************************************************************
 * Copyright (c) 2011-2016 The University of York.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.epsilon.emc.wrappers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.eclipse.hawk.core.graph.IGraphNode;
import org.eclipse.hawk.core.graph.IGraphTypeNodeReference;
import org.eclipse.hawk.core.query.IQueryEngine;
import org.eclipse.hawk.epsilon.emc.EOLQueryEngine;
import org.eclipse.hawk.graph.ModelElementNode;
import org.eclipse.hawk.graph.Slot;
import org.eclipse.hawk.graph.TypeNode;

public class TypeNodeWrapper implements IGraphTypeNodeReference {

	private final TypeNode typeNode;
	private final EOLQueryEngine model;

	public TypeNodeWrapper(TypeNode node, EOLQueryEngine model) {
		this.typeNode = node;
		this.model = model;
	}

	@Override
	public String getId() {
		return typeNode.getNode().getId().toString();
	}

	@Override
	public IGraphNode getNode() {
		return typeNode.getNode();
	}

	@Override
	public IQueryEngine getContainerModel() {
		return model;
	}

	@Override
	public String getTypeName() {
		return "_hawkTypeNode";
	}

	public String getName() {
		return typeNode.getTypeName();
	}

	public Collection<Object> getAll() {
		return model.getAllOf(typeNode.getNode(), ModelElementNode.EDGE_LABEL_OFKIND);
	}

	public List<SlotWrapper> getAttributes() {
		List<SlotWrapper> attributes = new ArrayList<>();
		for (Slot s : typeNode.getSlots().values()) {
			if (s.isAttribute()) {
				attributes.add(new SlotWrapper(s, model));
			}
		}
		return attributes;
	}

	public List<SlotWrapper> getReferences() {
		List<SlotWrapper> attributes = new ArrayList<>();
		for (Slot s : typeNode.getSlots().values()) {
			if (s.isReference()) {
				attributes.add(new SlotWrapper(s, model));
			}
		}
		return attributes;
	}

	public Collection<Slot> getFeatures() {
		return typeNode.getSlots().values();
	}

	public MetamodelNodeWrapper getMetamodel() {
		return new MetamodelNodeWrapper(typeNode.getMetamodel(), model);
	}

	public List<TypeNodeWrapper> getAllSupertypes() {
		return typeNode.getAllSupertypes().stream()
			.map(e -> new TypeNodeWrapper(e, model)).collect(Collectors.toList());
	}

	public List<TypeNodeWrapper> getAllSubtypes() {
		return typeNode.getAllSubtypes().stream()
			.map(e -> new TypeNodeWrapper(e, model)).collect(Collectors.toList());
	}

	@Override
	public String toString() {
		return String.format("TNW|id:%s|type:%s", getId(), getTypeName());
	}

	@Override
	public int hashCode() {
		return Objects.hash(model, typeNode);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TypeNodeWrapper other = (TypeNodeWrapper) obj;
		return Objects.equals(model, other.model) && Objects.equals(typeNode, other.typeNode);
	}

}