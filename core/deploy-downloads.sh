#!/bin/bash

VERSION=$(grep Bundle-Version "$WORKSPACE"/core/plugins/org.eclipse.hawk.core/META-INF/MANIFEST.MF | \
              cut --delim=: -f2 | \
              sed -re 's/ *([0-9]+)[.]([0-9]+)[.].*/\1.\2.0/')
DIST_DIR="/home/data/httpd/download.eclipse.org/hawk/$VERSION"
CORE_DIR="$DIST_DIR/core"
UPDATES_DIR="$DIST_DIR/updates"
THIRDPARTY_DIR="$DIST_DIR/thirdparty-nonp2"
SSH_USER=genie.hawk@projects-storage.eclipse.org

echo "Creating downloads for Hawk Core ${VERSION} at ${CORE_DIR} and ${UPDATES_DIR}"

# Use rsync with delete rather than a straight up 'rm', in case SSH fails intermittently
ssh "$SSH_USER" mkdir -p "$CORE_DIR"
rsync -rvhz "$WORKSPACE/core/releng/org.eclipse.hawk.updatesite/target/repository/" "$SSH_USER":"${CORE_DIR}/" --delete
rsync -rvhz "$WORKSPACE/core/releng/org.eclipse.hawk.updatesite/composite/" "$SSH_USER":"${UPDATES_DIR}/" --delete
rsync -rvhz "$WORKSPACE/core/releng/org.eclipse.hawk.updatesite/thirdparty-nonp2/" "$SSH_USER":"${THIRDPARTY_DIR}/" --delete
scp "$WORKSPACE"/core/releng/org.eclipse.hawk.updatesite/target/org.eclipse.hawk.updatesite-*.zip "$SSH_USER":"${DIST_DIR}/"

# Build source ZIP file (requested by Horacio)
pushd "$WORKSPACE"
SOURCES_BASENAME="hawk-sources-$VERSION"
git archive --format=zip --prefix="${SOURCES_BASENAME}-$(date '+%Y%m%d%H%M%S')-$(git rev-parse --short HEAD)/" -o "${SOURCES_BASENAME}.zip" HEAD
scp "${SOURCES_BASENAME}.zip" "$SSH_USER":"${DIST_DIR}/"
popd
