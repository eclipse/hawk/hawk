/*******************************************************************************
 * Copyright (c) 2020 Aston University.
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * This Source Code may also be made available under the following Secondary
 * Licenses when the conditions for such availability set forth in the Eclipse
 * Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
 *
 * SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
 *
 * Contributors:
 *     Antonio Garcia-Dominguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.hawk.ui2.mmselectors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

import org.eclipse.hawk.core.IModelIndexer;
import org.eclipse.swt.widgets.Shell;

/**
 * Component which presents a UI to the user to pick a set of metamodels from a
 * certain source and have them registered in a compatible indexer.
 */
public interface IHawkMetaModelResourceSelector {

	/**
	 * Eclipse extension point ID for all metamodel selectors.
	 */
	String EXT_ID = "org.eclipse.hawk.ui.metamodelSelector";

	/**
	 * Name of the attribute that holds the human-readable name that should be
	 * presented to the user to invoke the selector (e.g. as the text of a button).
	 */
	String EXT_ATTR_NAME = "buttonText";

	/**
	 * Name of the attribute which refers to an implementation of this class.
	 */
	String EXT_ATTR_SELECTOR = "class";

	/**
	 * Returns {@code true} if and only if the selector is applicable to this
	 * indexer. For instance, selectors that
	 */
	boolean isApplicableTo(IModelIndexer indexer);

	/**
	 * Presents a UI to register zero or more metamodels from a certain source.
	 *
	 * @param parentShell Parent shell for any dialogs that may need to be opened.
	 * @param indexer     Indexer into which the selected metamodels should be
	 *                    registered. The indexer will have passed the test in
	 *                    {@link #isApplicableTo(IModelIndexer)}.
	 * @throws Exception Exception while selecting or registering one or more
	 *                   metamodels.
	 */
	void selectMetamodels(Shell parentShell, IModelIndexer indexer) throws Exception;

	public static String[] getKnownMetamodelFilePatterns(IModelIndexer indexer) {
		final Set<String> extensions = indexer.getKnownMetamodelFileExtensions();

		final java.util.List<String> patterns = new ArrayList<>(extensions.size());
		for (String ext : extensions) {
			// NOTE: metamodel parsers always report metamodel file extensions with starting "."
			patterns.add("*" + ext);
		}
		Collections.sort(patterns);
		patterns.add("*.*");

		return patterns.toArray(new String[patterns.size()]);
	}

}
