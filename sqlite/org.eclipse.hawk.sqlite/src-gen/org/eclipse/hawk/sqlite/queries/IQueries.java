/*******************************************************************************
* Copyright (c) 2022-2023 The University of York.
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* http://www.eclipse.org/legal/epl-2.0.
*
* This Source Code may also be made available under the following Secondary
* Licenses when the conditions for such availability set forth in the Eclipse
* Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
*
* SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
*
* Contributors:
*     Antonio Garcia-Dominguez - initial API and implementation
******************************************************************************/
package org.eclipse.hawk.sqlite.queries;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.io.IOException;


/**
* Automatically generated query interface. DO NOT EDIT MANUALLY.
* @generated
*/
public interface IQueries {
	PreparedStatement getInsertNodeStatement(String label) throws SQLException;
	
	PreparedStatement getNodeIDsByLabelStatement(String label) throws SQLException;
	
	PreparedStatement getNodeCountByLabelStatement(String label) throws SQLException;
	
	PreparedStatement getFirstNodeIDByLabelStatement(String label) throws SQLException;
	
	PreparedStatement getNodePropKeysStatement(int nodeID) throws SQLException;
	
	PreparedStatement getNodePropValueStatement(int nodeID, String name) throws SQLException;
	
	PreparedStatement getUpsertNodePropStatement(int nodeID, String key, Object value) throws SQLException, IOException;
	
	PreparedStatement getDeleteNodePropStatement(int nodeID, String name) throws SQLException;
	
	PreparedStatement getDeleteNodeStatement(int nodeID) throws SQLException;
	
	PreparedStatement getDeleteNodePropsStatement(int nodeID) throws SQLException;
	
	PreparedStatement getInsertEdgeStatement(int startID, int endID, String label) throws SQLException;
	
	PreparedStatement getDeleteEdgeStatement(int edgeID) throws SQLException;
	
	PreparedStatement getOutgoingEdgesWithTypeStatement(String type, int fromID) throws SQLException;
	
	PreparedStatement getOutgoingEdgesWithTypeAndNodeStatement(String type, int fromID, int toID) throws SQLException;
	
	PreparedStatement getOutgoingEdgesStatement(int fromID) throws SQLException;
	
	PreparedStatement getIncomingEdgesWithTypeStatement(String type, int toID) throws SQLException;
	
	PreparedStatement getIncomingEdgesStatement(int toID) throws SQLException;
	
	PreparedStatement getEdgesStatement(int startOrEndID) throws SQLException;
	
	PreparedStatement getEdgesWithTypeStatement(String type, int startOrEndID) throws SQLException;
	
	PreparedStatement getEdgePropKeysStatement(int edgeID) throws SQLException;
	
	PreparedStatement getEdgePropValueStatement(int edgeID, String name) throws SQLException;
	
	PreparedStatement getUpsertEdgePropStatement(int edgeID, String key, Object value) throws SQLException, IOException;
	
	PreparedStatement getDeleteEdgePropStatement(int edgeID, String name) throws SQLException;
	
	PreparedStatement getDeleteEdgePropsStatement(int edgeID) throws SQLException;
	
	PreparedStatement getDeleteEdgePropsForNodeStatement(int nodeID) throws SQLException;
	
	PreparedStatement getDeleteEdgesForNodeStatement(int nodeID) throws SQLException;
	
	PreparedStatement getUpsertNodeIndexStatement(String name) throws SQLException;
	
	PreparedStatement getDeleteNodeIndexStatement(String name) throws SQLException;
	
	PreparedStatement getAllNodeIndicesStatement() throws SQLException;
	
	PreparedStatement getAddNodeIndexEntryStatement(String indexName, String key, int nodeId, Object value) throws SQLException;
	
	PreparedStatement getRemoveNodeIndexEntryStatement(String indexName, String key, int nodeId, Object value) throws SQLException;
	
	PreparedStatement getRemoveNodeFromIndexStatement(String indexName, int nodeId) throws SQLException;
	
	PreparedStatement getRemoveNodeFieldFromIndexStatement(String indexName, int nodeId, String key) throws SQLException;
	
	PreparedStatement getRemoveNodeValueFromIndexStatement(String indexName, int nodeId, Object value) throws SQLException;
	
	PreparedStatement getQueryIndexValuePatternStatement(String indexName, String key, String value) throws SQLException;
	
	PreparedStatement getQueryIndexValuePatternCountStatement(String indexName, String key, String value) throws SQLException;
	
	PreparedStatement getQueryIndexValuePatternSingleStatement(String indexName, String key, String value) throws SQLException;
	
	PreparedStatement getQueryIndexValueExactStatement(String indexName, String key, Object value) throws SQLException;
	
	PreparedStatement getQueryIndexValueExactCountStatement(String indexName, String key, Object value) throws SQLException;
	
	PreparedStatement getQueryIndexValueExactSingleStatement(String indexName, String key, Object value) throws SQLException;
	
	PreparedStatement getQueryIndexValueAllValuesStatement(String indexName, String key) throws SQLException;
	
	PreparedStatement getQueryIndexValueAllValuesCountStatement(String indexName, String key) throws SQLException;
	
	PreparedStatement getQueryIndexValueAllValuesSingleStatement(String indexName, String key) throws SQLException;
	
	PreparedStatement getQueryIndexValueAllPairsStatement(String indexName) throws SQLException;
	
	PreparedStatement getQueryIndexValueAllPairsCountStatement(String indexName) throws SQLException;
	
	PreparedStatement getQueryIndexValueAllPairsSingleStatement(String indexName) throws SQLException;
	
	PreparedStatement getQueryIndexNumberRangeStatement(String indexName, String key, boolean fromInclusive, Number from, boolean toInclusive, Number to) throws SQLException;
	
	PreparedStatement getQueryIndexNumberRangeCountStatement(String indexName, String key, boolean fromInclusive, Number from, boolean toInclusive, Number to) throws SQLException;
	
	PreparedStatement getQueryIndexNumberRangeSingleStatement(String indexName, String key, boolean fromInclusive, Number from, boolean toInclusive, Number to) throws SQLException;
	
}