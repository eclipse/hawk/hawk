/*******************************************************************************
* Copyright (c) 2022-2023 The University of York.
*
* This program and the accompanying materials are made available under the
* terms of the Eclipse Public License 2.0 which is available at
* http://www.eclipse.org/legal/epl-2.0.
*
* This Source Code may also be made available under the following Secondary
* Licenses when the conditions for such availability set forth in the Eclipse
* Public License, v. 2.0 are satisfied: GNU General Public License, version 3.
*
* SPDX-License-Identifier: EPL-2.0 OR GPL-3.0
*
* Contributors:
*     Antonio Garcia-Dominguez - initial API and implementation
******************************************************************************/
package org.eclipse.hawk.sqlite.queries;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Blob;

import java.util.Map;
import java.util.HashMap;
import java.util.function.Supplier;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
* Automatically generated query set. DO NOT EDIT MANUALLY.
* @generated
*/
public class TimeAwareQueries implements IQueries {
	public static final String TYPE_BLOB_BASE64 = "blobBase64";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TimeAwareQueries.class);
	
	private final Connection connection;
	private final Supplier<Long> defaultTimeProvider;
	private PreparedStatement stmtInsertNode;
	private PreparedStatement stmtDeleteNode;
	private PreparedStatement stmtNodeIDsByLabel;
	private PreparedStatement stmtNodeCountByLabel;
	private PreparedStatement stmtFirstNodeIDByLabel;
	private PreparedStatement stmtNodePropKeys;
	private PreparedStatement stmtNodePropValue;
	private PreparedStatement stmtUpsertNodeProp;
	private PreparedStatement stmtDeleteNodeProp;
	private PreparedStatement stmtDeleteNodeProps;
	private PreparedStatement stmtInsertEdge;
	private PreparedStatement stmtDeleteEdge;
	private PreparedStatement stmtOutgoingEdgesWithType;
	private PreparedStatement stmtOutgoingEdgesWithTypeAndNode;
	private PreparedStatement stmtOutgoingEdges;
	private PreparedStatement stmtIncomingEdgesWithType;
	private PreparedStatement stmtIncomingEdges;
	private PreparedStatement stmtEdges;
	private PreparedStatement stmtEdgesWithType;
	private PreparedStatement stmtEdgePropKeys;
	private PreparedStatement stmtEdgePropValue;
	private PreparedStatement stmtUpsertEdgeProp;
	private PreparedStatement stmtDeleteEdgeProp;
	private PreparedStatement stmtDeleteEdgeProps;
	private PreparedStatement stmtDeleteEdgePropsForNode;
	private PreparedStatement stmtDeleteEdgesForNode;
	private PreparedStatement stmtAllInstantsForNode;
	private PreparedStatement stmtNextInstant;
	private PreparedStatement stmtLatestInstant;
	private PreparedStatement stmtPrevInstant;
	private PreparedStatement stmtEarliestInstant;
	private PreparedStatement stmtInstantsBetween;
	private PreparedStatement stmtInstantsFrom;
	private PreparedStatement stmtInstantsUpTo;
	private PreparedStatement stmtIsAlive;
	private PreparedStatement stmtOutgoingEdgesWithTypeBetween;
	private PreparedStatement stmtIncomingEdgesWithTypeBetween;
	private PreparedStatement stmtOutgoingEdgesWithTypeCreatedBetween;
	private PreparedStatement stmtIncomingEdgesWithTypeCreatedBetween;
	private PreparedStatement stmtUpsertNodeIndex;
	private PreparedStatement stmtDeleteNodeIndex;
	private PreparedStatement stmtAllNodeIndices;
	private Map<String, PreparedStatement> stmtAddNodeIndexEntry = new HashMap<>();
	private Map<String, PreparedStatement> stmtAnnotateNodeIndexEntry = new HashMap<>();
	private Map<String, PreparedStatement> stmtRemoveNodeIndexEntry = new HashMap<>();
	private Map<String, PreparedStatement> stmtRemoveNodeFromIndex = new HashMap<>();
	private Map<String, PreparedStatement> stmtRemoveNodeFieldFromIndex = new HashMap<>();
	private Map<String, PreparedStatement> stmtRemoveNodeValueFromIndex = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValuePattern = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValuePatternCount = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValuePatternSingle = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValueExact = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValueExactCount = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValueExactSingle = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValueAllValues = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValueAllValuesCount = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValueAllValuesSingle = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValueAllPairs = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValueAllPairsCount = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexValueAllPairsSingle = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexNumberRange = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexNumberRangeCount = new HashMap<>();
	private Map<String, PreparedStatement> stmtQueryIndexNumberRangeSingle = new HashMap<>();
	private Map<String, PreparedStatement> stmtIndexVersions = new HashMap<>();
	private Map<String, PreparedStatement> stmtIndexEarliestVersionSince = new HashMap<>();
	
	public TimeAwareQueries(Connection connection, Supplier<Long> defaultTimeProvider) {
		this.connection = connection;
		this.defaultTimeProvider = defaultTimeProvider;
	}
	
	public PreparedStatement getInsertNodeStatement(String label, long time) throws SQLException {
		if (stmtInsertNode == null) {
			stmtInsertNode = connection.prepareStatement("INSERT INTO nodes (label, validFrom) VALUES (?, ?);");
		}
		stmtInsertNode.setString(1, label);
		stmtInsertNode.setLong(2, time);
		return stmtInsertNode;
	}
	
	@Override
	public PreparedStatement getInsertNodeStatement(String label) throws SQLException {
		return this.getInsertNodeStatement(label, defaultTimeProvider.get());
	}
	
	public PreparedStatement getDeleteNodeStatement(int id, long time) throws SQLException {
		if (stmtDeleteNode == null) {
			stmtDeleteNode = connection.prepareStatement("UPDATE nodes SET validTo = ? WHERE rowid = ? AND validTo IS NULL;");
		}
		stmtDeleteNode.setLong(1, time);
		stmtDeleteNode.setInt(2, id);
		return stmtDeleteNode;
	}
	
	@Override
	public PreparedStatement getDeleteNodeStatement(int id) throws SQLException {
		return this.getDeleteNodeStatement(id, defaultTimeProvider.get());
	}
	
	public PreparedStatement getNodeIDsByLabelStatement(String label, long time) throws SQLException {
		if (stmtNodeIDsByLabel == null) {
			stmtNodeIDsByLabel = connection.prepareStatement("SELECT rowid FROM nodes WHERE label = ? AND validFrom <= ? AND (validTo IS NULL OR validTo > ?);");
		}
		stmtNodeIDsByLabel.setString(1, label);
		stmtNodeIDsByLabel.setLong(2, time);
		stmtNodeIDsByLabel.setLong(3, time);
		return stmtNodeIDsByLabel;
	}
	
	@Override
	public PreparedStatement getNodeIDsByLabelStatement(String label) throws SQLException {
		return this.getNodeIDsByLabelStatement(label, defaultTimeProvider.get());
	}
	
	public PreparedStatement getNodeCountByLabelStatement(String label, long time) throws SQLException {
		if (stmtNodeCountByLabel == null) {
			stmtNodeCountByLabel = connection.prepareStatement("SELECT COUNT(1) FROM nodes WHERE label = ? AND validFrom <= ? AND (validTo IS NULL OR validTo > ?);");
		}
		stmtNodeCountByLabel.setString(1, label);
		stmtNodeCountByLabel.setLong(2, time);
		stmtNodeCountByLabel.setLong(3, time);
		return stmtNodeCountByLabel;
	}
	
	@Override
	public PreparedStatement getNodeCountByLabelStatement(String label) throws SQLException {
		return this.getNodeCountByLabelStatement(label, defaultTimeProvider.get());
	}
	
	public PreparedStatement getFirstNodeIDByLabelStatement(String label, long time) throws SQLException {
		if (stmtFirstNodeIDByLabel == null) {
			stmtFirstNodeIDByLabel = connection.prepareStatement("SELECT rowid FROM nodes WHERE label = ? AND validFrom <= ? AND (validTo IS NULL OR validTo > ?) LIMIT 1;");
		}
		stmtFirstNodeIDByLabel.setString(1, label);
		stmtFirstNodeIDByLabel.setLong(2, time);
		stmtFirstNodeIDByLabel.setLong(3, time);
		return stmtFirstNodeIDByLabel;
	}
	
	@Override
	public PreparedStatement getFirstNodeIDByLabelStatement(String label) throws SQLException {
		return this.getFirstNodeIDByLabelStatement(label, defaultTimeProvider.get());
	}
	
	public PreparedStatement getNodePropKeysStatement(int id, long time) throws SQLException {
		if (stmtNodePropKeys == null) {
			stmtNodePropKeys = connection.prepareStatement("SELECT DISTINCT key FROM ( SELECT nodeid, key, max(validAt) AS validAt FROM nodeprops WHERE nodeid = ? AND validAt <= ? GROUP BY nodeid, key ) JOIN nodeprops USING (nodeid, key, validAt) WHERE value IS NOT NULL;");
		}
		stmtNodePropKeys.setInt(1, id);
		stmtNodePropKeys.setLong(2, time);
		return stmtNodePropKeys;
	}
	
	@Override
	public PreparedStatement getNodePropKeysStatement(int id) throws SQLException {
		return this.getNodePropKeysStatement(id, defaultTimeProvider.get());
	}
	
	public PreparedStatement getNodePropValueStatement(int id, String name, long time) throws SQLException {
		if (stmtNodePropValue == null) {
			stmtNodePropValue = connection.prepareStatement("SELECT type, value FROM nodeprops WHERE nodeid = ? AND key = ? AND validAt <= ? ORDER BY validAt DESC LIMIT 1;");
		}
		stmtNodePropValue.setInt(1, id);
		stmtNodePropValue.setString(2, name);
		stmtNodePropValue.setLong(3, time);
		return stmtNodePropValue;
	}
	
	@Override
	public PreparedStatement getNodePropValueStatement(int id, String name) throws SQLException {
		return this.getNodePropValueStatement(id, name, defaultTimeProvider.get());
	}
	
	public PreparedStatement getUpsertNodePropStatement(int id, String key, Object value, long time) throws SQLException, IOException {
		if (stmtUpsertNodeProp == null) {
			stmtUpsertNodeProp = connection.prepareStatement("INSERT OR REPLACE INTO nodeprops (nodeid, key, type, value, validAt) VALUES (?, ?, ?, ?, ?);");
		}
		stmtUpsertNodeProp.setInt(1, id);
		stmtUpsertNodeProp.setString(2, key);
		stmtUpsertNodeProp.setString(3, getPropertyType(value));
		if (value instanceof Blob) {
			String value_base64 = base64(value);
			stmtUpsertNodeProp.setString(4, value_base64);
		} else {
			stmtUpsertNodeProp.setObject(4, value);
		}
		stmtUpsertNodeProp.setLong(5, time);
		return stmtUpsertNodeProp;
	}
	
	@Override
	public PreparedStatement getUpsertNodePropStatement(int id, String key, Object value) throws SQLException, IOException {
		return this.getUpsertNodePropStatement(id, key, value, defaultTimeProvider.get());
	}
	
	public PreparedStatement getDeleteNodePropStatement(int id, String name, long time) throws SQLException {
		if (stmtDeleteNodeProp == null) {
			stmtDeleteNodeProp = connection.prepareStatement("INSERT OR REPLACE INTO nodeprops (nodeid, key, type, value, validAt) SELECT ?, ?, 'deleted', NULL, ? WHERE EXISTS ( SELECT 1 FROM nodeprops WHERE nodeid = ? AND key = ? AND validAt = ( SELECT MAX(validAt) FROM nodeprops WHERE nodeid = ? AND key = ? AND validAt <= ? ) AND value IS NOT NULL );");
		}
		stmtDeleteNodeProp.setInt(1, id);
		stmtDeleteNodeProp.setInt(4, id);
		stmtDeleteNodeProp.setInt(6, id);
		stmtDeleteNodeProp.setString(2, name);
		stmtDeleteNodeProp.setString(5, name);
		stmtDeleteNodeProp.setString(7, name);
		stmtDeleteNodeProp.setLong(3, time);
		stmtDeleteNodeProp.setLong(8, time);
		return stmtDeleteNodeProp;
	}
	
	@Override
	public PreparedStatement getDeleteNodePropStatement(int id, String name) throws SQLException {
		return this.getDeleteNodePropStatement(id, name, defaultTimeProvider.get());
	}
	
	public PreparedStatement getDeleteNodePropsStatement(int id, long time) throws SQLException {
		if (stmtDeleteNodeProps == null) {
			stmtDeleteNodeProps = connection.prepareStatement("INSERT OR REPLACE INTO nodeprops (nodeid, key, type, value, validAt) SELECT p.nodeid, p.key, 'deleted', NULL, ? FROM (SELECT nodeid, key, max(validAt) AS validAt FROM nodeprops WHERE nodeid = ? AND validAt <= ? GROUP BY nodeid, key) JOIN nodeprops p USING (nodeid, key, validAt) WHERE p.value IS NOT NULL;");
		}
		stmtDeleteNodeProps.setLong(1, time);
		stmtDeleteNodeProps.setInt(2, id);
		stmtDeleteNodeProps.setLong(3, time);
		return stmtDeleteNodeProps;
	}
	
	@Override
	public PreparedStatement getDeleteNodePropsStatement(int id) throws SQLException {
		return this.getDeleteNodePropsStatement(id, defaultTimeProvider.get());
	}
	
	public PreparedStatement getInsertEdgeStatement(int startID, int endID, String label, long time) throws SQLException {
		if (stmtInsertEdge == null) {
			stmtInsertEdge = connection.prepareStatement("INSERT OR REPLACE INTO edges (startId, endId, label, validFrom, validTo) VALUES (?, ?, ?, ?, NULL) ON CONFLICT DO UPDATE SET validTo = NULL;");
		}
		stmtInsertEdge.setInt(1, startID);
		stmtInsertEdge.setInt(2, endID);
		stmtInsertEdge.setString(3, label);
		stmtInsertEdge.setLong(4, time);
		return stmtInsertEdge;
	}
	
	@Override
	public PreparedStatement getInsertEdgeStatement(int startID, int endID, String label) throws SQLException {
		return this.getInsertEdgeStatement(startID, endID, label, defaultTimeProvider.get());
	}
	
	public PreparedStatement getDeleteEdgeStatement(int id, long time) throws SQLException {
		if (stmtDeleteEdge == null) {
			stmtDeleteEdge = connection.prepareStatement("UPDATE edges SET validTo = ? WHERE rowid = ? AND validTo IS NULL;");
		}
		stmtDeleteEdge.setLong(1, time);
		stmtDeleteEdge.setInt(2, id);
		return stmtDeleteEdge;
	}
	
	@Override
	public PreparedStatement getDeleteEdgeStatement(int id) throws SQLException {
		return this.getDeleteEdgeStatement(id, defaultTimeProvider.get());
	}
	
	public PreparedStatement getOutgoingEdgesWithTypeStatement(String type, int fromID, long time) throws SQLException {
		if (stmtOutgoingEdgesWithType == null) {
			stmtOutgoingEdgesWithType = connection.prepareStatement("SELECT rowid, endId FROM edges WHERE label = ? AND startId = ? AND validFrom <= ? AND (validTo IS NULL OR validTo > ?);");
		}
		stmtOutgoingEdgesWithType.setString(1, type);
		stmtOutgoingEdgesWithType.setInt(2, fromID);
		stmtOutgoingEdgesWithType.setLong(3, time);
		stmtOutgoingEdgesWithType.setLong(4, time);
		return stmtOutgoingEdgesWithType;
	}
	
	@Override
	public PreparedStatement getOutgoingEdgesWithTypeStatement(String type, int fromID) throws SQLException {
		return this.getOutgoingEdgesWithTypeStatement(type, fromID, defaultTimeProvider.get());
	}
	
	public PreparedStatement getOutgoingEdgesWithTypeAndNodeStatement(String type, int fromID, int toID, long time) throws SQLException {
		if (stmtOutgoingEdgesWithTypeAndNode == null) {
			stmtOutgoingEdgesWithTypeAndNode = connection.prepareStatement("SELECT rowid, endId FROM edges WHERE label = ? AND startId = ? AND endId = ? AND validFrom <= ? AND (validTo IS NULL OR validTo > ?);");
		}
		stmtOutgoingEdgesWithTypeAndNode.setString(1, type);
		stmtOutgoingEdgesWithTypeAndNode.setInt(2, fromID);
		stmtOutgoingEdgesWithTypeAndNode.setInt(3, toID);
		stmtOutgoingEdgesWithTypeAndNode.setLong(4, time);
		stmtOutgoingEdgesWithTypeAndNode.setLong(5, time);
		return stmtOutgoingEdgesWithTypeAndNode;
	}
	
	@Override
	public PreparedStatement getOutgoingEdgesWithTypeAndNodeStatement(String type, int fromID, int toID) throws SQLException {
		return this.getOutgoingEdgesWithTypeAndNodeStatement(type, fromID, toID, defaultTimeProvider.get());
	}
	
	public PreparedStatement getOutgoingEdgesStatement(int fromID, long time) throws SQLException {
		if (stmtOutgoingEdges == null) {
			stmtOutgoingEdges = connection.prepareStatement("SELECT rowid, endId, label FROM edges WHERE startId = ? AND validFrom <= ? AND (validTo IS NULL OR validTo > ?);");
		}
		stmtOutgoingEdges.setInt(1, fromID);
		stmtOutgoingEdges.setLong(2, time);
		stmtOutgoingEdges.setLong(3, time);
		return stmtOutgoingEdges;
	}
	
	@Override
	public PreparedStatement getOutgoingEdgesStatement(int fromID) throws SQLException {
		return this.getOutgoingEdgesStatement(fromID, defaultTimeProvider.get());
	}
	
	public PreparedStatement getIncomingEdgesWithTypeStatement(String type, int toID, long time) throws SQLException {
		if (stmtIncomingEdgesWithType == null) {
			stmtIncomingEdgesWithType = connection.prepareStatement("SELECT rowid, startId FROM edges WHERE label = ? AND endId = ? AND validFrom <= ? AND (validTo IS NULL OR validTo > ?);");
		}
		stmtIncomingEdgesWithType.setString(1, type);
		stmtIncomingEdgesWithType.setInt(2, toID);
		stmtIncomingEdgesWithType.setLong(3, time);
		stmtIncomingEdgesWithType.setLong(4, time);
		return stmtIncomingEdgesWithType;
	}
	
	@Override
	public PreparedStatement getIncomingEdgesWithTypeStatement(String type, int toID) throws SQLException {
		return this.getIncomingEdgesWithTypeStatement(type, toID, defaultTimeProvider.get());
	}
	
	public PreparedStatement getIncomingEdgesStatement(int toID, long time) throws SQLException {
		if (stmtIncomingEdges == null) {
			stmtIncomingEdges = connection.prepareStatement("SELECT rowid, startId, label FROM edges WHERE endId = ? AND validFrom <= ? AND (validTo IS NULL OR validTo > ?);");
		}
		stmtIncomingEdges.setInt(1, toID);
		stmtIncomingEdges.setLong(2, time);
		stmtIncomingEdges.setLong(3, time);
		return stmtIncomingEdges;
	}
	
	@Override
	public PreparedStatement getIncomingEdgesStatement(int toID) throws SQLException {
		return this.getIncomingEdgesStatement(toID, defaultTimeProvider.get());
	}
	
	public PreparedStatement getEdgesStatement(int startOrEndID, long time) throws SQLException {
		if (stmtEdges == null) {
			stmtEdges = connection.prepareStatement("SELECT rowid, startId, endId, label FROM edges WHERE (startId = ? OR endId = ?) AND validFrom <= ? AND (validTo IS NULL OR validTo > ?);");
		}
		stmtEdges.setInt(1, startOrEndID);
		stmtEdges.setInt(2, startOrEndID);
		stmtEdges.setLong(3, time);
		stmtEdges.setLong(4, time);
		return stmtEdges;
	}
	
	@Override
	public PreparedStatement getEdgesStatement(int startOrEndID) throws SQLException {
		return this.getEdgesStatement(startOrEndID, defaultTimeProvider.get());
	}
	
	public PreparedStatement getEdgesWithTypeStatement(String type, int startOrEndID, long time) throws SQLException {
		if (stmtEdgesWithType == null) {
			stmtEdgesWithType = connection.prepareStatement("SELECT rowid, startId, endid FROM edges WHERE label = ? AND (startId = ? OR endId = ?) AND validFrom <= ? AND (validTo IS NULL OR validTo > ?);");
		}
		stmtEdgesWithType.setString(1, type);
		stmtEdgesWithType.setInt(2, startOrEndID);
		stmtEdgesWithType.setInt(3, startOrEndID);
		stmtEdgesWithType.setLong(4, time);
		stmtEdgesWithType.setLong(5, time);
		return stmtEdgesWithType;
	}
	
	@Override
	public PreparedStatement getEdgesWithTypeStatement(String type, int startOrEndID) throws SQLException {
		return this.getEdgesWithTypeStatement(type, startOrEndID, defaultTimeProvider.get());
	}
	
	public PreparedStatement getEdgePropKeysStatement(int id, long time) throws SQLException {
		if (stmtEdgePropKeys == null) {
			stmtEdgePropKeys = connection.prepareStatement("SELECT DISTINCT key FROM ( SELECT edgeid, key, max(validAt) AS validAt FROM edgeprops WHERE edgeid = ? AND validAt <= ? GROUP BY edgeid, key ) JOIN edgeprops USING (edgeid, key, validAt) WHERE value IS NOT NULL;");
		}
		stmtEdgePropKeys.setInt(1, id);
		stmtEdgePropKeys.setLong(2, time);
		return stmtEdgePropKeys;
	}
	
	@Override
	public PreparedStatement getEdgePropKeysStatement(int id) throws SQLException {
		return this.getEdgePropKeysStatement(id, defaultTimeProvider.get());
	}
	
	public PreparedStatement getEdgePropValueStatement(int id, String name, long time) throws SQLException {
		if (stmtEdgePropValue == null) {
			stmtEdgePropValue = connection.prepareStatement("SELECT type, value FROM edgeprops WHERE edgeid = ? AND key = ? AND validAt <= ? ORDER BY validAt DESC LIMIT 1;");
		}
		stmtEdgePropValue.setInt(1, id);
		stmtEdgePropValue.setString(2, name);
		stmtEdgePropValue.setLong(3, time);
		return stmtEdgePropValue;
	}
	
	@Override
	public PreparedStatement getEdgePropValueStatement(int id, String name) throws SQLException {
		return this.getEdgePropValueStatement(id, name, defaultTimeProvider.get());
	}
	
	public PreparedStatement getUpsertEdgePropStatement(int id, String key, Object value, long time) throws SQLException, IOException {
		if (stmtUpsertEdgeProp == null) {
			stmtUpsertEdgeProp = connection.prepareStatement("INSERT OR REPLACE INTO edgeprops (edgeid, key, type, value, validAt) VALUES (?, ?, ?, ?, ?);");
		}
		stmtUpsertEdgeProp.setInt(1, id);
		stmtUpsertEdgeProp.setString(2, key);
		stmtUpsertEdgeProp.setString(3, getPropertyType(value));
		if (value instanceof Blob) {
			String value_base64 = base64(value);
			stmtUpsertEdgeProp.setString(4, value_base64);
		} else {
			stmtUpsertEdgeProp.setObject(4, value);
		}
		stmtUpsertEdgeProp.setLong(5, time);
		return stmtUpsertEdgeProp;
	}
	
	@Override
	public PreparedStatement getUpsertEdgePropStatement(int id, String key, Object value) throws SQLException, IOException {
		return this.getUpsertEdgePropStatement(id, key, value, defaultTimeProvider.get());
	}
	
	public PreparedStatement getDeleteEdgePropStatement(int id, String name, long time) throws SQLException {
		if (stmtDeleteEdgeProp == null) {
			stmtDeleteEdgeProp = connection.prepareStatement("INSERT OR REPLACE INTO edgeprops (edgeid, key, type, value, validAt) SELECT ?, ?, 'deleted', NULL, ? WHERE EXISTS ( SELECT 1 FROM edgeprops WHERE edgeid = ? AND key = ? AND validAt = ( SELECT MAX(validAt) FROM edgeprops WHERE edgeid = ? AND key = ? AND validAt <= ? ) AND value IS NOT NULL );");
		}
		stmtDeleteEdgeProp.setInt(1, id);
		stmtDeleteEdgeProp.setInt(4, id);
		stmtDeleteEdgeProp.setInt(6, id);
		stmtDeleteEdgeProp.setString(2, name);
		stmtDeleteEdgeProp.setString(5, name);
		stmtDeleteEdgeProp.setString(7, name);
		stmtDeleteEdgeProp.setLong(3, time);
		stmtDeleteEdgeProp.setLong(8, time);
		return stmtDeleteEdgeProp;
	}
	
	@Override
	public PreparedStatement getDeleteEdgePropStatement(int id, String name) throws SQLException {
		return this.getDeleteEdgePropStatement(id, name, defaultTimeProvider.get());
	}
	
	public PreparedStatement getDeleteEdgePropsStatement(int id, long time) throws SQLException {
		if (stmtDeleteEdgeProps == null) {
			stmtDeleteEdgeProps = connection.prepareStatement("INSERT OR REPLACE INTO edgeprops (edgeid, key, type, value, validAt) SELECT p.edgeid, p.key, 'deleted', NULL, ? FROM (SELECT edgeid, key, max(validAt) AS validAt FROM edgeprops WHERE edgeid = ? AND validAt <= ? GROUP BY edgeid, key) JOIN edgeprops p USING (edgeid, key, validAt) WHERE p.value IS NOT NULL;");
		}
		stmtDeleteEdgeProps.setLong(1, time);
		stmtDeleteEdgeProps.setInt(2, id);
		stmtDeleteEdgeProps.setLong(3, time);
		return stmtDeleteEdgeProps;
	}
	
	@Override
	public PreparedStatement getDeleteEdgePropsStatement(int id) throws SQLException {
		return this.getDeleteEdgePropsStatement(id, defaultTimeProvider.get());
	}
	
	public PreparedStatement getDeleteEdgePropsForNodeStatement(int nodeID, long time) throws SQLException {
		if (stmtDeleteEdgePropsForNode == null) {
			stmtDeleteEdgePropsForNode = connection.prepareStatement("INSERT OR REPLACE INTO edgeprops (edgeid, key, type, value, validAt) SELECT p.edgeid, p.key, 'deleted', NULL, ? FROM (SELECT edgeid, key, max(validAt) AS validAt FROM edgeprops WHERE edgeid IN ( SELECT rowid FROM edges WHERE startId = ? OR endId = ? ) AND validAt <= ? GROUP BY edgeid, key) JOIN edgeprops p USING (edgeid, key, validAt) WHERE p.value IS NOT NULL;");
		}
		stmtDeleteEdgePropsForNode.setLong(1, time);
		stmtDeleteEdgePropsForNode.setLong(3, time);
		stmtDeleteEdgePropsForNode.setInt(2, nodeID);
		stmtDeleteEdgePropsForNode.setLong(4, time);
		return stmtDeleteEdgePropsForNode;
	}
	
	@Override
	public PreparedStatement getDeleteEdgePropsForNodeStatement(int nodeID) throws SQLException {
		return this.getDeleteEdgePropsForNodeStatement(nodeID, defaultTimeProvider.get());
	}
	
	public PreparedStatement getDeleteEdgesForNodeStatement(int nodeID, long time) throws SQLException {
		if (stmtDeleteEdgesForNode == null) {
			stmtDeleteEdgesForNode = connection.prepareStatement("UPDATE edges SET validTo = ? WHERE startId = ? OR endId = ?;");
		}
		stmtDeleteEdgesForNode.setLong(1, time);
		stmtDeleteEdgesForNode.setInt(2, nodeID);
		stmtDeleteEdgesForNode.setInt(3, nodeID);
		return stmtDeleteEdgesForNode;
	}
	
	@Override
	public PreparedStatement getDeleteEdgesForNodeStatement(int nodeID) throws SQLException {
		return this.getDeleteEdgesForNodeStatement(nodeID, defaultTimeProvider.get());
	}
	
	public PreparedStatement getAllInstantsForNodeStatement(int nodeID) throws SQLException {
		if (stmtAllInstantsForNode == null) {
			stmtAllInstantsForNode = connection.prepareStatement("SELECT DISTINCT tp.timepoint FROM ( SELECT validFrom AS timepoint FROM nodes WHERE rowid = ? UNION SELECT validAt FROM nodeprops WHERE nodeid = ? UNION SELECT validFrom FROM edges WHERE startId = ? OR endId = ? UNION SELECT validTo FROM edges WHERE validTo IS NOT NULL AND (startId = ? OR endId = ?) ) AS tp JOIN (SELECT validTo FROM nodes WHERE rowid = ?) AS tpThreshold ON tpThreshold.validTo IS NULL OR tpThreshold.validTo > tp.timepoint ORDER BY tp.timepoint DESC;");
		}
		stmtAllInstantsForNode.setInt(1, nodeID);
		stmtAllInstantsForNode.setInt(2, nodeID);
		stmtAllInstantsForNode.setInt(3, nodeID);
		stmtAllInstantsForNode.setInt(4, nodeID);
		stmtAllInstantsForNode.setInt(5, nodeID);
		stmtAllInstantsForNode.setInt(6, nodeID);
		stmtAllInstantsForNode.setInt(7, nodeID);
		return stmtAllInstantsForNode;
	}
	
	public PreparedStatement getNextInstantStatement(int nodeID, long time) throws SQLException {
		if (stmtNextInstant == null) {
			stmtNextInstant = connection.prepareStatement("SELECT DISTINCT tp.timepoint FROM ( SELECT validFrom AS timepoint FROM nodes WHERE rowid = ? UNION SELECT validAt FROM nodeprops WHERE nodeid = ? UNION SELECT validFrom FROM edges WHERE startId = ? OR endId = ? UNION SELECT validTo FROM edges WHERE validTo IS NOT NULL AND (startId = ? OR endId = ?) ) AS tp JOIN (SELECT validTo FROM nodes WHERE rowid = ?) AS tpThreshold ON tpThreshold.validTo IS NULL OR tpThreshold.validTo > tp.timepoint WHERE tp.timepoint > ? ORDER BY tp.timepoint ASC LIMIT 1;");
		}
		stmtNextInstant.setInt(1, nodeID);
		stmtNextInstant.setInt(2, nodeID);
		stmtNextInstant.setInt(3, nodeID);
		stmtNextInstant.setInt(4, nodeID);
		stmtNextInstant.setInt(5, nodeID);
		stmtNextInstant.setInt(6, nodeID);
		stmtNextInstant.setInt(7, nodeID);
		stmtNextInstant.setLong(8, time);
		return stmtNextInstant;
	}
	
	public PreparedStatement getNextInstantStatement(int nodeID) throws SQLException {
		return this.getNextInstantStatement(nodeID, defaultTimeProvider.get());
	}
	
	public PreparedStatement getLatestInstantStatement(int nodeID) throws SQLException {
		if (stmtLatestInstant == null) {
			stmtLatestInstant = connection.prepareStatement("SELECT DISTINCT tp.timepoint FROM ( SELECT validFrom AS timepoint FROM nodes WHERE rowid = ? UNION SELECT validAt FROM nodeprops WHERE nodeid = ? UNION SELECT validFrom FROM edges WHERE startId = ? OR endId = ? UNION SELECT validTo FROM edges WHERE validTo IS NOT NULL AND (startId = ? OR endId = ?) ) AS tp JOIN (SELECT validTo FROM nodes WHERE rowid = ?) AS tpThreshold ON tpThreshold.validTo IS NULL OR tpThreshold.validTo > tp.timepoint ORDER BY tp.timepoint DESC LIMIT 1;");
		}
		stmtLatestInstant.setInt(1, nodeID);
		stmtLatestInstant.setInt(2, nodeID);
		stmtLatestInstant.setInt(3, nodeID);
		stmtLatestInstant.setInt(4, nodeID);
		stmtLatestInstant.setInt(5, nodeID);
		stmtLatestInstant.setInt(6, nodeID);
		stmtLatestInstant.setInt(7, nodeID);
		return stmtLatestInstant;
	}
	
	public PreparedStatement getPrevInstantStatement(int nodeID, long time) throws SQLException {
		if (stmtPrevInstant == null) {
			stmtPrevInstant = connection.prepareStatement("SELECT DISTINCT tp.timepoint FROM ( SELECT validFrom AS timepoint FROM nodes WHERE rowid = ? UNION SELECT validAt FROM nodeprops WHERE nodeid = ? UNION SELECT validFrom FROM edges WHERE startId = ? OR endId = ? UNION SELECT validTo FROM edges WHERE validTo IS NOT NULL AND (startId = ? OR endId = ?) ) AS tp JOIN (SELECT validTo FROM nodes WHERE rowid = ?) AS tpThreshold ON tpThreshold.validTo IS NULL OR tpThreshold.validTo > tp.timepoint WHERE tp.timepoint < ? ORDER BY tp.timepoint DESC LIMIT 1;");
		}
		stmtPrevInstant.setInt(1, nodeID);
		stmtPrevInstant.setInt(2, nodeID);
		stmtPrevInstant.setInt(3, nodeID);
		stmtPrevInstant.setInt(4, nodeID);
		stmtPrevInstant.setInt(5, nodeID);
		stmtPrevInstant.setInt(6, nodeID);
		stmtPrevInstant.setInt(7, nodeID);
		stmtPrevInstant.setLong(8, time);
		return stmtPrevInstant;
	}
	
	public PreparedStatement getPrevInstantStatement(int nodeID) throws SQLException {
		return this.getPrevInstantStatement(nodeID, defaultTimeProvider.get());
	}
	
	public PreparedStatement getEarliestInstantStatement(int nodeID) throws SQLException {
		if (stmtEarliestInstant == null) {
			stmtEarliestInstant = connection.prepareStatement("SELECT DISTINCT tp.timepoint FROM ( SELECT validFrom AS timepoint FROM nodes WHERE rowid = ? UNION SELECT validAt FROM nodeprops WHERE nodeid = ? UNION SELECT validFrom FROM edges WHERE startId = ? OR endId = ? UNION SELECT validTo FROM edges WHERE validTo IS NOT NULL AND (startId = ? OR endId = ?) ) AS tp JOIN (SELECT validTo FROM nodes WHERE rowid = ?) AS tpThreshold ON tpThreshold.validTo IS NULL OR tpThreshold.validTo > tp.timepoint ORDER BY tp.timepoint ASC LIMIT 1;");
		}
		stmtEarliestInstant.setInt(1, nodeID);
		stmtEarliestInstant.setInt(2, nodeID);
		stmtEarliestInstant.setInt(3, nodeID);
		stmtEarliestInstant.setInt(4, nodeID);
		stmtEarliestInstant.setInt(5, nodeID);
		stmtEarliestInstant.setInt(6, nodeID);
		stmtEarliestInstant.setInt(7, nodeID);
		return stmtEarliestInstant;
	}
	
	public PreparedStatement getInstantsBetweenStatement(int nodeID, long fromInclusive, long toInclusive) throws SQLException {
		if (stmtInstantsBetween == null) {
			stmtInstantsBetween = connection.prepareStatement("SELECT DISTINCT tp.timepoint FROM ( SELECT validFrom AS timepoint FROM nodes WHERE rowid = ? UNION SELECT validAt FROM nodeprops WHERE nodeid = ? UNION SELECT validFrom FROM edges WHERE startId = ? OR endId = ? UNION SELECT validTo FROM edges WHERE validTo IS NOT NULL AND (startId = ? OR endId = ?) ) AS tp JOIN (SELECT validTo FROM nodes WHERE rowid = ?) AS tpThreshold ON tpThreshold.validTo IS NULL OR tpThreshold.validTo > tp.timepoint WHERE tp.timepoint >= ? AND tp.timepoint <= ? ORDER BY tp.timepoint DESC;");
		}
		stmtInstantsBetween.setInt(1, nodeID);
		stmtInstantsBetween.setInt(2, nodeID);
		stmtInstantsBetween.setInt(3, nodeID);
		stmtInstantsBetween.setInt(4, nodeID);
		stmtInstantsBetween.setInt(5, nodeID);
		stmtInstantsBetween.setInt(6, nodeID);
		stmtInstantsBetween.setInt(7, nodeID);
		stmtInstantsBetween.setLong(8, fromInclusive);
		stmtInstantsBetween.setLong(9, toInclusive);
		return stmtInstantsBetween;
	}
	
	public PreparedStatement getInstantsFromStatement(int nodeID, long fromInclusive) throws SQLException {
		if (stmtInstantsFrom == null) {
			stmtInstantsFrom = connection.prepareStatement("SELECT DISTINCT tp.timepoint FROM ( SELECT validFrom AS timepoint FROM nodes WHERE rowid = ? UNION SELECT validAt FROM nodeprops WHERE nodeid = ? UNION SELECT validFrom FROM edges WHERE startId = ? OR endId = ? UNION SELECT validTo FROM edges WHERE validTo IS NOT NULL AND (startId = ? OR endId = ?) ) AS tp JOIN (SELECT validTo FROM nodes WHERE rowid = ?) AS tpThreshold ON tpThreshold.validTo IS NULL OR tpThreshold.validTo > tp.timepoint WHERE tp.timepoint >= ? ORDER BY tp.timepoint DESC;");
		}
		stmtInstantsFrom.setInt(1, nodeID);
		stmtInstantsFrom.setInt(2, nodeID);
		stmtInstantsFrom.setInt(3, nodeID);
		stmtInstantsFrom.setInt(4, nodeID);
		stmtInstantsFrom.setInt(5, nodeID);
		stmtInstantsFrom.setInt(6, nodeID);
		stmtInstantsFrom.setInt(7, nodeID);
		stmtInstantsFrom.setLong(8, fromInclusive);
		return stmtInstantsFrom;
	}
	
	public PreparedStatement getInstantsUpToStatement(int nodeID, long toInclusive) throws SQLException {
		if (stmtInstantsUpTo == null) {
			stmtInstantsUpTo = connection.prepareStatement("SELECT DISTINCT tp.timepoint FROM ( SELECT validFrom AS timepoint FROM nodes WHERE rowid = ? UNION SELECT validAt FROM nodeprops WHERE nodeid = ? UNION SELECT validFrom FROM edges WHERE startId = ? OR endId = ? UNION SELECT validTo FROM edges WHERE validTo IS NOT NULL AND (startId = ? OR endId = ?) ) AS tp JOIN (SELECT validTo FROM nodes WHERE rowid = ?) AS tpThreshold ON tpThreshold.validTo IS NULL OR tpThreshold.validTo > tp.timepoint WHERE tp.timepoint <= ? ORDER BY tp.timepoint DESC;");
		}
		stmtInstantsUpTo.setInt(1, nodeID);
		stmtInstantsUpTo.setInt(2, nodeID);
		stmtInstantsUpTo.setInt(3, nodeID);
		stmtInstantsUpTo.setInt(4, nodeID);
		stmtInstantsUpTo.setInt(5, nodeID);
		stmtInstantsUpTo.setInt(6, nodeID);
		stmtInstantsUpTo.setInt(7, nodeID);
		stmtInstantsUpTo.setLong(8, toInclusive);
		return stmtInstantsUpTo;
	}
	
	public PreparedStatement getIsAliveStatement(int nodeID, long time) throws SQLException {
		if (stmtIsAlive == null) {
			stmtIsAlive = connection.prepareStatement("SELECT EXISTS (SELECT 1 FROM nodes WHERE rowid = ? AND validFrom <= ? AND (validTo IS NULL OR validTo > ?));");
		}
		stmtIsAlive.setInt(1, nodeID);
		stmtIsAlive.setLong(2, time);
		stmtIsAlive.setLong(3, time);
		return stmtIsAlive;
	}
	
	public PreparedStatement getIsAliveStatement(int nodeID) throws SQLException {
		return this.getIsAliveStatement(nodeID, defaultTimeProvider.get());
	}
	
	public PreparedStatement getOutgoingEdgesWithTypeBetweenStatement(String type, int fromID, long fromInclusive, long toExclusive) throws SQLException {
		if (stmtOutgoingEdgesWithTypeBetween == null) {
			stmtOutgoingEdgesWithTypeBetween = connection.prepareStatement("SELECT rowid, endId, validFrom FROM edges WHERE label = ? AND startId = ? AND ((validFrom >= ? AND validFrom < ?) OR (? >= validFrom AND ? < COALESCE(validTo, ?)));");
		}
		stmtOutgoingEdgesWithTypeBetween.setString(1, type);
		stmtOutgoingEdgesWithTypeBetween.setInt(2, fromID);
		stmtOutgoingEdgesWithTypeBetween.setLong(3, fromInclusive);
		stmtOutgoingEdgesWithTypeBetween.setLong(5, fromInclusive);
		stmtOutgoingEdgesWithTypeBetween.setLong(6, fromInclusive);
		stmtOutgoingEdgesWithTypeBetween.setLong(4, toExclusive);
		stmtOutgoingEdgesWithTypeBetween.setLong(7, toExclusive);
		return stmtOutgoingEdgesWithTypeBetween;
	}
	
	public PreparedStatement getIncomingEdgesWithTypeBetweenStatement(String type, int toID, long fromInclusive, long toExclusive) throws SQLException {
		if (stmtIncomingEdgesWithTypeBetween == null) {
			stmtIncomingEdgesWithTypeBetween = connection.prepareStatement("SELECT rowid, startId, validFrom FROM edges WHERE label = ? AND endId = ? AND ((validFrom >= ? AND validFrom < ?) OR (? >= validFrom AND ? < COALESCE(validTo, ?)));");
		}
		stmtIncomingEdgesWithTypeBetween.setString(1, type);
		stmtIncomingEdgesWithTypeBetween.setInt(2, toID);
		stmtIncomingEdgesWithTypeBetween.setLong(3, fromInclusive);
		stmtIncomingEdgesWithTypeBetween.setLong(5, fromInclusive);
		stmtIncomingEdgesWithTypeBetween.setLong(6, fromInclusive);
		stmtIncomingEdgesWithTypeBetween.setLong(4, toExclusive);
		stmtIncomingEdgesWithTypeBetween.setLong(7, toExclusive);
		return stmtIncomingEdgesWithTypeBetween;
	}
	
	public PreparedStatement getOutgoingEdgesWithTypeCreatedBetweenStatement(String type, int fromID, long fromInclusive, long toExclusive) throws SQLException {
		if (stmtOutgoingEdgesWithTypeCreatedBetween == null) {
			stmtOutgoingEdgesWithTypeCreatedBetween = connection.prepareStatement("SELECT rowid, endId, validFrom FROM edges WHERE label = ? AND startId = ? AND validFrom >= ? AND validFrom < ?;");
		}
		stmtOutgoingEdgesWithTypeCreatedBetween.setString(1, type);
		stmtOutgoingEdgesWithTypeCreatedBetween.setInt(2, fromID);
		stmtOutgoingEdgesWithTypeCreatedBetween.setLong(3, fromInclusive);
		stmtOutgoingEdgesWithTypeCreatedBetween.setLong(4, toExclusive);
		return stmtOutgoingEdgesWithTypeCreatedBetween;
	}
	
	public PreparedStatement getIncomingEdgesWithTypeCreatedBetweenStatement(String type, int toID, long fromInclusive, long toExclusive) throws SQLException {
		if (stmtIncomingEdgesWithTypeCreatedBetween == null) {
			stmtIncomingEdgesWithTypeCreatedBetween = connection.prepareStatement("SELECT rowid, startId, validFrom FROM edges WHERE label = ? AND endId = ? AND validFrom >= ? AND validFrom < ?;");
		}
		stmtIncomingEdgesWithTypeCreatedBetween.setString(1, type);
		stmtIncomingEdgesWithTypeCreatedBetween.setInt(2, toID);
		stmtIncomingEdgesWithTypeCreatedBetween.setLong(3, fromInclusive);
		stmtIncomingEdgesWithTypeCreatedBetween.setLong(4, toExclusive);
		return stmtIncomingEdgesWithTypeCreatedBetween;
	}
	
	@Override
	public PreparedStatement getUpsertNodeIndexStatement(String name) throws SQLException {
		if (stmtUpsertNodeIndex == null) {
			stmtUpsertNodeIndex = connection.prepareStatement("INSERT INTO nodeindices (name) VALUES (?) ON CONFLICT (name) DO NOTHING;");
		}
		stmtUpsertNodeIndex.setString(1, name);
		return stmtUpsertNodeIndex;
	}
	
	@Override
	public PreparedStatement getDeleteNodeIndexStatement(String name) throws SQLException {
		if (stmtDeleteNodeIndex == null) {
			stmtDeleteNodeIndex = connection.prepareStatement("DELETE FROM nodeindices WHERE name = ?;");
		}
		stmtDeleteNodeIndex.setString(1, name);
		return stmtDeleteNodeIndex;
	}
	
	@Override
	public PreparedStatement getAllNodeIndicesStatement() throws SQLException {
		if (stmtAllNodeIndices == null) {
			stmtAllNodeIndices = connection.prepareStatement("SELECT name FROM nodeindices;");
		}
		return stmtAllNodeIndices;
	}
	
	public PreparedStatement getAddNodeIndexEntryStatement(String indexName, String key, int nodeId, Object value, long time) throws SQLException {
		PreparedStatement stmt = stmtAddNodeIndexEntry.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("INSERT OR REPLACE INTO `idx_%s` (key, nodeId, value, validFrom, validTo) VALUES (?, ?, ?, ?, NULL);", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setInt(2, nodeId);
		stmt.setObject(3, value);
		stmt.setLong(4, time);
		return stmt;
	}
	
	@Override
	public PreparedStatement getAddNodeIndexEntryStatement(String indexName, String key, int nodeId, Object value) throws SQLException {
		return this.getAddNodeIndexEntryStatement(indexName, key, nodeId, value, defaultTimeProvider.get());
	}
	
	public PreparedStatement getAnnotateNodeIndexEntryStatement(String indexName, String key, int nodeId, Object value, long validToExclusive, long time) throws SQLException {
		PreparedStatement stmt = stmtAnnotateNodeIndexEntry.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("INSERT OR REPLACE INTO `idx_%s` (key, nodeId, value, validFrom, validTo) VALUES (?, ?, ?, ?, ?);", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setInt(2, nodeId);
		stmt.setObject(3, value);
		stmt.setLong(4, time);
		stmt.setLong(5, validToExclusive);
		return stmt;
	}
	
	public PreparedStatement getAnnotateNodeIndexEntryStatement(String indexName, String key, int nodeId, Object value, long validToExclusive) throws SQLException {
		return this.getAnnotateNodeIndexEntryStatement(indexName, key, nodeId, value, validToExclusive, defaultTimeProvider.get());
	}
	
	public PreparedStatement getRemoveNodeIndexEntryStatement(String indexName, String key, int nodeId, Object value, long time) throws SQLException {
		PreparedStatement stmt = stmtRemoveNodeIndexEntry.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("UPDATE `idx_%s` SET validTo = ? WHERE key = ? AND nodeId = ? AND value = ? AND validTo IS NULL;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setLong(1, time);
		stmt.setString(2, key);
		stmt.setInt(3, nodeId);
		stmt.setObject(4, value);
		return stmt;
	}
	
	@Override
	public PreparedStatement getRemoveNodeIndexEntryStatement(String indexName, String key, int nodeId, Object value) throws SQLException {
		return this.getRemoveNodeIndexEntryStatement(indexName, key, nodeId, value, defaultTimeProvider.get());
	}
	
	public PreparedStatement getRemoveNodeFromIndexStatement(String indexName, int nodeId, long time) throws SQLException {
		PreparedStatement stmt = stmtRemoveNodeFromIndex.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("UPDATE `idx_%s` SET validTo = ? WHERE nodeId = ? AND validTo IS NULL;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setLong(1, time);
		stmt.setInt(2, nodeId);
		return stmt;
	}
	
	@Override
	public PreparedStatement getRemoveNodeFromIndexStatement(String indexName, int nodeId) throws SQLException {
		return this.getRemoveNodeFromIndexStatement(indexName, nodeId, defaultTimeProvider.get());
	}
	
	public PreparedStatement getRemoveNodeFieldFromIndexStatement(String indexName, int nodeId, String key, long time) throws SQLException {
		PreparedStatement stmt = stmtRemoveNodeFieldFromIndex.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("UPDATE `idx_%s` SET validTo = ? WHERE nodeId = ? AND key = ? AND validTo IS NULL;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setLong(1, time);
		stmt.setInt(2, nodeId);
		stmt.setString(3, key);
		return stmt;
	}
	
	@Override
	public PreparedStatement getRemoveNodeFieldFromIndexStatement(String indexName, int nodeId, String key) throws SQLException {
		return this.getRemoveNodeFieldFromIndexStatement(indexName, nodeId, key, defaultTimeProvider.get());
	}
	
	public PreparedStatement getRemoveNodeValueFromIndexStatement(String indexName, int nodeId, Object value, long time) throws SQLException {
		PreparedStatement stmt = stmtRemoveNodeValueFromIndex.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("UPDATE `idx_%s` SET validTo = ? WHERE nodeId = ? AND value = ? AND validTo IS NULL;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setLong(1, time);
		stmt.setInt(2, nodeId);
		stmt.setObject(3, value);
		return stmt;
	}
	
	@Override
	public PreparedStatement getRemoveNodeValueFromIndexStatement(String indexName, int nodeId, Object value) throws SQLException {
		return this.getRemoveNodeValueFromIndexStatement(indexName, nodeId, value, defaultTimeProvider.get());
	}
	
	public PreparedStatement getQueryIndexValuePatternStatement(String indexName, String key, String value, long time) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValuePattern.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT DISTINCT nodeId FROM `idx_%s` WHERE key = ? AND value GLOB ? AND validFrom <= ? AND (validTo IS NULL OR validTo > ?);", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setString(2, value);
		stmt.setLong(3, time);
		stmt.setLong(4, time);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValuePatternStatement(String indexName, String key, String value) throws SQLException {
		return this.getQueryIndexValuePatternStatement(indexName, key, value, defaultTimeProvider.get());
	}
	
	public PreparedStatement getQueryIndexValuePatternCountStatement(String indexName, String key, String value, long time) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValuePatternCount.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT COUNT(DISTINCT nodeId) FROM `idx_%s` WHERE key = ? AND value GLOB ? AND validFrom <= ? AND (validTo IS NULL OR validTo > ?);", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setString(2, value);
		stmt.setLong(3, time);
		stmt.setLong(4, time);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValuePatternCountStatement(String indexName, String key, String value) throws SQLException {
		return this.getQueryIndexValuePatternCountStatement(indexName, key, value, defaultTimeProvider.get());
	}
	
	public PreparedStatement getQueryIndexValuePatternSingleStatement(String indexName, String key, String value, long time) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValuePatternSingle.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT DISTINCT nodeId FROM `idx_%s` WHERE key = ? AND value GLOB ? AND validFrom <= ? AND (validTo IS NULL OR validTo > ?)LIMIT 1;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setString(2, value);
		stmt.setLong(3, time);
		stmt.setLong(4, time);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValuePatternSingleStatement(String indexName, String key, String value) throws SQLException {
		return this.getQueryIndexValuePatternSingleStatement(indexName, key, value, defaultTimeProvider.get());
	}
	
	public PreparedStatement getQueryIndexValueExactStatement(String indexName, String key, Object value, long time) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValueExact.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT DISTINCT nodeId FROM `idx_%s` WHERE key = ? AND value = ? AND validFrom <= ? AND (validTo IS NULL OR validTo > ?);", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setObject(2, value);
		stmt.setLong(3, time);
		stmt.setLong(4, time);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValueExactStatement(String indexName, String key, Object value) throws SQLException {
		return this.getQueryIndexValueExactStatement(indexName, key, value, defaultTimeProvider.get());
	}
	
	public PreparedStatement getQueryIndexValueExactCountStatement(String indexName, String key, Object value, long time) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValueExactCount.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT COUNT(DISTINCT nodeId) FROM `idx_%s` WHERE key = ? AND value = ? AND validFrom <= ? AND (validTo IS NULL OR validTo > ?);", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setObject(2, value);
		stmt.setLong(3, time);
		stmt.setLong(4, time);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValueExactCountStatement(String indexName, String key, Object value) throws SQLException {
		return this.getQueryIndexValueExactCountStatement(indexName, key, value, defaultTimeProvider.get());
	}
	
	public PreparedStatement getQueryIndexValueExactSingleStatement(String indexName, String key, Object value, long time) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValueExactSingle.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT DISTINCT nodeId FROM `idx_%s` WHERE key = ? AND value = ? AND validFrom <= ? AND (validTo IS NULL OR validTo > ?)LIMIT 1;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setObject(2, value);
		stmt.setLong(3, time);
		stmt.setLong(4, time);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValueExactSingleStatement(String indexName, String key, Object value) throws SQLException {
		return this.getQueryIndexValueExactSingleStatement(indexName, key, value, defaultTimeProvider.get());
	}
	
	public PreparedStatement getQueryIndexValueAllValuesStatement(String indexName, String key, long time) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValueAllValues.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT DISTINCT nodeId FROM `idx_%s` WHERE key = ? AND validFrom <= ? AND (validTo IS NULL OR validTo > ?);", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setLong(2, time);
		stmt.setLong(3, time);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValueAllValuesStatement(String indexName, String key) throws SQLException {
		return this.getQueryIndexValueAllValuesStatement(indexName, key, defaultTimeProvider.get());
	}
	
	public PreparedStatement getQueryIndexValueAllValuesCountStatement(String indexName, String key, long time) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValueAllValuesCount.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT COUNT(DISTINCT nodeId) FROM `idx_%s` WHERE key = ? AND validFrom <= ? AND (validTo IS NULL OR validTo > ?);", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setLong(2, time);
		stmt.setLong(3, time);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValueAllValuesCountStatement(String indexName, String key) throws SQLException {
		return this.getQueryIndexValueAllValuesCountStatement(indexName, key, defaultTimeProvider.get());
	}
	
	public PreparedStatement getQueryIndexValueAllValuesSingleStatement(String indexName, String key, long time) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValueAllValuesSingle.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT nodeId FROM `idx_%s` WHERE key = ? AND validFrom <= ? AND (validTo IS NULL OR validTo > ?)LIMIT 1;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setLong(2, time);
		stmt.setLong(3, time);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValueAllValuesSingleStatement(String indexName, String key) throws SQLException {
		return this.getQueryIndexValueAllValuesSingleStatement(indexName, key, defaultTimeProvider.get());
	}
	
	public PreparedStatement getQueryIndexValueAllPairsStatement(String indexName, long time) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValueAllPairs.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT DISTINCT nodeId FROM `idx_%s` WHERE validFrom <= ? AND (validTo IS NULL OR validTo > ?);", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setLong(1, time);
		stmt.setLong(2, time);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValueAllPairsStatement(String indexName) throws SQLException {
		return this.getQueryIndexValueAllPairsStatement(indexName, defaultTimeProvider.get());
	}
	
	public PreparedStatement getQueryIndexValueAllPairsCountStatement(String indexName, long time) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValueAllPairsCount.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT COUNT(DISTINCT nodeId) FROM `idx_%s` WHERE validFrom <= ? AND (validTo IS NULL OR validTo > ?);", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setLong(1, time);
		stmt.setLong(2, time);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValueAllPairsCountStatement(String indexName) throws SQLException {
		return this.getQueryIndexValueAllPairsCountStatement(indexName, defaultTimeProvider.get());
	}
	
	public PreparedStatement getQueryIndexValueAllPairsSingleStatement(String indexName, long time) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexValueAllPairsSingle.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT nodeId FROM `idx_%s` WHERE validFrom <= ? AND (validTo IS NULL OR validTo > ?)LIMIT 1;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setLong(1, time);
		stmt.setLong(2, time);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexValueAllPairsSingleStatement(String indexName) throws SQLException {
		return this.getQueryIndexValueAllPairsSingleStatement(indexName, defaultTimeProvider.get());
	}
	
	public PreparedStatement getQueryIndexNumberRangeStatement(String indexName, String key, boolean fromInclusive, Number from, boolean toInclusive, Number to, long time) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexNumberRange.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT DISTINCT nodeId FROM `idx_%s` WHERE key = ? AND validFrom <= ? AND (validTo IS NULL OR validTo > ?) AND (? AND value >= ? OR value > ?) AND (? AND value <= ? OR value < ?);", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setLong(2, time);
		stmt.setLong(3, time);
		stmt.setBoolean(4, fromInclusive);
		stmt.setObject(5, from);
		stmt.setObject(6, from);
		stmt.setBoolean(7, toInclusive);
		stmt.setObject(8, to);
		stmt.setObject(9, to);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexNumberRangeStatement(String indexName, String key, boolean fromInclusive, Number from, boolean toInclusive, Number to) throws SQLException {
		return this.getQueryIndexNumberRangeStatement(indexName, key, fromInclusive, from, toInclusive, to, defaultTimeProvider.get());
	}
	
	public PreparedStatement getQueryIndexNumberRangeCountStatement(String indexName, String key, boolean fromInclusive, Number from, boolean toInclusive, Number to, long time) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexNumberRangeCount.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT COUNT(DISTINCT nodeId) FROM `idx_%s` WHERE key = ? AND validFrom <= ? AND (validTo IS NULL OR validTo > ?) AND (? AND value >= ? OR value > ?) AND (? AND value <= ? OR value < ?);", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setLong(2, time);
		stmt.setLong(3, time);
		stmt.setBoolean(4, fromInclusive);
		stmt.setObject(5, from);
		stmt.setObject(6, from);
		stmt.setBoolean(7, toInclusive);
		stmt.setObject(8, to);
		stmt.setObject(9, to);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexNumberRangeCountStatement(String indexName, String key, boolean fromInclusive, Number from, boolean toInclusive, Number to) throws SQLException {
		return this.getQueryIndexNumberRangeCountStatement(indexName, key, fromInclusive, from, toInclusive, to, defaultTimeProvider.get());
	}
	
	public PreparedStatement getQueryIndexNumberRangeSingleStatement(String indexName, String key, boolean fromInclusive, Number from, boolean toInclusive, Number to, long time) throws SQLException {
		PreparedStatement stmt = stmtQueryIndexNumberRangeSingle.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT nodeId FROM `idx_%s` WHERE key = ? AND validFrom <= ? AND (validTo IS NULL OR validTo > ?) AND (? AND value >= ? OR value > ?) AND (? AND value <= ? OR value < ?)LIMIT 1;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setString(1, key);
		stmt.setLong(2, time);
		stmt.setLong(3, time);
		stmt.setBoolean(4, fromInclusive);
		stmt.setObject(5, from);
		stmt.setObject(6, from);
		stmt.setBoolean(7, toInclusive);
		stmt.setObject(8, to);
		stmt.setObject(9, to);
		return stmt;
	}
	
	@Override
	public PreparedStatement getQueryIndexNumberRangeSingleStatement(String indexName, String key, boolean fromInclusive, Number from, boolean toInclusive, Number to) throws SQLException {
		return this.getQueryIndexNumberRangeSingleStatement(indexName, key, fromInclusive, from, toInclusive, to, defaultTimeProvider.get());
	}
	
	public PreparedStatement getIndexVersionsStatement(String indexName, int nodeID, String key, Object value, long time) throws SQLException {
		PreparedStatement stmt = stmtIndexVersions.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT validFrom, validTo FROM `idx_%s` WHERE nodeid = ? AND key = ? AND value = ? AND (validTo IS NULL OR (validTo > ? AND validTo > validFrom)) ORDER BY validFrom DESC;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setInt(1, nodeID);
		stmt.setString(2, key);
		stmt.setObject(3, value);
		stmt.setLong(4, time);
		return stmt;
	}
	
	public PreparedStatement getIndexVersionsStatement(String indexName, int nodeID, String key, Object value) throws SQLException {
		return this.getIndexVersionsStatement(indexName, nodeID, key, value, defaultTimeProvider.get());
	}
	
	public PreparedStatement getIndexEarliestVersionSinceStatement(String indexName, int nodeID, String key, Object value, long time) throws SQLException {
		PreparedStatement stmt = stmtIndexEarliestVersionSince.computeIfAbsent(indexName,
		(name) -> {
			try {
				return connection.prepareStatement(String.format("SELECT validFrom, validTo FROM `idx_%s` WHERE nodeid = ? AND key = ? AND value = ? AND (validTo IS NULL OR (validTo > ? AND validTo > validFrom)) ORDER BY validFrom ASC LIMIT 1;", name));
			} catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
				return null;
			}
		});
		stmt.setInt(1, nodeID);
		stmt.setString(2, key);
		stmt.setObject(3, value);
		stmt.setLong(4, time);
		return stmt;
	}
	
	public PreparedStatement getIndexEarliestVersionSinceStatement(String indexName, int nodeID, String key, Object value) throws SQLException {
		return this.getIndexEarliestVersionSinceStatement(indexName, nodeID, key, value, defaultTimeProvider.get());
	}
	
	protected String getPropertyType(Object value) {
		if (value instanceof Blob) {
			return TYPE_BLOB_BASE64;
		} else {
			return value == null ? "unknown" : value.getClass().getSimpleName();
		}
	}
	
	/* From https://www.baeldung.com/java-inputstream-to-outputstream (Java 8 alternative to InputStream.transferTo) */
	protected void copy(InputStream source, OutputStream target) throws IOException {
		byte[] buf = new byte[8192];
		int length;
		while ((length = source.read(buf)) > 0) {
			target.write(buf, 0, length);
		}
	}
	
	/**
	|* Xerial SQLite JDBC driver does not support BLOB - we encode those as base64 strings.
	|* @throws SQLException There was an error reading the blob.
	|* @throws IOException There was an error copying the bytes.
	\*/
	protected String base64(Object value) throws IOException, SQLException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		copy(((Blob) value).getBinaryStream(), bos);
		return Base64.encodeBase64String(bos.toByteArray());
	}
	
}