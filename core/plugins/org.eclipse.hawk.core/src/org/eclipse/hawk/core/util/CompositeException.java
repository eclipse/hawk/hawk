package org.eclipse.hawk.core.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Collects one or more exceptions as a single one.
 */
public class CompositeException extends Exception {
	private static final long serialVersionUID = 1L;
	private List<Exception> exceptions = new ArrayList<>();

	public CompositeException(List<Exception> exceptions) {
		this.exceptions = exceptions;
	}

	public List<Exception> getExceptions() {
		return exceptions;
	}

	@Override
	public String getMessage() {
		if (exceptions.isEmpty()) return "";

		StringBuilder sb = new StringBuilder();
		for (Exception ex : exceptions) {
			if (exceptions.size() > 1) sb.append("- ");
			sb.append(ex.getMessage());
			sb.append(System.lineSeparator());
		}

		return sb.toString();
	}

	
}